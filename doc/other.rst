Other stuff 
================================================================================

Additional functionality that can be used by any var3d module

Uniprot
--------------------------------------------------------------------------------

Some functions that interact with uniprot and parse data.

.. autofunction:: var3d.uniprot.ValidACPattern

.. autofunction:: var3d.uniprot.FetchUniprotEntry

.. autofunction:: var3d.uniprot.ParseUniprotSequence

.. autoclass:: var3d.uniprot.FeatureTable
    :members:

.. autofunction:: var3d.uniprot.ParseUniprotFT

.. autofunction:: var3d.uniprot.ParseRecommendedNames

.. autofunction:: var3d.uniprot.GetUniprotSequence

Generate alignments
--------------------------------------------------------------------------------

var3d implements yet another strategy pattern to obtain sequence alignments
for a given target sequence. Alignments can be retrieved using any class
implementing the interface provided by the 
:class:`var3d.msa_generator.MSAGenerator`. Currently this is:

* :class:`var3d.msa_generator.MMseqs2MSAGenerator`
* :class:`var3d.msa_generator.JackhmmerMSAGenerator`
* :class:`var3d.msa_generator.HHblitsMSAGenerator`

.. autoclass:: var3d.msa_generator.MSAGenerator
    :members:

.. autoclass:: var3d.msa_generator.MMseqs2MSAGenerator
    :members:

.. autoclass:: var3d.msa_generator.JackhmmerMSAGenerator
    :members:

.. autoclass:: var3d.msa_generator.HHblitsMSAGenerator
    :members:
 
Misc
--------------------------------------------------------------------------------

Random functions that might be useful

.. autofunction:: var3d.common.CompareEnt

.. autofunction:: var3d.common.CheckRange
