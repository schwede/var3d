Bring it all together - Pipelines
================================================================================

There are pipeline objects for two distinct tasks: 
:class:`var3d.pipeline.DataImportPipeline` and
:class:`var3d.pipeline.AnnotationPipeline`. Both work with
:class:`var3d.pipeline.DataContainer` to store reference sequence information
and the associated :class:`var3d.base.Structure` and :class:`var3d.base.Variant`
objects.

Data Handling
--------------------------------------------------------------------------------

.. autoclass:: var3d.pipeline.DataContainer
    :members:

Pipelines
--------------------------------------------------------------------------------

.. autoclass:: var3d.pipeline.DataImportPipeline
    :members:

.. autofunction:: var3d.pipeline.GetEmptyAnno

.. autoclass:: var3d.pipeline.AnnotationPipeline
    :members:

Process annotation pipelines in parallel
--------------------------------------------------------------------------------

With growing number of :class:`var3d.base.Variant` or 
:class:`var3d.base.Structure` objects in a 
:class:`var3d.pipeline.DataContainer`, processing everything sequentially in a
:class:`var3d.pipeline.AnnotationPipeline` may become unfeasible. You have to
manually split it up the workload by specifying *variant_hashes* and 
*structure_hashes* when calling :class:`var3d.pipeline.AnnotationPipeline.Run`.
:func:`var3d.pipeline.MergeAnnotations` (merge two annotation results) and
:func:`var3d.pipeline.CollectAnnotations` (merge n annotation results that are
stored as files) help you to concatenate the results of such a split workload.

.. autofunction:: var3d.pipeline.MergeAnnotations

.. autofunction:: var3d.pipeline.CollectAnnotations
