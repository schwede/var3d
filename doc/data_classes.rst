Variants & Structures - The data classes
================================================================================

Variants and structures are represented by their respective data types 
(:class:`var3d.base.Variant` and :class:`var3d.base.Structure`) which are self
consistent, i.e. all information can be JSON serialized and loaded again.
var3d distinguishes types of variants:

.. autoclass:: var3d.base.VariantType
    :members:

.. autoclass:: var3d.base.Variant
    :members:

.. autoclass:: var3d.base.Structure
    :members:
