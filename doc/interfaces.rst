Standardized Interfaces - Do things with Variants & Structures
================================================================================

There are interfaces for two distinct tasks: data import and annotation.

Data Import Interfaces
--------------------------------------------------------------------------------

.. autoclass:: var3d.base.VarImporter
    :members:

.. autoclass:: var3d.base.StructImporter
    :members:

Annotation Interfaces
--------------------------------------------------------------------------------

.. autoclass:: var3d.base.SeqAnno
    :members:

.. autoclass:: var3d.base.StructAnno
    :members:

.. autoclass:: var3d.base.VarSeqAnno
    :members:

.. autoclass:: var3d.base.VarStructAnno
    :members:
