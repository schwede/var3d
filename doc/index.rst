.. var3d documentation master file, created by
   sphinx-quickstart on Tue Oct  5 13:40:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

var3d
=====

Before diving into the docs, here's some code to give a taste of what can be
done with var3d:

.. literalinclude:: ../pipelines/crambin_annotation.py

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Getting started <readme_link>
   Variants & structures <data_classes>
   Standardized interfaces <interfaces>
   Bring it all together - Pipelines <pipeline>
   The actual annotations <implementations>
   Other stuff <other>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
