The actual annotations
================================================================================

Computation happen in annotation objects that implement the interfaces defined
in the base module. 

Variant import
--------------------------------------------------------------------------------

.. autoclass:: var3d.var_importer.HRVarImporter
    :members:

.. autoclass:: var3d.var_importer.UniprotEntryVarImporter
    :members:


Structure import
--------------------------------------------------------------------------------

.. autoclass:: var3d.struct_importer.FilesystemStructImporter
    :members:

.. autoclass:: var3d.struct_importer.SMRStructImporter
    :members:


Sequence annotations
--------------------------------------------------------------------------------

.. autoclass:: var3d.seq_anno.ConsurfSeqAnno
    :members:

.. autoclass:: var3d.seq_anno.EntropySeqAnno
    :members:

.. autoclass:: var3d.seq_anno.AAIndexSeqAnno
    :members:


Structure annotations
--------------------------------------------------------------------------------

.. autoclass:: var3d.struct_anno.AccessibilityStructAnno
    :members:

.. autoclass:: var3d.struct_anno.TransmembraneStructAnno
    :members:

.. autoclass:: var3d.struct_anno.InterfaceStructAnno
    :members:

.. autoclass:: var3d.struct_anno.PLIPStructAnno
    :members:

Sequence based variant annotations
--------------------------------------------------------------------------------

.. autoclass:: var3d.var_seq_anno.ProveanVarSeqAnno
    :members:

.. autoclass:: var3d.var_seq_anno.AAIndexVarSeqAnno
    :members:


Structure based variant annotations
--------------------------------------------------------------------------------

.. autoclass:: var3d.var_struct_anno.FoldXVarStructAnno
    :members:
