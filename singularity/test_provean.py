import sys
import json

from ost import seq
from ost import io

from var3d import base
from var3d import var_seq_anno
from var3d import pipeline

if len(sys.argv) != 2:
    print("USAGE: v3d <VAR3D_IMAGE> test_provean.py <NR>")
    sys.exit(42)

nr = sys.argv[1]

provean_anno = var_seq_anno.ProveanVarSeqAnno(nr)
anno_pipeline = pipeline.AnnotationPipeline(var_seq_anno=[provean_anno])

# crambin, the mother of all sequences
s = seq.CreateSequence("A", "TTCCPSIVARSNFNVCRLPGTPEAICATYTGCIIIPGATCPGDYAN")

# We have this awesome variant just for this sequence...
# Let's sublass VarImporter! For this we implement the
# Run and the Key method
class YoloVarImporter(base.VarImporter):
    def Run(self, sequence, seq_range=None):
        if sequence.GetName() == "A":
            v = base.Variant(
                "C", "W", 4, base.VariantType.SUBSTITUTION
            )
            return [v]
        return []

    def Key(self):
        return "yolo"

v_imp = YoloVarImporter()
import_pipeline = pipeline.DataImportPipeline(var_importer=[v_imp])
data = pipeline.DataContainer(s)
data = import_pipeline.Import(data)
anno = anno_pipeline.Run(data)
with open("provean_anno.json", 'w') as fh:
    json.dump(anno, fh)

