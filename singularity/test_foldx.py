import sys
import json

from ost import seq
from ost import io

from var3d import base
from var3d import var_struct_anno
from var3d import pipeline

if len(sys.argv) != 1:
    print("USAGE: v3d <VAR3D_IMAGE> test_foldx.py")
    sys.exit(42)

foldx_anno = var_struct_anno.FoldXVarStructAnno()
anno_pipeline = pipeline.AnnotationPipeline(var_struct_anno=[foldx_anno])

# crambin, the mother of all sequences
s = seq.CreateSequence("A", "TTCCPSIVARSNFNVCRLPGTPEAICATYTGCIIIPGATCPGDYAN")

# We have this awesome variant just for this sequence...
# Let's sublass VarImporter! For this we implement the
# Run and the Key method
class YoloVarImporter(base.VarImporter):
    def Run(self, sequence, seq_range=None):
        if sequence.GetName() == "A":
            v = base.Variant(
                "C", "W", 4, base.VariantType.SUBSTITUTION
            )
            return [v]
        return []

    def Key(self):
        return "yolo"

# and it comes with a structure!
class YoloStructImporter(base.StructImporter):
    def Run(self, sequence, seq_range=None):
        if sequence.GetName() == "A":
            crambin = io.LoadPDB("crambin.pdb")
            structure = base.Structure(s, "crambin", crambin,"cname=A")
            return [structure]
        else:
            return []
    def Key(self):
        return "yolo"

v_imp = YoloVarImporter()
s_imp = YoloStructImporter()
import_pipeline = pipeline.DataImportPipeline(var_importer=[v_imp],
                                              struct_importer = [s_imp])
data = pipeline.DataContainer(s)
data = import_pipeline.Import(data)
anno = anno_pipeline.Run(data)
with open("foldx_anno.json", 'w') as fh:
    json.dump(anno, fh)

