#! /usr/bin/env python3

import argparse
import sys
import os

class dependency:
    """Slim container holding data that go into different sections
    in a singularity definition file
    """
    def __init__(self):
        self.files = list()
        self.apt_get_packages = list()
        self.post_commands = list()
        self.env_commands = list()

    def add_file(self, local_path, container_path):
        self.files.append((local_path, container_path))

    def add_apt_get_package(self, package):
        self.apt_get_packages.append(package)

    def add_post_command(self, post_command):
        self.post_commands.append(post_command)

    def add_env_command(self, env_command):
        self.env_commands.append(env_command)


def get_base_dependency(ost_tag, promod3_tag, openmm_tag, hhblits3_tag,
                        mmseqs2_tag, python_version):
    """Returns dependency to build software enabling var3d base capabilitites

    :param ost_tag: Tag to be pulled from OpenStructure repo, e.g. "2.3.1"
    :type ost_tag: :class:`str`
    :param promod3_tag: Tag to be pulled from ProMod3 repo, e.g. 3.2.1
    :type promod3_tag: :class:`str`
    :param openmm_tag: Tag to be pulled from OpenMM repo, e.g. 7.6.0
    :type openmm_tag: :class:`str`
    :param hhblits3_tag: Tag to be pulled from HHblits 3 repo, e.g. v3.3.0
    :type hhblits3_tag: :class:`str`
    :param mmseqs2_tag: Tag to be pulled from MMseqs 2 repo, e.g. 13-45111
    :type mmseqs2_tag: :class:`str`
    :param python_version: Python version in container in form {Major}.{Minor},
                           e.g. 3.9. Used to correctly set PYTHONPATH variables.
                           DEPENDS ON CONTAINER BASE IMAGE
    :type python_version: :class:`str`
    """
    d = dependency()

    d.add_apt_get_package("python3-all")
    d.add_apt_get_package("python3-numpy")
    d.add_apt_get_package("python3-scipy")
    d.add_apt_get_package("python3-pandas")
    d.add_apt_get_package("python3-biopython")
    d.add_apt_get_package("python3-sphinx")
    d.add_apt_get_package("python3-pip")
    d.add_apt_get_package("python3-pymongo")
    d.add_apt_get_package("python3-sklearn")
    d.add_apt_get_package("python3-rdkit")
    d.add_apt_get_package("python3-lxml")
    d.add_apt_get_package("python-is-python3")
    d.add_apt_get_package("hmmer")
    d.add_apt_get_package("cd-hit")
    d.add_apt_get_package("mafft")
    d.add_apt_get_package("muscle")
    d.add_apt_get_package("make")
    d.add_apt_get_package("cmake")
    d.add_apt_get_package("g++")
    d.add_apt_get_package("gfortran")
    d.add_apt_get_package("git")
    d.add_apt_get_package("tar")
    d.add_apt_get_package("libsqlite3-dev")
    d.add_apt_get_package("sip-dev")
    d.add_apt_get_package("libtiff-dev")
    d.add_apt_get_package("libfftw3-dev")
    d.add_apt_get_package("libeigen3-dev")
    d.add_apt_get_package("libboost-all-dev")
    d.add_apt_get_package("libpng-dev")
    d.add_apt_get_package("libxml2-dev")
    d.add_apt_get_package("wget")
    d.add_apt_get_package("doxygen")
    d.add_apt_get_package("swig")
    d.add_apt_get_package("clustalw")
    d.add_apt_get_package("jupyter")

    src_folder = "/usr/local/src"
    openmm_include_path = "/usr/local/openmm/include/"
    openmm_lib_path = "/usr/local/openmm/lib/"

    # myst parser is not yet available as Debian package
    # required to render markdown in sphinx
    d.add_post_command("pip3 install myst-parser")

    openmm_url = f"https://github.com/pandegroup/openmm/archive/{openmm_tag}.tar.gz"
    openmm_cmd = f"cd {src_folder} && "
    openmm_cmd += f"wget -O openmm.tar.gz -nc {openmm_url} && "
    openmm_cmd += f"mkdir {src_folder}/openmm && "
    openmm_cmd += f"tar xf openmm.tar.gz -C {src_folder}/openmm --strip-components=1 && "
    openmm_cmd += f"mkdir -p {src_folder}/openmm/build && "
    openmm_cmd += f"cd {src_folder}/openmm/build && "
    openmm_cmd += f"cmake .. && make && make install && "
    openmm_cmd += f"cd {src_folder}/openmm/build/python && "
    openmm_cmd += f"python3 setup.py build && python3 setup.py install && "
    openmm_cmd += f"rm {src_folder}/openmm.tar.gz && "
    openmm_cmd += f"rm -rf {src_folder}/openmm"
    d.add_post_command(openmm_cmd)

    ost_url = f"https://git.scicore.unibas.ch/schwede/openstructure/-/archive/{ost_tag}/openstructure-{ost_tag}.tar.gz"
    ost_cmd = f"cd {src_folder} && "
    ost_cmd += f"wget -O openstructure.tar.gz -nc {ost_url} && "
    ost_cmd += f"mkdir openstructure && "
    ost_cmd += f"tar xf openstructure.tar.gz -C {src_folder}/openstructure --strip-components=1 && "
    ost_cmd += f"mkdir -p {src_folder}/openstructure/build && "
    ost_cmd += f"cd {src_folder}/openstructure/build && "
    ost_cmd += f"cmake .. -DOPTIMIZE=ON -DENABLE_MM=ON -DCOMPILE_TMTOOLS=1 "
    ost_cmd += f"-DUSE_NUMPY=0 "
    ost_cmd += f"-DOPEN_MM_LIBRARY={openmm_lib_path}/libOpenMM.so "
    ost_cmd += f"-DOPEN_MM_INCLUDE_DIR={openmm_include_path} "
    ost_cmd += f"-DOPEN_MM_PLUGIN_DIR={openmm_lib_path}/plugins "
    ost_cmd += f"-DENABLE_GFX=ON -DENABLE_GUI=OFF -DENABLE_INFO=OFF "
    ost_cmd += f"-DCMAKE_C_FLAGS=\"-isystem /usr/include/boost/ -isystem {openmm_include_path}/include\" "
    ost_cmd += f"-DCMAKE_CXX_FLAGS=\"-isystem /usr/include/boost/ -isystem {openmm_include_path}/include\" && "
    ost_cmd += f"make && wget ftp://ftp.wwpdb.org/pub/pdb/data/monomers/components.cif.gz && "
    ost_cmd += f"stage/bin/chemdict_tool create components.cif.gz compounds.chemlib pdb && "
    ost_cmd += f"stage/bin/chemdict_tool update ../modules/conop/data/charmm.cif compounds.chemlib charmm && "
    ost_cmd += f"cmake .. -DCOMPOUND_LIB={src_folder}/openstructure/build/compounds.chemlib && "
    ost_cmd += f"make && make check && make install && "
    ost_cmd += f"rm {src_folder}/openstructure.tar.gz && rm -rf {src_folder}/openstructure"
    d.add_post_command(ost_cmd)

    # the following are required that we can immediately use ost during build
    #d.add_post_command("export PYTHONPATH=/usr/local/lib64/python3.8/site-packages:$PYTHONPATH")
    d.add_post_command("export OST_ROOT=\"/usr/local\"")
    d.add_post_command("export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH:/usr/local/lib64:/usr/local/openmm/lib/\"")
    
    promod3_url = f"https://git.scicore.unibas.ch/schwede/ProMod3/-/archive/{promod3_tag}/promod-{promod3_tag}.tar.gz"
    promod3_cmd = f"cd {src_folder} && "
    promod3_cmd += f"wget -O promod.tar.gz -nc {promod3_url} && "
    promod3_cmd += f"mkdir promod && tar xf promod.tar.gz -C {src_folder}/promod --strip-components=1 && "
    promod3_cmd += f"mkdir -p {src_folder}/promod/build && "
    promod3_cmd += f"cd {src_folder}/promod/build && "
    promod3_cmd += f"cmake .. -DOST_ROOT=/usr/local -DOPTIMIZE=1 -DENABLE_SSE=1 -DDISABLE_DOCUMENTATION=1 && "
    promod3_cmd += f"make && make check && make install && "
    promod3_cmd += f"cd {src_folder} && rm {src_folder}/promod.tar.gz && "
    promod3_cmd += f"rm -rf {src_folder}/promod"
    d.add_post_command(promod3_cmd)

    mmseqs2_url = "https://github.com/soedinglab/MMseqs2.git"
    mmseqs2_cmd = f"cd {src_folder} && "
    mmseqs2_cmd += f"git clone -b {mmseqs2_tag} {mmseqs2_url} && "
    mmseqs2_cmd += f"cd MMseqs2 && mkdir build && cd build && "
    mmseqs2_cmd += f"cmake -DCMAKE_BUILD_TYPE=RELEASE ../ && make && make install && "
    mmseqs2_cmd += f"cd {src_folder} && rm -rf MMseqs2"
    d.add_post_command(mmseqs2_cmd)

    hhblits3_url = "https://github.com/soedinglab/hh-suite.git"

    hhblits3_cmd = f"git clone -b {hhblits3_tag} {hhblits3_url} hh-suite && "
    hhblits3_cmd += f"cd hh-suite && mkdir build && cd build && cmake ../ && make && make install "
    d.add_post_command(hhblits3_cmd)
    # for some weird reason, chaining these commands results in weird errors
    d.add_post_command(f"cd {src_folder}")
    d.add_post_command(f"rm -rf hh-suite")

    d.add_env_command(f"export PYTHONPATH=/usr/local/lib64/python{python_version}/site-packages:$PYTHONPATH")
    d.add_env_command("export OST_ROOT=\"usr_local\"")
    d.add_env_command("export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH:/usr/local/lib64:/usr/local/openmm/lib/\"")

    return d


def get_foldx_dependency(foldx_dependency_dir):
    """Returns dependency to include FoldX, according executable must 
    be provided by the user

    :param foldx_dependency_dir: Directory containing the FoldX executable
    :type foldx_dependency_dir: :class:`str`
    """
    # check if expected file is there
    foldx_exec = os.path.join(foldx_dependency_dir, "foldx")
    if not os.path.exists(foldx_exec):
        raise RuntimeError(f"Expect file \"foldx\" in FoldX dependency dir ({foldx_dependency_dir})")
    d = dependency()
    d.add_file(foldx_exec, "/foldx/foldx")
    return d


def get_provean_dependency(provean_dependency_dir):
    """Returns dependency to include PROVEAN. PROVEAN seems to segfault with
    newer BLAST versions, we therefore directly ship the required executables
    in version 2.4.0. That's the file that is expected in
    *provean_dependency_dir*. 

    :param provean_dependency_dir: Directory containing a file called
                                   blast-2.4.0.tar.gz. This archive contains statically
                                   linked binaries for blastdbcmd and psiblast
    :type provean_dependency_dir: :class:`str`
    """
    d = dependency()
    # check if expected file is there
    blast_tarball = os.path.join(provean_dependency_dir, "blast-2.4.0.tar.gz")
    if not os.path.exists(blast_tarball):
        raise RuntimeError(f"Expect file blast-2.4.0.tar.gz in PROVEAN dependency dir ({provean_dependency_dir})")
    d.add_file(blast_tarball, "/provean/dependencies/blast.tar.gz")

    provean_url = "https://sourceforge.net/projects/provean/files/provean-1.1.5.tar.gz/download"

    provean_cmd = "cd /provean/dependencies && tar -xzvf blast.tar.gz && "
    provean_cmd += "rm blast.tar.gz && cd /provean && wget -O provean.tar.gz "
    provean_cmd += f"-nc {provean_url} && tar -xzvf provean.tar.gz && "
    provean_cmd += "rm provean.tar.gz && cd provean-1.1.5 && "
    provean_cmd += "./configure --prefix=/provean && make && make install && "
    provean_cmd += "rm -r /provean/provean-1.1.5"
    d.add_post_command(provean_cmd)
    return d


def get_plip_dependency(plip_dependency_dir, plip_tag, openbabel_tag):
    """Returns dependency to include PLIP. Two Python helper files are shiped
    by us and are located in plip_dependency_dir.

    :param plip_dependency_dir: Directory containing the two Python helper files
                                sm-run-plip and plip_helpers.py
    :type plip_dependency_dir: :class:`str`
    :param plip_tag: Tag to be pulled from PLIP repo, e.g. v2.2.2
    :type plip_tag: :class:`str`
    :param openbabel_tag: Tag to be pulled from OpenBabel repo, e.g. openbabel-3-3-0
    :type openbabel_tag: :class:`str`
    """
    d = dependency()
    # check if expected files are there
    files = [os.path.join(plip_dependency_dir, "sm-run-plip"),
             os.path.join(plip_dependency_dir, "plip_helpers.py")]
    for f in files:
        if not os.path.exists(f):
            raise RuntimeError(f"Expect file {f} in PLIP dependency dir ({plip_dependency_dir})")
        d.add_file(f, "/plip/" + os.path.basename(f))
        
    src_folder = "/usr/local/src"
    plip_url = "https://github.com/pharmai/plip.git"
    openbabel_url = "https://github.com/openbabel/openbabel.git"

    openbabel_cmd = f"cd {src_folder} && git clone -b {openbabel_tag} {openbabel_url} && "
    openbabel_cmd += f"cd openbabel && mkdir build && cd build && "
    openbabel_cmd += f"cmake .. -DPYTHON_EXECUTABLE=/usr/bin/python3 "
    openbabel_cmd += f"-DPYTHON_BINDINGS=ON -DCMAKE_INSTALL_PREFIX=/usr/local "
    openbabel_cmd += f"-DRUN_SWIG=ON && make install && cd {src_folder} && "
    openbabel_cmd += f"rm -rf openbabel"
    d.add_post_command(openbabel_cmd)

    plip_cmd = f"cd {src_folder} && git clone -b {plip_tag} {plip_url} && "
    plip_cmd += f"cd plip && cp -r plip /plip && cd {src_folder} && "
    plip_cmd += f"rm -rf plip && chmod +x /plip/plip/plipcmd.py"
    d.add_post_command(plip_cmd)

    d.add_env_command("export PYTHONPATH=/plip:$PYTHONPATH")

    return d


def get_consurf_dependency(consurf_dependency_dir):
    """Returns dependency to include ConSurf.

    The ConSurf-DB code has kindly been provided by the Ben-Tal lab and
    extended by some wrapper scripts. Ping us if you need to run ConSurf in a
    var3d environment.

    :param consurf_dependency_dir: Directory containing all files required to
                                   run ConSurf. Ping us...
    :type consurf_dependency_dir: :class:`str`
    """
    d = dependency()
    files = [os.path.join(consurf_dependency_dir, "ConSeq_gradesPE_and_Outputs.pm"),
             os.path.join(consurf_dependency_dir, "consurf"),
             os.path.join(consurf_dependency_dir, "ConSurfCommon.pm"),
             os.path.join(consurf_dependency_dir, "CONSURF_CONSTANTS.pm"),
             os.path.join(consurf_dependency_dir, "consurf_db.py"),
             os.path.join(consurf_dependency_dir, "CONSURF_FUNCTIONS.pm"),
             os.path.join(consurf_dependency_dir, "consurfrc.default"),
             os.path.join(consurf_dependency_dir, "consurf_wrapper.py"),
             os.path.join(consurf_dependency_dir, "cp_rasmol_gradesPE_and_pipe.pm"),
             os.path.join(consurf_dependency_dir, "create_final_homologues_html_file.pl"),
             os.path.join(consurf_dependency_dir, "GENERAL_CONSTANTS.pm"),
             os.path.join(consurf_dependency_dir, "inferModel.py"),
             os.path.join(consurf_dependency_dir, "MSA_parser.pm"),
             os.path.join(consurf_dependency_dir, "parseFiles.pm"),
             os.path.join(consurf_dependency_dir, "pdbParser.pm"),
             os.path.join(consurf_dependency_dir, "pdbParser.py"),
             os.path.join(consurf_dependency_dir, "prepareMSA.pm"),
             os.path.join(consurf_dependency_dir, "preprocess.py"),
             os.path.join(consurf_dependency_dir, "prottest-3.4.2-20160508.tar.gz"),
             os.path.join(consurf_dependency_dir, "rate4site-3.0.0.tar.gz"),
             os.path.join(consurf_dependency_dir, "rate4site_routines.pm"),
             os.path.join(consurf_dependency_dir, "README.md"),
             os.path.join(consurf_dependency_dir, "run_hmmer_and_cd-hit.pl"),
             os.path.join(consurf_dependency_dir, "Singularity"),
             os.path.join(consurf_dependency_dir, "TREE_parser.pm"),
             os.path.join(consurf_dependency_dir, "wasabiUtils.pm")]
    for f in files:
        if not os.path.exists(f):
            raise RuntimeError(f"Expect file {f} in ConSurf dependency dir ({consurf_dependency_dir})")
        d.add_file(f, "/consurf/" + os.path.basename(f))

    # There would be a prottest package in debian but it seems incompatible 
    # with its PyML dependency. 
    # Let's copy the tarball from https://github.com/ddarriba/prottest3/releases
    # that comes with its own PhyML binary which seems to work
    f = os.path.join(consurf_dependency_dir, "prottest-3.4.2-20160508.tar.gz")
    if not os.path.exists(f):
        raise RuntimeError(f"Expect file {f} in ConSurf dependency dir ({consurf_dependency_dir})")
    d.add_file(f, "/consurf/dependencies/" + os.path.basename(f))

    # There would be a rate4site package in debian that comes with two binaries:
    # rate4site and rate4site_doublerep. The latter is used as fallback when the 
    # first fails. However, that thing seems not doublerepi enough. Let's just build 
    # it ourselves and doublerep the hell out of it. 
    # Tarball downloaded from: ftp://rostlab.org/rate4site/ 
    # (linked from https://www.tau.ac.il/~itaymay/cp/rate4site.html)
    f = os.path.join(consurf_dependency_dir, "rate4site-3.0.0.tar.gz")
    if not os.path.exists(f):
        raise RuntimeError(f"Expect file {f} in ConSurf dependency dir ({consurf_dependency_dir})")
    d.add_file(f, "/consurf/dependencies/" + os.path.basename(f))

    d.add_apt_get_package("bioperl")
    d.add_apt_get_package("libbio-searchio-hmmer-perl")
    d.add_apt_get_package("libconfig-inifiles-perl")
    d.add_apt_get_package("libole-storage-lite-perl")
    d.add_apt_get_package("libspreadsheet-writeexcel-perl")
    d.add_apt_get_package("libcrypt-rc4-perl")
    d.add_apt_get_package("libdigest-perl-md5-perl")
    d.add_apt_get_package("libspreadsheet-parseexcel-perl")
    d.add_apt_get_package("libtext-csv-xs-perl")
    d.add_apt_get_package("libjson-perl")
    d.add_apt_get_package("default-jre")

    prottest_cmd = "cd /consurf/dependencies && "
    prottest_cmd += "tar -xzvf prottest-3.4.2-20160508.tar.gz && " 
    prottest_cmd += "rm prottest-3.4.2-20160508.tar.gz"
    d.add_post_command(prottest_cmd)

    # We generate two copies of the same thing. One is compiled "normally", 
    # the other comes with a doublerep flavor.
    rate4site_cmd = "cd /consurf/dependencies && "
    rate4site_cmd += "mkdir rate4site && "
    rate4site_cmd += "cp rate4site-3.0.0.tar.gz rate4site && "
    rate4site_cmd += "cd rate4site && "
    rate4site_cmd += "tar -xzvf rate4site-3.0.0.tar.gz && "
    rate4site_cmd += "cd rate4site-3.0.0 && "
    rate4site_cmd += "./configure CXXFLAGS=\"--std=c++98\" && make && "
    rate4site_cmd += "rm /consurf/dependencies/rate4site/rate4site-3.0.0.tar.gz"
    d.add_post_command(rate4site_cmd)

    rate4site_cmd = "cd /consurf/dependencies && "
    rate4site_cmd += "mkdir rate4site_doublerep && "
    rate4site_cmd += "cp rate4site-3.0.0.tar.gz rate4site_doublerep && "
    rate4site_cmd += "cd rate4site_doublerep && "
    rate4site_cmd += "tar -xzvf rate4site-3.0.0.tar.gz && "
    rate4site_cmd += "cd rate4site-3.0.0 && "
    rate4site_cmd += "./configure CXXFLAGS=\"--std=c++98 -DDOUBLEREP\" && "
    rate4site_cmd += "make && "
    rate4site_cmd += "rm /consurf/dependencies/rate4site_doublerep/rate4site-3.0.0.tar.gz && "
    rate4site_cmd += "rm /consurf/dependencies/rate4site-3.0.0.tar.gz"
    d.add_post_command(rate4site_cmd)

    d.add_env_command("export PERL5LIB=/consurf")
    d.add_env_command("export CONSURFCONF=/consurf/consurfrc.default")

    return d


def build(dependencies, base_image):
    """Collects dependencies and creates content for Singularity definition file

    :param dependencies: List of dependencies
    :type dependencies: :class:`list` of :class:`dependency`
    :param base_image: Base image that will be pulled from Dockerhub. Raises an
                       error for anything else than debian based images. 
    :type base_image: :class:`str`
    :returns: Content of Singularity definition file
    :rtype: :class:`list` of :class:`str`
    """

    if "debian" not in base_image:
        raise RuntimeError("Only tested on Debian base images")

    singularity_def = ["BootStrap: docker",
                       f"From: {base_image}"]

    # files section
    ###############
    files = list()
    for d in dependencies:
        files += d.files
    if len(files) > 0:
        # only add section if there are files
        singularity_def.append("%files")
        for f in files:
            singularity_def.append(f"{f[0]} {f[1]}")

    # post section
    ##############
    # install packages with package manager
    apt_get_packages = set()
    for d in dependencies:
        for p in d.apt_get_packages:
            apt_get_packages.add(p)
    singularity_def.append("%post")
    singularity_def.append("export DEBIAN_FRONTEND=noninteractive")
    singularity_def.append("apt-get update -y")
    apt_get_packages = set()
    for d in dependencies:
        for p in d.apt_get_packages:
            apt_get_packages.add(p)
    apt_get_packages.add("locales")
    singularity_def.append("apt-get install -y " + ' '.join(list(apt_get_packages)))

    # set locale
    singularity_def.append("echo \"LC_ALL=en_US.UTF-8\" >> /etc/environment")
    singularity_def.append("echo \"en_US.UTF-8 UTF-8\" >> /etc/locale.gen")
    singularity_def.append("echo \"LANG=en_US.UTF-8\" > /etc/locale.conf")
    singularity_def.append("locale-gen en_US.UTF-8")

    # build commands
    for d in dependencies:
        singularity_def += d.post_commands

    # environment section
    #####################
    env_commands = list()
    for d in dependencies:
        env_commands += d.env_commands
    if len(env_commands) > 0:
        # only add section if there are any env commands
        singularity_def.append("%environment")
        singularity_def += env_commands

    return singularity_def


def _parse_args():
    desc = "Builder for Singularity Definition File."
    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("--def_out", type=str, required=True,
                        help="Singularity definition file output")
    parser.add_argument("--add_foldx", action="store_true",
                        help="Whether to add FoldX to container. Required "
                        "files must be in directory specified by "
                        "--foldx_dependency_dir")
    parser.add_argument("--foldx_dependency_dir", type=str,
                        default="dependencies/foldx",
                        help="Directory containing all FoldX related files")
    parser.add_argument("--add_provean", action="store_true",
                        help="Whether to add PROVEAN to container. Required files "
                        "must be in directory specified by "
                        "--provean_dependency_dir")
    parser.add_argument("--provean_dependency_dir", type=str,
                        default="dependencies/provean",
                        help="Directory containing all PROVEAN related files")
    parser.add_argument("--add_plip", action="store_true",
                        help="Whether to add PLIP to container. Required files "
                        "must be in directory specified by "
                        "--plip_dependency_dir")
    parser.add_argument("--plip_dependency_dir", type=str,
                        default="dependencies/plip",
                        help="Directory containing all PLIP related files")
    parser.add_argument("--add_consurf", action="store_true",
                        help="Whether to add ConSurf to Container. Required "
                        "files must be in directory specified by "
                        "--consurf_dependency_dir")
    parser.add_argument("--consurf_dependency_dir", type=str,
                        default="dependencies/consurf")
    return parser.parse_args()


def main():

    args = _parse_args()

    base_image = "debian:bullseye"
    base_image_python_version = "3.9"

    dependencies = [get_base_dependency("2.3.1", "3.2.1", "7.6.0", "v3.3.0",
                                        "13-45111", base_image_python_version)]

    if args.add_foldx:
        dependencies.append(get_foldx_dependency(args.foldx_dependency_dir))

    if args.add_provean:
        dependencies.append(get_provean_dependency(args.provean_dependency_dir))

    if args.add_plip:
        dependencies.append(get_plip_dependency(args.plip_dependency_dir,
                                                "v2.2.2", "openbabel-3-0-0"))

    if args.add_consurf:
        dependencies.append(get_consurf_dependency(args.consurf_dependency_dir))

    singularity_def = build(dependencies, base_image)

    with open(args.def_out, 'w') as fh:
        fh.write(os.linesep.join(singularity_def))

    return 0

if __name__ == '__main__':
    sys.exit(main())
