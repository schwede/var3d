#!/usr/bin/env ost

"""Run PLIP.

This action runs PLIP (the Protein-Ligand Interaction Profiler
<https://github.com/ssalentin/plip>) on a PDBIzed structure in PDB
format.

WARNING: This action assumes that ligands are in the _ chain. As of 2020 this
holds true for all our structures, be it SMTL biounits, SWISS-MODEL homology
models or SMR entries.

For performance reason, the structure is split in chunks with only a few
(--split) ligands in it, together with residues within --select-proximity.
Unless --split is <= 0, polymer ligands (peptides, sugars) are kept in separate
chunks of their own regardless of the value of --split.

"""

import collections
try:
    import ujson as json
except ImportError:
    import json
import os
import shutil
import subprocess
import sys
import tempfile

from argparse import ArgumentParser
import plip.basic.config
import ost

# user must set all relevant arguments manually if config unavailable
try:
    from sm import config
except ImportError:
    config = None

# try to import plip_helpers at default SWISS-MODEL location, use
# relative import as a fallback
try:
    from sm.core.plip_helpers import PLIPModel
except:
    from plip_helpers import PLIPModel


PDB_MAX_ATOMS = 99999 # Limit set by PDB
USAGE = "usage: sm run-plip [-j|--json] [--select-proximity=DIST] " \
        "[--job_status <status_file>] [plip options] --file <PDB_FILE>"

def _DoWriteStatus(filename, msg):
    """Write a status message
    """
    # only import cmptcmpt if we actually need it
    from sm.core import cmptcmpt
    cmptcmpt.WriteStatus(filename, msg)

def _DontWriteStatus(filename, msg):
    """Skip writing a status message
    """
    pass

_WriteStatus = _DoWriteStatus

def _ParseArgs(args):

    # set defaults to None if config unavailable - needs manual checking after
    # argument parsing whether they have been provided by the user
    plip_bin_default = None
    select_proximity_default = None
    split_default = None
    if config is not None:
        plip_bin_default = config.PLIP_BIN
        select_proximity_default = config.PLIP_PROXIMITY_SELECT_DISTANCE
        split_default = config.PLIP_SPLIT_CHUNK_SIZE

    """Parse options."""
    parser = ArgumentParser(usage=USAGE)

    # SM options
    parser.add_argument('--plip-bin',
                        default=plip_bin_default,
                        help="PLIP executable")
    parser.add_argument('--job_status',
                        default=None,
                        help="Change SGE status")
    parser.add_argument('--file', '-f',
                        required=True,
                        help="set input file")
    parser.add_argument('--select-proximity',
                        default=select_proximity_default,
                        type=float,
                        help="Select only atoms within this distance to the "
                             "ligand. Set to 0 or negative to skip selection "
                             "and pass the complete structure.")
    parser.add_argument('--select-proximity-residues', default=True,
                        help="Select whole residues with at least on atoms in "
                             "proximity to the ligand.")
    parser.add_argument("-o", "--out",
                        dest="outpath",
                        default="./")
    parser.add_argument("--name",
                        dest="outputfilename",
                        default="report",
                        help="Set a filename for the report TXT and XML files. "
                             "Will only work when processing single"
                             " structures.")
    parser.add_argument("-x", "--xml",
                        dest="xml",
                        default=False,
                        help="Generate report file in XML format",
                        action="store_true")
    parser.add_argument("-j", "--json",
                        dest="json",
                        default=False,
                        help="Generate report file in JSON format",
                        action="store_true")
    parser.add_argument("-s", "--split",
                        dest="split",
                        default=split_default,
                        type=int,
                        help="Split PDB files into chunks with this number of "
                             "ligands. Set to 0 or a negative value to "
                             "disable.")
    parser.add_argument('-k', '--keep',
                        action="store_true",
                        help="Keep temporary files (PDB and XML)")
    parser.add_argument('-v', '--verbose',
                        dest='verbosity',
                        action='append_const',
                        const=1,
                        default=[ost.GetVerbosityLevel()],
                        help="increase verbosity. Can be used multiple times.")
    parser.add_argument('-q', '--quiet',
                        dest='verbosity',
                        action='append_const',
                        const=-1,
                        help="decrease verbosity. Can be used multiple times.")

    # PLIP options are left as-is by parse_known_args
    opts, plip_args = parser.parse_known_args(args)

    # check if user provided required arguments if smng config not available
    if config is None:
        if opts.select_proximity_residues is None or opts.split is None \
         or opts.plip_bin is None:
            raise RuntimeError("Must set --plip-bin, --select-proximity and "
                               "--split if smng config is not available.")

    # Set verbosity
    opts.verbosity = sum(opts.verbosity)
    if opts.verbosity > 3:
        plip_args.append("-v")
    elif opts.verbosity <= 3:
        plip_args.append("-q")
    elif opts.verbosity < 2:
        plip_args.append("-s")


    # Set empty function in case we don't have a status file
    if opts.job_status is None:
        global _WriteStatus
        _WriteStatus = _DontWriteStatus

    return opts, plip_args

def IsAllNucleotideChain(chain):
    """ Test if a chain contains only nucleotide residues."""
    for residue in chain.residues:
        # Include anything that has any non-Nucleotide residue
        if residue.chem_type != 'N':
            return False
    return True

def RemoveDNAOnlyChains(entity):
    """ Returns an EntityView with the chains containing only DNA residues
     removed.
    """
    view = entity.CreateEmptyView()
    for chain in entity.chains:
        if chain.name == '_': # Include ligand
            view.AddChain(chain, ost.mol.INCLUDE_ALL)
        elif chain.name == '-': # Include water
            view.AddChain(chain, ost.mol.INCLUDE_ALL)
        elif not IsAllNucleotideChain(chain):
            view.AddChain(chain, ost.mol.INCLUDE_ALL)
        else:
            pass # ignore DNA/RNA chain
    return view

def FindDuplicates(collection):
    """ Find duplicate values in collection.

    :rtype: :set
    """
    seen = set()
    duplicates = set()
    for x in collection:
        if x in seen:
            duplicates.add(x)
        seen.add(x)
    return duplicates

def ListPolymerLigands(ligand_chain):
    """ Determines the numeric-part of residue numbers that are polymer
    ligands.

    Polymer ligands are represented in the ligand chain _ as residues
    sharing the same numeric part of the residue number, with different
    insertion codes. Typically this will be either peptide or sugar
    chains.

    :param ligand_chain: an OST ChainHandle or ChainView containing ligands
    :returns: a list of numeric residue numbers that are polymer ligands
    :rtype: :list(:int)
    """

    # Get the numeric-part of ligand residues
    resnums = [r.number.num for r in ligand_chain.residues]
    # Find duplicates - those are polymer ligands
    dupes = FindDuplicates(resnums)
    LogInfo("Found %s polymer ligands: %s" % (
        len(dupes), ", ".join([str(x) for x in dupes])))
    return dupes

def SelectProximity(entity, opts):
    """ Returns an EntityView containing only atoms in proximity to the ligand.


    Inputs: an ost.Mol.Entity
    opts: an object, normally the arguments from ArgParser, with attributes:
        - select_proximity
        - select_proximity_residues
    """
    if opts.select_proximity <= 0:
        return entity

    flags = ost.mol.QueryFlag.MATCH_RESIDUES \
        if opts.select_proximity_residues \
        else 0
    ligand_chains_string = "cname='_'"
    return entity.Select("%s <> [ %s ]" % (
        opts.select_proximity, ligand_chains_string), flags)

def SelectLigandChunk(entity, chunk):
    """ Returns an EntityView containing only ligand residues in chunk.
    The view contains all other chains including water.

    Inputs: an ost.Mol.Entity
    chunk: an list of residue numbers
    """
    rnum_string = " or ".join(["rnum=%s"%residue for residue in chunk])
    return entity.Select("cname!='_' or %s" % rnum_string)

def MergeJSON(json_list):
    """ Merges a list of PLIP ToDict outputs into a single output.

    Input: json_list, a list of dict structures produced by PLIPModel.ToDict.

    Output: the merged dict
    """
    merged_json = {}
    for selection_json in json_list:
        for ligand_name, ligand_list in list(selection_json.items()):
            merged_json.setdefault(ligand_name, [])
            merged_json[ligand_name] = merged_json[ligand_name] + ligand_list
    return merged_json

def DetectPeptideLigand(selection, resnum):
    """Given an OST selection with a single polymer ligand in the _ chain,
    return True if this ligand is a peptide, False otherwise.

    By default PLIP will ignore standard residues in a peptide ligand.
    We need to give it a --peptides argument in order for it to annotate
    peptide ligands fully. This function tries to detect when that should be
    the case.
    """
    polymer_residues = selection.FindChain("_").residues
    assert len(polymer_residues) >= 2
    for residue in polymer_residues:
        assert residue.number.num == resnum
        if residue.peptide_linking:
            ost.LogInfo("Peptide-linking residue found: %s" % str(residue))
            return True
        if residue.name in ost.conop.STANDARD_AMINOACIDS:
            ost.LogInfo("Standard amino acid residue found: %s" % str(residue))
            return True
    ost.LogInfo("Residue %s does not look like a peptide" % resnum)
    return False

def SplitAndMerge(entity, opts, plip_args, chunk_size):
    """  This function splits the input file into chunks with fewer ligands
     (chunk_size). It merges the json output and returns a single, merged json
     structure.
    """
    if opts.xml:
        ost.LogError("Cannot save XML when splitting the PDB file.")
        sys.exit(1)
    _WriteStatus(opts.job_status, 'RUNNING')

    ligand_chain = entity.FindChain("_")
    if not ligand_chain.IsValid():
        ost.LogWarning("Entry contains no ligand.")
        return {}

    polymer_ligands = ListPolymerLigands(ligand_chain)
    jsons = []
    tmpdir = tempfile.mkdtemp()

    for chunk in ChunkLigands(ligand_chain, chunk_size, polymer_ligands):
        chunk_selection = SelectLigandChunk(entity, chunk.residues)

        if chunk.isPolymerLigand:
            # detect if peptide ligand
            assert len(chunk.residues) == 1
            isPeptideLigand = DetectPeptideLigand(chunk_selection, chunk.residues[0])
        else:
            isPeptideLigand = False

        selection = SelectProximity(chunk_selection, opts)
        
        # Create a unique name for the PDB file and XML report of this chunk:
        if len(chunk.residues) > 5:
            chunk_name = "_".join([str(x) for x in chunk.residues[0:3]]) + \
                         '...' + str(chunk.residues[-1])
        else:
            chunk_name = "_".join([str(x) for x in chunk.residues])

        if selection.atom_count > PDB_MAX_ATOMS:
            ost.LogError("Too many atoms for PLIP after filtering and "
                         "splitting: %s > %s" % (
                             selection.atom_count, PDB_MAX_ATOMS))
            _WriteStatus(opts.job_status, 'FAILED')
            sys.exit(1)

        selection_pdb_file = os.path.join(tmpdir, "selection.%s.pdb"%chunk_name)
        ost.io.SavePDB(selection, selection_pdb_file)

        LogScript("Running PLIP on residues {}{}".format(
            ", ".join([str(r) for r in chunk.residues]),
            " as peptide ligand" if isPeptideLigand else " as polymer ligand" \
                if chunk.isPolymerLigand else ""))
        ExecutePLIP(opts.plip_bin,
                    plip_args,
                    selection_pdb_file, tmpdir,
                    "plip_report_%s" % chunk_name,
                    False,  # No XML
                    True,  # JSON
                    opts.job_status,
                    isPeptideLigand)

        partial_json = ReadXML(
            tmpdir,
            "plip_report_%s" % chunk_name).ToDict(entity)

        # Keep only the ligands that are part of this chunk
        # The selection bit may have selected extra ligands that will be/have
        # been processed in an other chunk. Ideally we should take this into
        # account and not re-calculate them, but that would require more
        # bookkeeping, and we'd also need to extend the selection further to
        # ensure we include also the neighboring ligands of those extra ones
        # too. This would require an algorithm that recursively extends the
        # ligand selection until no ligand is within 15A. This may defeat
        # the purpose of the splitting in the case of structures with many
        # nearby ligands that may never be separated by more than 15A, ending up
        # selecting everything. So for now we just ignore the extra ligands
        partial_json_filtered = {}
        for ligname, liglist in list(partial_json.items()):
            for ligand in liglist:
                if ligand['position'] in chunk.residues:
                    partial_json_filtered.setdefault(ligname, []).append(ligand)
                elif isPeptideLigand and ligand['position'] == 0:
                    assert ligand['ligtype'] == "PEPTIDE"
                    ligand['position'] = chunk.residues[0]
                    partial_json_filtered.setdefault(ligname, []).append(ligand)
                else:
                    ost.LogDebug("Ignoring ligand %s not in chunk %s" % (
                        ligand['position'], chunk.residues))

        jsons.append(partial_json_filtered)

    if not opts.keep:
        shutil.rmtree(tmpdir)
    else:
        ost.LogInfo("Temporary files kept in %s" % tmpdir)
    return MergeJSON(jsons)

def SaveJSON(plip_json, outpath, outputfilename):
    """ Save the PLIP JSON output in the standardized file.
    This function only avoids constructing the output file name in different
    places of this script"""
    with open(os.path.join(outpath, outputfilename + ".json"), "w") \
            as json_file:
        json.dump(plip_json, json_file)

def ReadXML(outpath, name):
    """ Reads the PLIP XML report and returns it as JSON.
    :param entity: an entity handle (view doesn't work)
    """
    plip_xml_fn = os.path.join(outpath, name + ".xml")
    return PLIPModel(plip_xml_fn)

def ExecutePLIP(plip_bin, plip_args, f, outpath, name, xml, json, job_status,
                peptide_ligand):
    """ Runs PLIP with subprocess.Popen and checks the return code"""
    plip_cmd = [plip_bin] + plip_args + \
               ['-f', f,
                '-o', outpath,
                '--name', name]

    if xml or json:
        plip_cmd.append('-x')

    if peptide_ligand:
        plip_cmd.extend(['--peptides', '_'])

    _WriteStatus(job_status, 'RUNNING')

    ost.LogVerbose(" ".join(plip_cmd))
    ps = subprocess.run(plip_cmd)
    
    if ps.returncode != 0:
        _WriteStatus(job_status, 'FAILED')
        sys.exit(ps.returncode)

def MaxResidueNumber(chain):
    """
    Returns the highest residue number of the chain.
    This may be different from the length of the chain if it contains
    any insertion codes.

    :param chain: OST chain
    :type chain: :class:`ost.Mol.ChainHandle` or :class:`ost.Mol.ChainView`
    """
    return max([residue.number.num for residue in chain.residues])

def UniqueResidueNumbers(chain):
    """
    Return the list of unique residue numbers in the chain.
    Ignores insertion codes, ie if chain contains 1, 1A and 1B,
    it will return a list with a single element: [1].

    :param chain: OST chain
    :type chain: :class:`ost.Mol.ChainHandle` or :class:`ost.Mol.ChainView`
    """
    ligand_residue_numbers = [residue.number.num for residue in chain.residues]
    return list(collections.OrderedDict.fromkeys(ligand_residue_numbers))

def UniqueResidueNames(chain):
    """
    Return the set of unique residue names in the chain.

    :param chain: OST chain
    :type chain: :class:`ost.Mol.ChainHandle` or :class:`ost.Mol.ChainView`
    """
    ligand_residue_names = [residue.name for residue in chain.residues]
    return set(ligand_residue_names)

def ChunkLigands(chain, chunk_size, polymer_ligands):
    """
    Generator that yields chunks of ligand residues. The values yielded
    are namedtuples with two attributes: residues (list of residue numbers)
    and isPolymerLigand (boolean).
    Automatically creates splits as list of residue numbers (int).
    If a peptide ligand is detected (see --peptide-ligand), the chunk
    will contain only the residue number of that peptide.
    If a peptide ligand will be excluded by PLIP (in BioLiP list and
    > 15 residues) a single chunk containing only this ligand will be
    returned.

    :param chain: OST chain
    :type chain: :class:`ost.Mol.ChainHandle` or :class:`ost.Mol.ChainView`
    :param chunk_size: the maximum size of the chunks to return. 0 to disable
    chunking. Peptide ligands will be chunked regardless of the value of
    this argument.
    :type chunk_size: :int
    :param polymer_ligands: an iterable set of non-peptide polymer
    ligands residue numbers (only the numeric part)
    :type peptide_ligands: :set<:str>
    """

    unique_ligand_residue_numbers = UniqueResidueNumbers(chain)

    # Create a named tuple to store the results
    Chunk = collections.namedtuple('Chunk', ['residues', 'isPolymerLigand'])

    # Detect and remove polymer ligands
    for polymer_ligand_resnum in polymer_ligands:
        unique_ligand_residue_numbers.remove(polymer_ligand_resnum)
        ost.LogVerbose("Processing polymer ligand %s" % (
            polymer_ligand_resnum))
        yield Chunk(residues=[polymer_ligand_resnum], isPolymerLigand=True)

    # No splitting if chunk_size == 0
    if chunk_size <= 0:
        yield Chunk(residues=unique_ligand_residue_numbers,
                    isPolymerLigand=False)
        return

    # Detect ligands that will be exluded by PLIP and yield them as a single
    # chunk. This was we can annotate them as "Excluded by PLIP" on our 
    # display. If we don't do that carefully we might end up with only
    # some of the ligands being excluded, which would look weird...
    # plip.basic.config.biolip_list contains the BioLiP list of
    # "suspicious ligands" that are considered for exclusion. Test for overlap
    # with our ligands
    unique_ligand_residue_names = UniqueResidueNames(chain)
    excludable_ligands = unique_ligand_residue_names.intersection(
        plip.basic.config.biolip_list)
    for ligand in excludable_ligands:
        # Count the number of ligands whith this residue name
        excludable_residues = [
            residue for residue in chain.residues
            if residue.name == ligand
            and residue.number.num in unique_ligand_residue_numbers]
        # We need at least 15
        if len(excludable_residues) > 15:
            excluded_residues = []
            for residue in excludable_residues:
                excluded_residues.append(residue.number.num)
                unique_ligand_residue_numbers.remove(residue.number.num)
            # Now split excluded residues in chunks of at least 15
            # If we split in chunks of < 15, PLIP won't exclude them
            while len(excluded_residues) > 30:
                chunk = excluded_residues[-15:]
                del excluded_residues[-15:]
                ost.LogVerbose("Processing ligands considered for exclusion:"
                               " %s" % chunk)
                yield Chunk(residues=chunk, isPolymerLigand=False)
            ost.LogVerbose("Processing ligands considered for exclusion:"
                           " %s" % excluded_residues)
            yield Chunk(residues=excluded_residues, isPolymerLigand=False)

    # Chunk the remaining residues
    chunk = []
    for residue_number in unique_ligand_residue_numbers:
        if len(chunk) == chunk_size:
            ost.LogVerbose("Processing chunk of %s ligands: %s" % (
                chunk_size, chunk))
            yield Chunk(residues=chunk, isPolymerLigand=False)
            chunk = [] # clear()
        chunk.append(residue_number)

    # Chunk the final residues if needed
    if chunk:
        ost.LogVerbose("Processing last chunk of %s ligands: %s" % (
            len(chunk), chunk))
        yield Chunk(residues=chunk, isPolymerLigand=False)

def RunPLIP(opts, plip_args):
    try:
        ost.LogVerbose("Reading file %s" % opts.file)
        in_entity = ost.io.LoadPDB(opts.file)
    except Exception as e:
        ost.LogError(str(e))
        sys.exit(1)

    merged_json = SplitAndMerge(in_entity, opts, plip_args, opts.split)
    SaveJSON(merged_json, opts.outpath, opts.outputfilename)
    _WriteStatus(opts.job_status, 'COMPLETED')

def _main():
    #  We need arguments
    if len(sys.argv) < 1:
        print(USAGE)
        sys.exit(-1)

    opts, plip_args = _ParseArgs(sys.argv[1:])
    ost.PushVerbosityLevel(opts.verbosity)

    RunPLIP(opts, plip_args)

if __name__ == '__main__':
    # make script 'hot'
    #unbuffered = os.fdopen(sys.stdout.fileno(), 'w', 0)
    #sys.stdout = unbuffered
    _main()

## Emacs magic
# Local Variables:
# mode: python
# End:
