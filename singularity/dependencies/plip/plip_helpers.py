from copy import deepcopy
import os
import subprocess
import tempfile
try:
    import ujson as json
except ImportError:
    import json

import ost
from plip.exchange import xml as plipxml

class _NoCoordinatesTransform():
    """ A dummy class for coordinates transform that makes no transformation."""
    def TransformCoordinates(self, coord):
        return coord

class PLIPModel(plipxml.PlipXML):
    """ Representation of PLIP output data."""
    
    colors = {
        'hydrophobic_interactions': [128, 128, 128],
        'hydrogen_bonds': [0, 0, 255],
        'water_bridges': [191, 191, 255],
        'pi_stacks': [0, 255, 0],
        #'pi-Stacking (parallel)     [0, 255, 0]     green     dashed line
        #'pi-Stacking (perpendicular)     [140, 179, 102]     smudge     dashed line
        'pi_cation_interactions': [255, 128, 0],
        'halogen_bonds':  [54, 255, 191],
        'salt_bridges': [255, 255, 0],
        'metal_complexes': [140, 64, 153]
        }
    humanReadableInteractions = {
        'hydrophobic_interactions': "Hydrophobic interactions",
        'hydrogen_bonds': "Hydrogen bonds",
        'water_bridges': "Water bridges",
        'salt_bridges': "Salt bridges",
        'pi_stacks': "pi-Stacking",
        'pi_cation_interactions': "pi-Cation interactions",
        'halogen_bonds': "Halogen bonds",
        'metal_complexes': "Metal complexes"
    }
    _default_chain = {'residues': [], 'interactions': {}}
    interaction_keys_all = ['resnr_lig', 'restype_lig', 'reschain_lig']
    interaction_keys_extra = {
        'hydrophobic_interactions': interaction_keys_all + ['ligcarbonidx', 'protcarbonidx'],
        'hydrogen_bonds': interaction_keys_all + ['sidechain', 'dist_h_a', 'dist_d_a', 'don_angle', 'protisdon', 'donoridx', 'acceptoridx', 'donortype', 'acceptortype'],
        'water_bridges': interaction_keys_all + ['dist_a_w', 'dist_d_w', 'don_angle', 'water_angle', 'protisdon', 'donor_idx', 'acceptor_idx', 'donortype', 'acceptortype', 'water_idx', 'watercoo'],
        'salt_bridges': interaction_keys_all + ['protispos', 'lig_group', 'lig_idx_list'],
        'pi_stacks': interaction_keys_all + ['centdist', 'angle', 'offset', 'type', 'lig_idx_list'],
        'pi_cation_interactions': interaction_keys_all + ['offset', 'protcharged', 'lig_group', 'lig_idx_list'],
        'halogen_bonds': interaction_keys_all + ['don_angle', 'acc_angle', 'donortype', 'acceptortype', 'don_idx', 'acc_idx', 'sidechain'],
        'metal_complexes': interaction_keys_all + ['metal_idx', 'metal_type', 'target_idx', 'target_type', 'coordination', 'location', 'rms', 'geometry', 'complexnum', 'targetcoo', 'metalcoo']
        }
    
    def __init__(self, plip_xml_file, transform = _NoCoordinatesTransform()):
        """ Contructor from a the XML file name.
        
            :param plip_xml_file: the PLIP XML report file name
            :type plip_xml_file: :str
            :param transform: An object with a `TransformCoordinates` method. 
                Defaults to :class:`_NoCoordinatesTransform` which makes no transform
            :type transform: :object, for instance a :class:`~sm.model.HomologyModel`
        """
        self.transform = transform
        plipxml.PlipXML.__init__(self, plip_xml_file)
    
    @classmethod
    def CreateFromModel(cls, model):
        """ Contructor from a :class:`~sm.model.HomologyModel`
        
            :param model: the model
            :type plip_xml_file: :class:`~sm.model.HomologyModel`
        """
        plip_xml_file = cls.GetPLIPReportModelPath(model)
        return PLIPModel(plip_xml_file, model)

    @staticmethod
    def GetPLIPOutputModelPath(model):
        """ Get the path of the folder containing the PLIP output (for a model)"""
        return model.path
    
    @staticmethod
    def GetPLIPReportModelName(model):
        """ Get the path of the PLIP XML report (for a model)"""
        return "plip_report"
    
    @staticmethod
    def GetPLIPReportModelPath(model):
        """ Get the path of the PLIP XML report (for a model)"""
        return os.path.join(PLIPModel.GetPLIPOutputModelPath(model), PLIPModel.GetPLIPReportModelName(model) + ".xml")
    
    @staticmethod
    def GetInteractingChains(bsite):
        """ Returns a list of the chains interacting with this binding site"""
        return [chain for chain in bsite.interacting_chains if chain != '_']
    
    @classmethod
    def GetColor(cls, type):
        """ Returns the RGB color corresponding to an interaction.
        The colors are taken from PLIP directly:
        https://github.com/ssalentin/plip/blob/stable/DOCUMENTATION.md#interactions
        
        :param type: The high-level type of interaction
        :type type: :str
        :returns: the RGB color
        :rtype: :list
        :raises KeyError: if type isn't known
        """
        return cls.colors[type]
     
    def ToDict(self, struc = None):
        """ Return a simplified representation of the PLIP report in a dictionary without objects
        
            :param struc: (optional) OST structure to be used to annotate if bonds are redundant (i.e.
                                     already reported as CONNECT entries in the PDB file.
            :type struc: :class:`~ost.mol.EntityHandle`
        """
        dict_out = {}
        compound_lib = ost.conop.GetDefaultLib()
        
        # Fix exluded ligands
        #self.excluded = self.getdata(self.doc, '/report/excluded_ligands/excluded_ligand')
        self.excluded = self.doc.xpath('/report/excluded_ligands/excluded_ligand/text()')
        if self.excluded:
            for excluded_ligand in self.excluded:
                if struc is None:
                    id = "{0}:_:{1}".format(excluded_ligand, 0)
                    dict_out[excluded_ligand] = [{
                            "hetid": excluded_ligand,
                            "name": excluded_ligand,
                            "composite": False,
                            "numInteractions": 0,
                            "members":[id],
                            "position": 0,
                            "chains": {},
                            "id": id,
                            "ligtype":"EXCLUDED",
                            'excluded': True
                    }]
                else:
                    for residue in struc.FindChain("_").residues:
                        if residue.name == excluded_ligand:
                            position = residue.number.num
                            id = "{0}:_:{1}".format(excluded_ligand, position)
                            jligand = {
                                "hetid": excluded_ligand,
                                "name": excluded_ligand,
                                "composite": False,
                                "numInteractions": 0,
                                "members":[id],
                                "position": position,
                                "chains": {},
                                "id": id,
                                "ligtype":"EXCLUDED",
                                'excluded': True
                            }
                            dict_out.setdefault(excluded_ligand,[]).append(jligand)
        
        for key, bsite in sorted(list(self.bsites.items()), key=lambda x: x[1].position):
            # Peptide ligands are named 'peptide ligand' in annotation db:
            if bsite.ligtype == "PEPTIDE":
                name = "peptide ligand"
            else:
                name = bsite.longname
                
            jligand = {
                'id': bsite.bsid,
                'name': name,
                'position': bsite.position,
                'ligtype': bsite.ligtype,
                'chains': {},
                'numInteractions': len([x for x in bsite.bindingsite.xpath('.//resnr/text()')]),
                'composite': bsite.composite,
                'hetid': bsite.hetid,
                'members': bsite.members,
                'excluded': False
            }
            
            for residue in bsite.bs_res:
                if residue['contact']:
                    chain = residue['reschain']
                    jresidue = {
                        'position': residue['resnr'],
                        'three_letters': residue['aa'],
                        'chain': chain,
                        'one_letter': compound_lib.FindCompound(residue['aa']).one_letter_code
                    }
                    #jsite['chains'].append(residue['reschain'])
                    jligand['chains'].setdefault(chain, deepcopy(self._default_chain))['residues'].append(jresidue)
            for chain, residues in list(jligand['chains'].items()):
                jligand['chains'][chain]['residues'].sort(key=lambda x: x['position'])
            
            # Add interactions
            jligand = self._AddInteractions(jligand, bsite.hydrophobics, 'hydrophobic_interactions', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.hbonds, 'hydrogen_bonds', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.wbridges, 'water_bridges', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.sbridges, 'salt_bridges', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.pi_stacks, 'pi_stacks', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.pi_cations, 'pi_cation_interactions', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.halogens, 'halogen_bonds', compound_lib, struc)
            jligand = self._AddInteractions(jligand, bsite.metal_complexes, 'metal_complexes', compound_lib, struc)
            # Count interactions per chain
            for chain, c in list(jligand['chains'].items()):
                numInteractions = 0
                for interactionType, i in list(jligand['chains'][chain]['interactions'].items()):
                    numInteractions += len(jligand['chains'][chain]['interactions'][interactionType])
                jligand['chains'][chain]['numInteractions'] = numInteractions
            
            dict_out.setdefault(name,[]).append(jligand)
        
        return dict_out
            
    def ToJSON(self, struc = None):
        """Return a simplified representation of the PLIP as a JSON string (str)
        
            :param struc: (optional) OST structure to be used to annotate if bonds are redundant (i.e.
                                     already reported as CONNECT entries in the PDB file.
            :type struc: :class:`~ost.mol.EntityHandle`
        """
        return json.dumps(self.ToDict(struc))

    def _AddInteractions(self, jligand, interactions, itype, compound_lib, struc):
        """ Add interactions to the ligand passed as first argument
        
            :param jligand: ligand dictionary to add the interactions to
            :type jligand: :dict`
            :param interactions: interactions to add
            :type interactions: :class:`~plip.plipxml.Interaction`
            :param itype: interaction type, such as hydrogen_bonds, etc.
            :type itype: :str`
            :param compound_lib: OST compound library to annotate one_letter_code
            :type compound_lib: :class:`~ost.conop.CompoundLib`
            :param struc: (optional) OST structure to be used to annotate if bonds are redundant (i.e.
                                     already reported as CONNECT entries in the PDB file.
            :type struc: :class:`~ost.mol.EntityHandle`
        """
        iname =  self.humanReadableInteractions[itype];
        for interaction in interactions:
            bond_id = jligand['id'] + "_" + itype + "_" + interaction.id;
            chain = interaction.reschain
            ibond = {
                    'bond_id': bond_id,
                    'chain': chain,
                    'type': itype,
                    'htype': iname,
                    'distance': interaction.dist,
                    'position': interaction.resnr,
                    'three_letters': interaction.restype,
                    'color': self.GetColor(itype)
                }
            if (itype == 'metal_complexes'):
                ibond['ligcoo'] = self.transform.TransformCoordinates(interaction.metalcoo)
                ibond['protcoo'] = self.transform.TransformCoordinates(interaction.targetcoo)
            else:
                ibond['ligcoo'] = self.transform.TransformCoordinates(interaction.ligcoo)
                ibond['protcoo'] = self.transform.TransformCoordinates(interaction.protcoo)
            if ( chain == '_' or chain == '-') and itype == 'metal_complexes':
                # That's the only case where we can have ligand-ligand
                # interactions. In this case the one letter code is unreliable,
                # fall back to the 3 letters
                ibond['one_letter'] = ibond['three_letters']
            else:
                ibond['one_letter'] = compound_lib.FindCompound(interaction.restype).one_letter_code
            ibond['extra'] = dict((key, getattr(interaction, key)) for key in self.interaction_keys_extra[itype])
            jligand['chains'].setdefault(chain, deepcopy(self._default_chain))['interactions'].setdefault(iname, []).append(ibond)
            # Find out if the bond is already present in the PDB structure
            # This happens with Heme molecules for instance
            if struc is not None:
                if itype == 'metal_complexes':
                    a1 = struc.FindWithin(ost.geom.Vec3(interaction.metalcoo[0], interaction.metalcoo[1], interaction.metalcoo[2]), 0.001)
                    a2 = struc.FindWithin(ost.geom.Vec3(interaction.targetcoo[0], interaction.targetcoo[1], interaction.targetcoo[2]), 0.001)
                else:
                    a1 = struc.FindWithin(ost.geom.Vec3(interaction.ligcoo[0], interaction.ligcoo[1], interaction.ligcoo[2]), 0.001)
                    a2 = struc.FindWithin(ost.geom.Vec3(interaction.protcoo[0], interaction.protcoo[1], interaction.protcoo[2]), 0.001)
                if a1 and a2:
                    ibond['redundant'] = a1[0].FindBondToAtom(a2[0]).IsValid()
                else: # One of the end of the bond is not an atom
                    ibond['redundant'] = False
            else:
                ibond['redundant'] = None
                
        return jligand

def GetPLIPLigands(model):
    """Get ligands from PLIP. Returns PLIP ligand annotation as a
    JSON-compatible dictionary

    :param model: model to check
    :type model: :class:`~sm.pipeline.HomologyModel`
    """
    try:
        plip_json_fn = os.path.join(model.path, "plip_report.json")
        with open(plip_json_fn) as plip_json_fd:
            plip_json = json.load(plip_json_fd)
        
        # Transform coordinates
        for ligname, unique_ligand in list(plip_json.items()):
            for ligand in unique_ligand:
                for chain_name, chain in list(ligand['chains'].items()):
                    for ikey, itype in list(chain['interactions'].items()):
                        for interaction in itype:
                            interaction['protcoo'] = model.TransformCoordinates(interaction['protcoo'])
                            interaction['ligcoo'] = model.TransformCoordinates(interaction['ligcoo'])
                            # Also transform 'extra' annotations
                            for key, value in list(interaction['extra'].items()):
                                if key.endswith("coo") and len(value) == 3:
                                    value = model.TransformCoordinates(value)
        # Save
        return plip_json
    except IOError: # Most likely plip wasn't run for this model
        return {}


def GetPLIPJSON(pdb_file, work_dir=None, name="plip_report"):
    """
    Run PLIP synchronously on the input structure with the typical arguments
    and returns the JSON data.\
    :arg pdb_file: the PDB file to analze, in .pdb or .pdb.gz format.
    :arg work_dir: an optional temporary directory to save the intermediate data
    :arg name: an optional name prefix for the temporary report file

    Return can be:
    - A dictionary with PLIP annotation
    - An empty dict if no ligand was detected
    - None in case of an error
    """
    plip_json = {}

    if work_dir is None:
        work_dir = tempfile.mkdtemp()
        remove_work_dir = True
    else:
        remove_work_dir = False
    plip_json_fn = os.path.join(work_dir, '%s.json' % name)

    plip_cmd = GetPLIPCommand(pdb_file, work_dir, name)
    ost.LogScript('Running PLIP command %s' % " ".join(plip_cmd))
    ps = subprocess.Popen(plip_cmd, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
    stdout, stderr = ps.communicate()

    if ps.returncode != 0:
        ost.LogError("PLIP command '%s' failed with exit code %s" % (
        " ".join(plip_cmd), ps.returncode))
        ost.LogError(stderr)
        plip_json = None
    else:
        try:
            with open(plip_json_fn) as json_in:
                plip_json = json.load(json_in)
        except IOError as e:
            ost.LogError("IOError reading PLIP JSON report ({0}): {1}".format(e.errno,
                                                                        e.strerror))
            plip_json = None
        except ValueError as e:
            ost.LogError("ValueError reading PLIP JSON report: {0}".format(str(e)))
            plip_json = None
    try:
        os.remove(plip_json_fn)
    except (IOError, OSError):
        pass # PLIP command probably crashed before creating the output file
    if remove_work_dir:
        os.rmdir(work_dir)

    return plip_json


def create_plip_template_annotation(plip_annot, lig_resname, position):
    """ Create a template-friendly PLIP annotation to be added to the ligand annotation
    (included_ligands in models, ligand.chains in SMTL).
    
    Currently the PLIP data "lives" separate from the ligand annotation which makes it harder
    to digest in a template where fetching keys in a different dictionary / list than the one
    that is currently being looped on is difficult. This function extracts the information for
    a single ligand molecule, and returns a dictionary entry with the relevant annotation
    for the ligand.
        
    :param plip_annot: The PLIP annotation JSON, as returned by PlipXML. For instance biounit.plip or
    :type plip_annot: :dict
    
    :param lig_resname: The residue(s) name(s) of the ligands
    :type lig_resname :str or :list[:str]
    
    :param position: the ligand position or residue number. chain.name in the SMTL, ligand['number'] in the models
    :type position: :int

    :returns:
        - the "PLIP" ligand annotation as a dictionary for template rendering, if any annotation was found by PLIP
        - None if the plip annotation was missing (plip not run or other error)
        - False if no plip information was found for this ligand (no ligand interaction detected)
        
        For a model this would be saved to: mdl_details['model'].included_ligands[i]['ligands']['plip'].
        For an SMTL entry: bio_unit.ligands[i].chains[j].plip
    """
    lig_name = "-".join(lig_resname)
    position = int(position) # Don't trust input type
    if plip_annot is None: # No PLIP in this template
        lig_annot = None
    elif lig_name in plip_annot: # Exact match of the ligand name
        lig_plip = [lig for lig in plip_annot[lig_name] if lig['position'] == position]
        if len(lig_plip) == 1:
            lig_annot = _add_plip_ligand_info(False, lig_plip[0], lig_name)
        else:
            lig_annot = None # Nothing detected at that position
    else: # PLIP didn't detect any interaction with this ligand
        lig_annot = None
        # It is possible that we have a composite ligand. Try to get by ligand number
        for lig_name_plip, ligand_separate in list(plip_annot.items()):
            lig_plip = [lig for lig in ligand_separate if lig['position'] == position]
            if len(lig_plip) == 1:
                lig_annot = _add_plip_ligand_info(lig_annot, lig_plip[0], lig_name)
    return lig_annot


def _add_plip_ligand_info(lig_annot, lig_plip, lig_name):
    """ Add the given PLIP ligand interactions to the ligand annotation
    
    :param lig_annot: The ligand annotation to add plip to, or a Falsy value to create a new annotation
    :type lig_annot: :dict`
    
    :param lig_plip: the subset of the 'plip_annot' JSON corresponding the the ligand name
        and position that we want to add to the ligand annotations.
    :type lig_plip: :dict
    
    :param lig_name: the ligand name as found in the ligand annotation
    :type lig_name: :str

    :returns: the modified lig_annot
    """
    if not lig_annot:
        lig_annot = {
            'numInteractions': {},
            'interactions': {},
            'name': lig_name,
            'position': lig_plip['position']
        }
    
    lig_annot['excluded'] = 'excluded' in lig_plip and lig_plip['excluded']
    for key, plip_chain in list(lig_plip['chains'].items()):
        lig_annot['numInteractions'][key] = 0
        for itype_key, itype_values in list(plip_chain['interactions'].items()):
            nonredundant_bonds = [i for i in itype_values if not i['redundant']]
            if len(nonredundant_bonds) > 0:
                lig_annot['numInteractions'][key] += len(nonredundant_bonds)
                lig_annot['interactions'].setdefault(itype_key, []).extend(nonredundant_bonds)
    return lig_annot


def GetPLIPCommand(pdb_file, work_dir, name):
    """
    Get a standard command for the sm run-plip action to generate a json
    :param pdb_file: a coordinate file in pdb or pdb.gz format
    :param work_dir: a directory where we will store the results
    :param name: the output name prefix. The final file will be named
        <work_dir>/<name>.json
    :return: the command as a list
    """
    try:
        from sm import config
    except ImportError:
        raise RuntimeError("GetPLIPCommand can only be called from an sm "
                           "environment, importing sm modules failed...")
    sm_bin = os.path.join(config.SM_ROOT_DIR, 'bin', 'sm')
    cmd=[sm_bin, 'run-plip',
         '-f', pdb_file,
         '--name', name,
         '-o', work_dir,
         '-j', # JSON output
         '--nofixfile', # Don't save the plipfixed*.pdb file
         '--nopdbcanmap'
         ]
    return cmd


__all__ = ('PLIPModel', 'GetPLIPJSON', 'GetPLIPCommand')