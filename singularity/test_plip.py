import sys
import json

from ost import seq
from ost import io

from var3d import base
from var3d import struct_anno
from var3d import pipeline

if len(sys.argv) != 1:
    print("USAGE: v3d <VAR3D_IMAGE> test_plip.py")
    sys.exit(42)

plip_anno = struct_anno.PLIPStructAnno()
anno_pipeline = pipeline.AnnotationPipeline(struct_anno=[plip_anno])

# crambin, the mother of all sequences
s = seq.CreateSequence("A", "RPYACPVESCDRRFSDSSNLTRHIRIHT")

# Fantastic structure importer
class YoloStructImporter(base.StructImporter):
    def Run(self, sequence, seq_range=None):
        if sequence.GetName() == "A":
            crambin = io.LoadPDB("zinc_finger.pdb")
            structure = base.Structure(s, "zinc_finger", crambin,"cname=A")
            return [structure]
        else:
            return []
    def Key(self):
        return "yolo"

s_imp = YoloStructImporter()
import_pipeline = pipeline.DataImportPipeline(struct_importer = [s_imp])
data = pipeline.DataContainer(s)
data = import_pipeline.Import(data)
anno = anno_pipeline.Run(data)
with open("plip_anno.json", 'w') as fh:
    json.dump(anno, fh)

