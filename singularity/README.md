var3d and Singularity
=====================

var3d is intended to be used as a containerized solution. Container building is
a two step process. 1) generate a Singularity definition file 2) build the
container.

Generate a singularity definition file
--------------------------------------

`def_builder.py` generates a singularity definition file. Without setting any flags,
you'll get a container with var3d base capabilities:

```
$ python3 def_builder.py --def_out <DEF_FILE> 
``` 

Adding external dependencies gradually adds annotation capabilities. 

* PLIP: add `--add_plip` flag to make use of struct_anno.PLIPStructAnno.
  Requires two files as dependency that we ship in `dependencies/plip`. No need
  to change `--plip_dependency_dir` from its default.

* PROVEAN: add `--add_provean` flag to make use of
  var_seq_anno.ProveanVarSeqAnno. PROVEAN requires the psiblast and
  blastdbcmd executables as dependency. It seems to segfault with executables
  from newer BLAST versions. We therefore directly ship them in version 2.4.0.
  No need to change `--provean_dependency_dir` from its default.

* FoldX: add `--add_foldx` flag to make use of
  var_struct_anno.FoldXVarStructAnno. FoldX cannot be shipped due to licensing
  reasons. Apply for a license here: http://foldxsuite.crg.eu/. Drop the
  executable with name `foldx` in `dependencies/foldx` or specify a directory
  with `--foldx_dependency_dir`

* ConSurf: add `--add_consurf` flag to make use of seq_anno.ConsurfSeqAnno.
  var3d can use the ConSurf-DB pipeline that has kindly been provided by the 
  Ben-Tal lab. Ping us if you need it...

Build the container
-------------------

By default, the `v3d` runscript expects the container at
`<path_to_var3d_repo>/singularity/var3d.sif`. Given the the generated definition
file <DEF_FILE>, you can achieve that with:

```
$ cd <path_to_var3d_repo>/singularity
$ sudo singularity build var3d.sif <DEF_FILE>
```

Test dependencies
-----------------

Testing the dependencies is not integrated in the general var3d unit testing
framework because of potentially long running times and differing results due
to different underlying databases. There is a testing script for each of the
dependencies and example results you can compare to.

* PLIP: `v3d test_plip.py` generates a file `plip_anno.json` which
  should be equivalent to `plip_anno_example.json`

* PROVEAN: `v3d --mount <DIRECTORY_WITH_NR> test_provean.py <NR>` generates
  a file `provean_anno.json` which should have qualitatively the same values
  as `provean_anno_example.json`.

* FoldX: `v3d test_foldx.py` generates a file `foldx_anno.json` which should
  have qualitatively the same values as `foldx_anno_example.json`

* ConSurf: `v3d --mount <DIRECTORY_WITH_UNIREF90> test_consurf.py <UNIREF90>`
  generates a file `consurf_anno.json` which should have qualitatively the same
  values as `consurf_anno_example.json`.
