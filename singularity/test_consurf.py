import sys
import json

from ost import seq

from var3d import seq_anno
from var3d import pipeline

if len(sys.argv) != 2:
    print("USAGE: v3d <VAR3D_IMAGE> test_consurf.py <UNIREF90>")
    sys.exit(42)

uniref90 = sys.argv[1]
consurf_anno = seq_anno.ConsurfSeqAnno(uniref90)
anno_pipeline = pipeline.AnnotationPipeline(seq_anno=[consurf_anno])

# crambin, the mother of all sequences
s = seq.CreateSequence("A", "TTCCPSIVARSNFNVCRLPGTPEAICATYTGCIIIPGATCPGDYAN")
data = pipeline.DataContainer(s)
anno = anno_pipeline.Run(data)
with open("consurf_anno.json", 'w') as fh:
    json.dump(anno, fh)

