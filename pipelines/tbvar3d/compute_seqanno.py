"""Computes sequence ConSurf/Entropy annotations and generates PROVEAN
supplemental files for caching.
"""

import os
import glob
import argparse
import hashlib
import shutil
import random
import json

from ost import seq

from var3d import base
from var3d import msa_generator
from var3d import seq_anno
from var3d import var_seq_anno
from var3d import pipeline


def _parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("--uniref90")
    parser.add_argument("--nr")
    parser.add_argument("--uniprot_ac")
    parser.add_argument("--sequence")
    parser.add_argument("--anno_dir")
    parser.add_argument("--provean_supplement_dir")

    args = parser.parse_args()

    if not os.path.exists(args.uniref90):
        raise RuntimeError("--uniref90 is non-existent")
    if not glob.glob(
        args.nr + "*"
    ):  # checks for files starting with that prefix
        raise RuntimeError("--nr is non-existent")
    if not os.path.exists(args.anno_dir):
        raise RuntimeError("--anno_dir is non-existent")
    if not os.path.exists(args.provean_supplement_dir):
        raise RuntimeError("--provean_supplement_dir does not exist")

    return args


args = _parse_args()

PROVEAN_SUPPLEMENT_DIR = args.provean_supplement_dir


def save_to_cache(sequence, filepath):
    if os.path.exists(filepath):
        h = hashlib.md5(str(sequence).encode()).hexdigest()
        p = os.path.join(PROVEAN_SUPPLEMENT_DIR, h)
        shutil.copy(filepath, p)


class FakeVarImporter(base.VarImporter):
    """Just import random variant to give Provean some fodder so it generates
    the supplement_file
    """
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        pos = random.randint(1, len(sequence))
        ref = sequence[pos - 1]
        alt = "A"
        if ref == alt:
            alt = "L"
        v = base.Variant.FromHR(ref + str(pos) + alt)
        return [v]


s = seq.CreateSequence(args.uniprot_ac, args.sequence)
data = pipeline.DataContainer(s)

# Data import
fake_variant_importer = FakeVarImporter()
data_import_pipeline = pipeline.DataImportPipeline(
    var_importer=[fake_variant_importer]
)
data = data_import_pipeline.Import(data)


consurf_annotator = seq_anno.ConsurfSeqAnno(args.uniref90)
jackhmmer_msa_generator = msa_generator.JackhmmerMSAGenerator(args.uniref90)
entropy_annotator = seq_anno.EntropySeqAnno(
    jackhmmer_msa_generator, normalize=True
)
provean_annotator = var_seq_anno.ProveanVarSeqAnno(
    args.nr, save_to_cache_callback=save_to_cache
)

# Annotate
annotation_pipeline = pipeline.AnnotationPipeline(
    seq_anno=[consurf_annotator, entropy_annotator],
    var_seq_anno=[provean_annotator],
)

anno = annotation_pipeline.Run(data, variant_hashes=data.variant_hashes)
h = hashlib.md5(str(s).encode()).hexdigest()
with open(os.path.join(args.anno_dir, h), "w") as fh:
    json.dump(anno, fh)
