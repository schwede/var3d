import pandas as pd


# simple script which loads "tbvar3d_targets.csv" and adds two columns
# "structure_description" and "ligand_description" these two columns contain a
# summary of the structural sources and modelling processes a small number of
# cases required more custom work, these are then edited manually in this script
# to describe what happened


# load csv file

df = pd.read_csv("tbvar3d_targets.csv")
df_out = pd.DataFrame()
# iterate over file

for i, row in df.iterrows():
    # gather fields which we need to generate the text
    method_structure = row["method_structure"]
    method_ligand = row["method_ligand"]
    oligomeric_state = row["oligomeric_state"]
    pdbid_exp = row["pdbid_exp"]
    pdbid_hom = row["pdbid_hom"]
    pdbid_trans = row["pdbid_trans"]
    pdbligandid_trans = row["pdbligandid_trans"]
    pdbligandid = row["pdbligandid"]
    drug_pmid_activated_form = row["drug_pmid_activated-form"]
    prot_uniprotid_heteromer = row["prot_uniprotid_heteromer"]

    #######################################################################
    #                           STRUCTURE DESCRIPTION                     #
    #######################################################################

    # generate structure text with the following scheme
    # Experimental Structure, PDB ID [xxxx]
    # Homology Model, SWISS-MODEL, template PDB ID xxxx
    # AlphaFold Protein Structure Database, [URL to af2db entry?]
    # AF2 model, internal AF2 modelling pipeline

    if method_structure == "Exp":
        structure_description = "Experimental Structure, PDB ID " + pdbid_exp
    if method_structure == "Hom":
        structure_description = (
            "Homology Model, SWISS-MODEL, template PDB ID " + pdbid_hom
        )
    if method_structure == "AF2":
        if oligomeric_state == "monomer":
            structure_description = (
                "AF2 model, AlphaFold Protein Structure Database"
            )
        else:
            structure_description = "AF2 model, internal AF2 modelling pipeline"

    row["structure_description"] = structure_description

    #######################################################################
    #                           LIGAND DESCRIPTION                        #
    #######################################################################

    # generate ligand text with the following scheme
    # Ligand present in Structure, PDB Ligand ID [xxx]
    # Ligand transferred from other Structure, structure source for ligand:
    #   PDB ID [xxxx], PDB Ligand ID [xxx]
    # Ligand superposed in structure, Ligand in structure PDB ID [xxx]
    # Ligand docked with AutoDockVina

    if method_ligand == "Exp":
        ligand_description = (
            "Ligand present in structure (PDB Ligand ID " + pdbligandid + ")."
        )
    elif method_ligand == "Trans":
        ligand_description = (
            "Ligand transferred from other structure (PDB ID "
            + pdbid_trans
            + ", PDB Ligand ID "
            + pdbligandid_trans
            + ")."
        )
    elif method_ligand == "Sup":
        ligand_description = (
            "Ligand superposed in structure (Ligand ID of replaced molecule "
            + pdbligandid_trans
            + ")."
        )
    elif method_ligand == "Dock":
        ligand_description = "Ligand docked with AutoDock Vina 1.2.0"
    else:
        ligand_description = ""

    row["ligand_description"] = ligand_description

    #######################################################################
    #                           ACTIVATED DRUG FOMRS                      #
    #######################################################################

    # generate text about prodrug with the following scheme
    # Ligand present in structure is the activated form of antibiotic:
    # PubChemID [XXXXX], PDB Ligand ID [xxx]
    if drug_pmid_activated_form == drug_pmid_activated_form:
        activated_form_description = (
            "Ligand present in structure is the activated form of antibiotic: PubChemID "
            + str(int(drug_pmid_activated_form))
            + " , PDB Ligand ID "
            + pdbligandid
        )
    else:
        activated_form_description = ""

    row["activated_form_description"] = activated_form_description

    #######################################################################
    #                           SPECIAL CASES                             #
    #######################################################################

    # there is a more complicated exception for the following cases, they will
    # get their custom descriptions
    # P9WG45 Moxifloxacin
    # P9WG45 Levofloxacin
    # P9WG47 Moxifloxacin
    # P9WG47 Levofloxacin
    if row["prot_uniprotid"] == "P9WG45" and row["drug_name"] == "Moxifloxacin":
        structure_description = "Experimental Structure PDB ID 5bs8 used as scaffold, the complete gyrB and gyrA chains were imported from the AlphaFold Protein Structure Database and superposed on the experimental structure."
        ligand_description = (
            "Ligand present in structure scaffold, PDB Ligand ID MFX"
        )
    if row["prot_uniprotid"] == "P9WG45" and row["drug_name"] == "Levofloxacin":
        structure_description = "Experimental Structure PDB ID 5btg used as scaffold, the complete gyrB and gyrA chains were imported from the AlphaFold Protein Structure Database and superposed on the experimental structure."
        ligand_description = (
            "Ligand present in structure scaffold, PDB Ligand ID LFX"
        )
    if row["prot_uniprotid"] == "P9WG47" and row["drug_name"] == "Moxifloxacin":
        structure_description = "Experimental Structure PDB ID 5bs8 used as scaffold, the complete gyrB and gyrA chains were imported from the AlphaFold Protein Structure Database and superposed on the experimental structure."
        ligand_description = (
            "Ligand present in structure scaffold, PDB Ligand ID MFX"
        )
    if row["prot_uniprotid"] == "P9WG45" and row["drug_name"] == "Levofloxacin":
        structure_description = "Experimental Structure PDB ID 5btg used as scaffold, the complete gyrB and gyrA chains were imported from the AlphaFold Protein Structure Database and superposed on the experimental structure."
        ligand_description = (
            "Ligand present in structure scaffold, PDB Ligand ID LFX"
        )

    row["structure_description"] = structure_description
    row["ligand_description"] = ligand_description

    df_out = df_out.append(row, ignore_index=True)

df_out.to_csv("tbvar3d_targets_desc.csv", index=False)
