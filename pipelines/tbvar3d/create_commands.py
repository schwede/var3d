import os

# v3d executable in var3d checkout
v3d_exec = "/scicore/home/schwede/studga00/prog/var3d/v3d"

# directory with JSON files that have been generated with create_pipeline_in.py
data_dir = "data"

# directory containing uniref90 and nr databases, will be mounted
# into container
seqdb_path = "/scicore/home/schwede/GROUP/var3d_data"
uniref90 = "/scicore/home/schwede/GROUP/var3d_data/uniref90.fasta"
nr = "/scicore/home/schwede/GROUP/var3d_data/nr/nr"

# mongo settings
mongo_host = "mongodb://worker02"
mongo_db = "up3d"

# tbvar3d pipeline
pipeline_script = "tbvar3d_pipeline.py"

# if you want to add it as a project to MongoDB, set project_id. If you
# only want to generate the annotation jsons, set it to None
project_id = "myctu"

# whether to add do_not_delete to True in added project
do_not_delete = True

# that's where the commands are written
cmd_file = "commands.cmd"

data_files = os.listdir(data_dir)
commands = list()
for df in data_files:
    if not df.endswith("_data.json"):
        continue
    cmd = [
        v3d_exec,
        "--mount",
        seqdb_path,
        pipeline_script,
        "--uniref90",
        uniref90,
        "--nr",
        nr,
        "--data_json",
        os.path.join(data_dir, df),
        "--anno_json",
        os.path.join(data_dir, df.replace("data", "anno")),
        "--mongo_host",
        mongo_host,
        "--mongo_db",
        mongo_db,
    ]
    if project_id:
        cmd += ["--project_id", project_id]
    if do_not_delete:
        cmd.append("--do_not_delete")

    commands.append(" ".join(cmd))

with open(cmd_file, "w") as fh:
    fh.write("\n".join(commands))
