import json
import os
import hashlib
from ost import io

anno_files = os.listdir("annotations")

# keep track of sequences - store by hash (anno files are stored by hash too)
tb_proteome = io.LoadSequenceList("uniprot-proteome_UP000001584.fasta")
sequences = dict()
for s in tb_proteome:
    h = hashlib.md5(str(s).encode()).hexdigest()
    sequences[h] = s

# the known errors
consurf_min_sequences_thresh = set()

# errors that are not specifically dealt with
errors = list()

for f_idx, f in enumerate(anno_files):
    with open(os.path.join("annotations", f)) as fh:
        data = json.load(fh)

    if len(data["err"]):
        # eat ConSurf error messages of not enough sequences found
        cleaned_err = list()
        for e in data["err"]:
            if "The minimal number of sequences required for the calculation is 50" not in e["err_msg"]:
                cleaned_err.append(e)
            else:
                consurf_min_sequences_thresh.add(f)
            
        if len(cleaned_err):
            errors.append((f, cleaned_err, data))

    # check whether ConSurf and Entropy annotation lengths match with sequence
    if data["seq_anno"]["consurf"]:
        assert(len(data["seq_anno"]["consurf"]) == len(sequences[f]))
    if data["seq_anno"]["entropy"]:
        assert(len(data["seq_anno"]["entropy"]) == len(sequences[f]))


print("N sequences in proteome:", len(sequences))
print("N checked annotations:", len(anno_files))
if len(sequences) != len(anno_files):
    print("ATTENTION! N sequences != N annotations")
print("N consurf min sequences thresh errors:", len(consurf_min_sequences_thresh))
print("N remaining errors:", len(errors))
print("here they are:")
for e_idx,  e in enumerate(errors):
    print("error", e_idx)
    print("sequence hash:", e[0])
    print("error:", e[1])
    print("sequence name:",sequences[e[0]].GetName())
    print("sequence:", sequences[e[0]])
    print()
