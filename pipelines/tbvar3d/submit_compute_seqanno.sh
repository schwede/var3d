#!/bin/bash
#SBATCH --job-name=tbvar3d_cache
#SBATCH --qos=1week
#SBATCH --time=3-00:00:00
#SBATCH --output=stdout/%A_%a.out
#SBATCH --mem=20G
#SBATCH --array=1-3992
ml Python/3.6.6-foss-2018b
SEEDFILE=compute_seqanno_commands.cmd
SEED=$(sed -n ${SLURM_ARRAY_TASK_ID}p $SEEDFILE)
eval $SEED

