import os
import json
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

def _update_cache(mongo_db, md5, anno, supporting_set, force_replace=True):
    
    if mongo_db is None:
        return

    cache = mongo_db.var3d_annotations.find_one({'md5': md5})

    if cache is None:
        cache = {"md5": md5}

    # update consurf
    if force_replace or "consurf" not in cache:
        if "consurf" in anno["seq_anno"]:
            cache["consurf"] = anno["seq_anno"]["consurf"]

    # update entropy
    if force_replace or "entropy" not in cache:
        if "entropy" in anno["seq_anno"]:
            cache["entropy"] = anno["seq_anno"]["entropy"]

    # update provean supporting set
    if force_replace or "provean_supporting_set" not in cache:
        cache["provean_supporting_set"] = supporting_set

    # use replace_one with upsert=True
    # if it's not there, it's inserted. If it's there, it's replaced
    try:
        mongo_db.var3d_annotations.replace_one({'md5': md5}, cache, upsert = True)
    except DuplicateKeyError:
        # It is possible, if multiple processes create the record at
        # the same time, that an md5 might be attempted to be 
        # created in multiple documents
        pass

anno_dir = "annotations"
supporting_dir = "provean_supplement_files"
mongo_host = 'mongodb://worker02'
mongo_db = 'up3d'

# setup mongo
mongo_client = MongoClient(mongo_host, tz_aware=True, serverSelectionTimeoutMS=6000)
mongo_db = mongo_client[mongo_db]

# check for present files
anno_files = os.listdir(anno_dir)
supporting_files = os.listdir(supporting_dir)

# make supplement_files a set so its faster to search an entry...
supporting_files = set(supporting_files)

for af in anno_files:

    if af not in supporting_files:
        print("Could not find " + af + " in supporting files")
        continue

    with open(os.path.join(anno_dir, af), 'r') as fh:
        anno = json.load(fh)

    with open(os.path.join(supporting_dir, af), 'r') as fh:
        supporting_set = fh.read()

    _update_cache(mongo_db, af, anno, supporting_set)

