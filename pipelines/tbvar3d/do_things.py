import pandas as pd
import json

xl_file = pd.ExcelFile("WHO-UCN-GTB-PCI-2021.7-eng.xlsx")
genome_indices = xl_file.parse("Genome_indices")

print(len(genome_indices))

with open("mapping_data.json") as fh:
    mapping_data = json.load(fh)

processed_rows = 0

for uniprot_ac, gene_locus in mapping_data[
    "uniprot_mycobrowser_mapping"
].items():

    df = genome_indices[
        genome_indices["final_annotation.LocusTag"] == gene_locus
    ]
    df = df[~df["final_annotation.TentativeHGVSProteicAnnotation"].isnull()]

    yolo = 0
    for i, row in df.iterrows():
        yolo += 1
    print(len(df), yolo)

    processed_rows += len(df)

print(processed_rows)
