import os
import argparse
import glob
import json
import socket
import fcntl
import hashlib
import traceback
import bson
import base64
import gzip
import re
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

import ost
from ost import io
from ost import seq
from ost.mol.alg import Accessibility

from var3d import base
from var3d import uniprot
from var3d import msa_generator
from var3d import struct_importer
from var3d import seq_anno
from var3d import struct_anno
from var3d import var_seq_anno
from var3d import var_struct_anno
from var3d import pipeline
from var3d import var_importer


def _concurrent_write(filename, message, clear=False):
    """Exclusive writing to a file.

    Writes to file with given name. To avoid concurrent access of the file,
    creates a lock file of the name filename.lock and make sure none else is
    accessing it. If the file/the directory does not exist, it is created first.
    """
    dirname = os.path.dirname(filename)
    if len(dirname) and not os.path.exists(dirname):
        os.makedirs(dirname)
    sh_lock = "%s.lock" % filename
    sh_lock_fd = open(sh_lock, "w")
    fcntl.lockf(sh_lock_fd, fcntl.LOCK_EX)
    flags = "a"
    if clear:
        flags = "w"
    fd = open(filename, flags)
    fd.write("%s\n" % (message))
    fd.close()
    fcntl.lockf(sh_lock_fd, fcntl.LOCK_UN)
    sh_lock_fd.close()


def _read_status(filename):
    """Read status from file.
    """
    sh_lock = "%s.lock" % filename
    if not os.path.exists(filename):
        return "STATUS_FILE_NOT_FOUND_%s FAILED" % filename
    sh_lock_fd = open(sh_lock, "r")
    fcntl.lockf(sh_lock_fd, fcntl.LOCK_SH)
    message = "\n".join(open(filename, "r").readlines())
    fcntl.lockf(sh_lock_fd, fcntl.LOCK_UN)
    sh_lock_fd.close()
    return message


def _write_status(filename, message, job_id=None, task_type=None):
    """Write current job status to file
    """
    if not job_id:
        job_id = _read_status(filename).split()[0].strip()
    elif task_type:
        task_type = task_type.upper()
        if task_type != "JOB":
            task_type = socket.gethostname()
        job_id = "%s@%s" % (str(job_id), task_type)
    msg = "%s %s" % (job_id, message)
    _concurrent_write(filename, msg, clear=True)


def _UpdateProgress(filename, message):
    _concurrent_write(filename, message, clear=False)


class _AccessibilityStructAnno(base.StructAnno):
    """Implementation of StructAnno - Annotates solvent accessibility

    Uses the Openstructure Accessibility function and returns annotations 
    either in square angstrom or normalized to range [0, 1]

    Other than the default AccessibilityStructAnno in var3d, this object
    does not map to any specific uniprot sequence but rather returns a dict
    in form {<chain_name_1>: [<acc_r1>, <acc_r2>, ...], <chain_name_2>: ...}
    The lists of accessibility values correspond to the residues that you
    get when blindly iterating over the structure and may contain None
    if a residue has no accessibility assigned.

    :param normalize: Whether to normalize
    :type normalize: :class:`bool`
    :param key: Return value of parent class Key function, default: 
                "accessibility"
    :type key: :class:`str`
    """

    def __init__(
        self,
        probe_radius=1.4,
        include_hydrogens=False,
        include_hetatm=False,
        include_water=False,
        normalize=False,
        key="accessibility",
    ):
        self.probe_radius = probe_radius
        self.include_hydrogens = include_hydrogens
        self.include_hetatm = include_hetatm
        self.include_water = include_water
        self.normalize = normalize
        self.key = key

    def Run(self, sequence, structures, seq_range=None):
        """Implementation of parent interface
        """
        anno = list()
        for structure in structures:
            Accessibility(
                structure.assembly,
                probe_radius=self.probe_radius,
                include_hydrogens=self.include_hydrogens,
                include_hetatm=self.include_hetatm,
                include_water=self.include_water,
            )
            prop_name = "asaAbs"
            if self.normalize:
                prop_name = "asaRel"
            structure_anno = dict()
            for chain in structure.assembly.chains:
                chain_accessibilities = list()
                for r in chain.residues:
                    if r.HasProp(prop_name):
                        chain_accessibilities.append(r.GetFloatProp(prop_name))
                    else:
                        chain_accessibilities.append(None)
                structure_anno[chain.GetName()] = chain_accessibilities
            anno.append(structure_anno)
        return anno

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def PerResidue(self):
        """Implementation of parent interface
        """
        return False


def _drugs_from_meta(meta):
    drugs = list()
    if "pubchemid" in meta:
        method_ligand = [None] * len(meta["pubchemid"])
        if "method_ligand" in meta:
            if len(meta["method_ligand"]) != len(meta["pubchemid"]):
                raise RuntimeError(
                    "method_ligand and pubchem meta data " "inconsistent"
                )
            method_ligand = meta["method_ligand"]
        ligand_res_num = [None] * len(meta["pubchemid"])
        if "ligand_res_num" in meta:
            if len(meta["ligand_res_num"]) != len(meta["pubchemid"]):
                raise RuntimeError(
                    "ligand_res_num and pubchem meta data " "inconsistent"
                )
            ligand_res_num = meta["ligand_res_num"]
        ligand_description = [None] * len(meta["pubchemid"])
        if "ligand_description" in meta:
            if len(meta["ligand_description"]) != len(meta["pubchemid"]):
                raise RuntimeError(
                    "ligand_description and pubchem meta data " "inconsistent"
                )
            ligand_description = meta["ligand_description"]
        activated_form_description = [None] * len(meta["pubchemid"])
        if "activated_form_description" in meta:
            if len(meta["activated_form_description"]) != len(
                meta["pubchemid"]
            ):
                raise RuntimeError(
                    "activated_form_description and pubchem meta data "
                    "inconsistent"
                )
            activated_form_description = meta["activated_form_description"]
        for pid, m, lrn, ld, afd in zip(
            meta["pubchemid"],
            method_ligand,
            ligand_res_num,
            ligand_description,
            activated_form_description,
        ):
            drugs.append(
                {
                    "pubchemid": pid,
                    "method_ligand": m,
                    "ligand_res_num": lrn,
                    "ligand_description": ld,
                    "activated_form_description": afd,
                }
            )
    return drugs


def _structures_for_mongodb(data):
    structures = list()

    for s_idx in range(data.GetNumStructures()):

        s = data.GetStructure(s_idx)
        mongo_s = s.ToJSON()
        mongo_s["assembly_md5"] = data.GetAssemblyHash(s_idx)

        # no need for the following data
        for dkey in [
            "seq_range",
            "sequence",
            "strict",
            "assembly",
            "structure_identifier",
            "provider",
        ]:
            if dkey in mongo_s:
                del mongo_s[dkey]

        # parse meta
        meta = mongo_s.pop("meta")
        meta_keys = [
            "method_structure",
            "oligomeric_state",
            "biounit",
            "experimental_pdbid",
            "template_pdbid",
        ]
        if not sum([1 for k in meta_keys if k in meta]) == len(meta_keys):
            raise RuntimeError(
                f"Expect the following keys in structure " f"meta: {meta_keys}"
            )

        method_structure = meta["method_structure"]
        if method_structure == "Hom":
            mongo_s["provider"] = "SWISSMODEL"
            template_pdbid = meta["template_pdbid"]
            biounit = meta["biounit"]
            mongo_s["template"] = template_pdbid
            mongo_s["qname"] = f"{template_pdbid}.{biounit}"
        elif method_structure == "Exp":
            mongo_s["provider"] = "PDB"
            mongo_s["qname"] = meta["experimental_pdbid"]
        elif method_structure == "AF2":
            mongo_s["provider"] = "AlphaFold"
            mongo_s["qname"] = "AlphaFold"
        elif method_structure is None:
            mongo_s["provider"] = "Not available"
            mongo_s["qname"] = "Not available"
        else:
            raise RuntimeError(f"Invalid method_structure: {method_structure}")
        mongo_s["stoichiometry"] = meta["oligomeric_state"]
        mongo_s["biounit"] = meta["biounit"]

        if "structure_description" in meta:
            mongo_s["structure_description"] = meta["structure_description"]
        else:
            mongo_s["structure_description"] = None

        # deal with potential drug ligand(s)
        mongo_s["drugs"] = _drugs_from_meta(meta)

        # The frontend wants the alignment in a slightly different format
        # let's construct it from scratch and overwrite what we have in mongo_s
        #
        # - mdl_from/mdl_to added to seqres to specify range covered in ref seq
        # - tpl_from/tpl_to added to atomseq to specify range of residues
        # - alignment gets cut to mdl_from/mtl_to
        # - additional info (resno) specifying the residue number for each
        #   residue in atomseq
        new_alns = list()

        for segment_idx in range(s.GetNumSegments()):
            aln = s.aln[segment_idx]

            # get residue numbers for each residue in ATOMSEQ, gaps are marked
            # with None
            rnums = list()
            for col in aln:
                if col[1] != "-":
                    rnums.append(col.GetResidue(1).GetNumber().GetNum())
                else:
                    rnums.append(None)

            # get seqres range that is covered by structural data
            range_covered = s.GetRangeCovered(segment_idx)
            # translate that to actual col indices in the alignment
            col_idx_from = None
            col_idx_to = None
            rnum = 0
            for col_idx, col in enumerate(aln):
                if col[0] != "-":
                    rnum += 1
                if rnum == range_covered[0]:
                    col_idx_from = col_idx
                if rnum == range_covered[1]:
                    col_idx_to = col_idx

            if col_idx_from is None or col_idx_to is None:
                raise RuntimeError("That should never happen")

            new_aln = dict()
            new_aln["seqres"] = dict()
            new_aln["seqres"]["name"] = aln.GetSequence(0).GetName()
            seqres = str(aln.GetSequence(0))
            new_aln["seqres"]["sequence"] = seqres[
                col_idx_from : col_idx_to + 1
            ]
            new_aln["seqres"]["mdl_from"] = range_covered[0]
            new_aln["seqres"]["mdl_to"] = range_covered[1]

            new_aln["atomseq"] = dict()
            new_aln["atomseq"]["name"] = aln.GetSequence(1).GetName()
            atomseq = str(aln.GetSequence(1))
            new_aln["atomseq"]["sequence"] = atomseq[
                col_idx_from : col_idx_to + 1
            ]
            rnums = rnums[col_idx_from : col_idx_to + 1]
            new_aln["atomseq"]["resno"] = rnums
            rnums_no_none = [x for x in rnums if x is not None]
            new_aln["atomseq"]["tpl_from"] = min(rnums_no_none)
            new_aln["atomseq"]["tpl_to"] = max(rnums_no_none)
            new_alns.append(new_aln)

        mongo_s["aln"] = new_alns
        mongo_s["mdl_from"] = min([x["seqres"]["mdl_from"] for x in new_alns])
        mongo_s["mdl_to"] = max([x["seqres"]["mdl_to"] for x in new_alns])

        structures.append(mongo_s)

    return structures


def _variants_for_mongodb(data):
    variants = list()
    for v_idx in range(data.GetNumVariants()):
        v = data.GetVariant(v_idx)
        mongo_v = v.ToJSON()
        mongo_v["hashcode"] = data.variant_hashes[v_idx]
        mongo_v["drugs"] = list()

        meta = mongo_v.pop("meta")  # directly process meta info and delete it
        if "mechanism" in meta:
            mongo_v["mechanism"] = (
                meta["mechanism"].replace("_", " ").capitalize()
            )
        if "variant_label" in meta:
            mongo_v["variant_label"] = meta["variant_label"]
        if "pubchemid" in meta and "WHO_grading" in meta:
            for pubchemid, who in zip(meta["pubchemid"], meta["WHO_grading"]):
                who_grad = who.replace("ssoc w R", "ssociated with resistance")
                mongo_v["drugs"].append(
                    {"pubchemid": pubchemid, "WHO_grading": who_grad}
                )

        # replace pos with start and end to simplify frontend display
        pos = mongo_v.pop("pos")
        mongo_v["start"] = pos
        mongo_v["end"] = pos + len(mongo_v["ref"]) - 1

        variants.append(mongo_v)
    return variants


def _project_to_mongo(
    mongo_db, data, anno, object_id, do_not_delete, project_id
):

    if mongo_db is None:
        return

    entry = dict()
    entry["variants"] = _variants_for_mongodb(data)
    entry["structures"] = _structures_for_mongodb(data)

    if object_id is not None:
        # we're only updating an already existing project
        # the only other thing besides variants and structures is
        # updating job_status
        entry["job_status"] = "COMPLETED"
        mongo_db.var3d.update_one(
            {"_id": bson.ObjectId(object_id)}, {"$set": entry}
        )
    elif project_id is not None:
        # create full entry from scratch
        entry["job_status"] = "COMPLETED"
        entry["do_not_delete"] = do_not_delete
        entry["project_id"] = project_id
        entry["md5"] = hashlib.md5(str(data.sequence).encode()).hexdigest()
        entry["uniprot_ac"] = data.sequence.GetName()
        mongo_db.var3d.replace_one(
            {"md5": entry["md5"], "project_id": project_id}, entry, upsert=True
        )


def _update_assembly_cache(mongo_db, data, anno):

    if mongo_db is None:
        return

    for structure_idx in range(data.GetNumStructures()):

        structure_hash = data.GetStructureHash(structure_idx)
        assembly_hash = data.GetAssemblyHash(structure_idx)

        # keep track if we added anything useful to cache
        update_cache = False

        cache = mongo_db.var3d_assemblies.find_one(
            {"assembly_md5": assembly_hash}
        )
        if cache is None:
            update_cache = True
            cache = {"assembly_md5": assembly_hash}

        # we even cache the actual structure
        if "omf_gz" not in cache:
            update_cache = True
            omf_gz = base64.b64decode(data.GetStructure(structure_idx).omf_str)
            cache["omf_gz"] = omf_gz

        annotations = ["accessibility", "transmembrane", "plip"]
        for a in annotations:
            if (
                a not in cache
                and a in anno["struct_anno"]
                and structure_hash in anno["struct_anno"][a]
                and anno["struct_anno"][a][structure_hash] != None
            ):
                update_cache = True
                cache[a] = anno["struct_anno"][a][structure_hash]

        if update_cache:
            # use replace_one with upsert=True
            # if it's not there, it's inserted. If it's there, it's replaced
            try:
                mongo_db.var3d_assemblies.replace_one(
                    {"assembly_md5": assembly_hash}, cache, upsert=True
                )
            except DuplicateKeyError:
                # It is possible, if multiple processes create the record at
                # the same time, that an md5 might be attempted to be
                # created in multiple documents
                pass


def _update_seqres_cache(mongo_db, data, anno):

    if mongo_db is None:
        return

    # keep track if we added anything useful to cache
    update_cache = False

    md5 = hashlib.md5(str(data.sequence).encode()).hexdigest()
    cache = mongo_db.var3d_annotations.find_one({"md5": md5})
    if cache is None:
        update_cache = True
        cache = {"md5": md5}

    # update consurf
    if "consurf" not in cache:
        if (
            "consurf" in anno["seq_anno"]
            and anno["seq_anno"]["consurf"] != None
        ):
            if len(anno["seq_anno"]["consurf"]) == len(data.sequence):
                update_cache = True
                cache["consurf"] = anno["seq_anno"]["consurf"]

    # update entropy
    if "entropy" not in cache:
        if (
            "entropy" in anno["seq_anno"]
            and anno["seq_anno"]["entropy"] != None
        ):
            if len(anno["seq_anno"]["entropy"]) == len(data.sequence):
                update_cache = True
                cache["entropy"] = anno["seq_anno"]["entropy"]

    # update provean - its a bit weird, I know...
    hr_provean_cache = dict()
    if "provean" in cache:
        hr_provean_cache = {
            item["refPosAlt"]: item["value"] for item in cache["provean"]
        }
    hr_provean_anno = dict()
    for v_idx in range(data.GetNumVariants()):
        v = data.GetVariant(v_idx)
        if v.variant_type in ["DELETION", "INSERTION", "SUBSTITUTION", "INDEL"]:
            hr_provean_anno[v.ToHR()] = anno["var_seq_anno"]["provean"][
                v.GetHash()
            ]
    # only add whats missing
    for k, v in hr_provean_anno.items():
        if k not in hr_provean_cache:
            update_cache = True
            hr_provean_cache[k] = v
    cache["provean"] = [
        {"refPosAlt": k, "value": v} for k, v in hr_provean_cache.items()
    ]

    # update provean supporting set from global cache variable
    if (
        "provean_supporting_set" not in cache
        and md5 in _PROVEAN_SUPPORTING_SET_CACHE
    ):
        update_cache = True
        cache["provean_supporting_set"] = _PROVEAN_SUPPORTING_SET_CACHE[md5]

    if update_cache:
        # use replace_one with upsert=True
        # if it's not there, it's inserted. If it's there, it's replaced
        try:
            mongo_db.var3d_annotations.replace_one(
                {"md5": md5}, cache, upsert=True
            )
        except DuplicateKeyError:
            # It is possible, if multiple processes create the record at
            # the same time, that an md5 might be attempted to be
            # created in multiple documents
            pass


def _update_mongo(mongo_db, data, anno, object_id, do_not_delete, project_id):
    if mongo_db is None:
        return
    _update_seqres_cache(mongo_db, data, anno)
    _update_assembly_cache(mongo_db, data, anno)
    if object_id is not None or project_id is not None:
        _project_to_mongo(
            mongo_db, data, anno, object_id, do_not_delete, project_id
        )


# callbacks to cache provean supporting sets work on this global variable
# please forgive me
_PROVEAN_SUPPORTING_SET_CACHE = dict()


def _save_to_provean_cache(sequence, filepath):
    if os.path.exists(filepath):
        h = hashlib.md5(str(sequence).encode()).hexdigest()
        with open(filepath, "r") as fh:
            _PROVEAN_SUPPORTING_SET_CACHE[h] = fh.read()


def _load_from_provean_cache(sequence, filepath):
    h = hashlib.md5(str(sequence).encode()).hexdigest()
    if h in _PROVEAN_SUPPORTING_SET_CACHE:
        with open(filepath, "w") as fh:
            fh.write(_PROVEAN_SUPPORTING_SET_CACHE[h])


def _load_cache(mongo_db, data):

    precomputed_anno = pipeline.GetEmptyAnno()

    if mongo_db is None:
        return precomputed_anno  # no caching :(

    # do sequence annotations
    #########################

    md5 = hashlib.md5(str(data.sequence).encode()).hexdigest()
    cache = mongo_db.var3d_annotations.find_one({"md5": md5})

    if cache is None:
        return precomputed_anno  # cache contains no entry for that md5 :(

    if "consurf" in cache:
        precomputed_anno["seq_anno"]["consurf"] = cache["consurf"]

    if "entropy" in cache:
        precomputed_anno["seq_anno"]["entropy"] = cache["entropy"]

    precomputed_anno["var_seq_anno"]["provean"] = dict()
    if data.GetNumVariants() and "provean" in cache:
        # it's stored as [{"refPosAlt": a1B, "value": 1.00}, ...]
        # let's transform to dict for lookup
        provean_cache = {x["refPosAlt"]: x["value"] for x in cache["provean"]}
        for v_idx in range(data.GetNumVariants()):
            v = data.GetVariant(v_idx)
            if v.ToHR() in provean_cache:
                val = provean_cache[v.ToHR()]
                precomputed_anno["var_seq_anno"]["provean"][v.GetHash()] = val

    # move provean supporting set to global cache variable if present
    if "provean_supporting_set" in cache:
        _PROVEAN_SUPPORTING_SET_CACHE[md5] = cache["provean_supporting_set"]

    # do structure annotations
    ##########################

    for structure_idx in range(data.GetNumStructures()):
        assembly_hash = data.GetAssemblyHash(structure_idx)
        cache = mongo_db.var3d_assemblies.find_one(
            {"assembly_md5": assembly_hash}
        )
        if cache is None:
            continue  # nada found
        structure_hash = data.GetStructureHash(structure_idx)
        annotations = ["accessibility", "transmembrane", "plip"]
        for a in annotations:
            if a in cache:
                if a not in precomputed_anno["struct_anno"]:
                    precomputed_anno["struct_anno"][a] = dict()
                precomputed_anno["struct_anno"][a][structure_hash] = cache[a]

    return precomputed_anno


def _annotate(anno_json, data, mongo_db, progress_file, uniref90, nr):
    """Annotation with potential loading from cache

    Loading from cache follows two strategies with the first having precendence

    Strategy 1: anno_json is an existing JSON file => load and compute
                missing stuff
    Strategy 2: poll mongo_db and compute missing stuff

    No matter what strategy has been used, if anything had to be computed, 
    the resulting anno object is passed to _update_cache which updates mongo_db
    if its a valid database
    """
    if os.path.exists(anno_json):
        if progress_file:
            _UpdateProgress(
                progress_file, "Load annoation cache from precomputed JSON"
            )
        with open(anno_json, "r") as fh:
            precomputed_anno = json.load(fh)
    else:
        if progress_file and mongo_db:
            _UpdateProgress(progress_file, "Load annotation cache from MongoDB")
        precomputed_anno = _load_cache(mongo_db, data)
    anno = pipeline.GetEmptyAnno()

    # consurf
    if "consurf" in precomputed_anno["seq_anno"]:
        if progress_file:
            _UpdateProgress(progress_file, "Reusing cached ConSurf values")
        anno["seq_anno"]["consurf"] = precomputed_anno["seq_anno"]["consurf"]
    else:
        if progress_file:
            _UpdateProgress(progress_file, "Compute ConSurf values")
        consurf_annotator = seq_anno.ConsurfSeqAnno(uniref90)
        consurf_pipeline = pipeline.AnnotationPipeline(
            seq_anno=[consurf_annotator]
        )
        consurf_anno = consurf_pipeline.Run(data)
        pipeline.MergeAnnotations(anno, consurf_anno)

    # entropy
    if "entropy" in precomputed_anno["seq_anno"]:
        if progress_file:
            _UpdateProgress(progress_file, "Reusing cached entropy values")
        anno["seq_anno"]["entropy"] = precomputed_anno["seq_anno"]["entropy"]
    else:
        if progress_file:
            _UpdateProgress(progress_file, "Compute Entropy values")
        jackhmmer_msa_generator = msa_generator.JackhmmerMSAGenerator(uniref90)
        entropy_annotator = seq_anno.EntropySeqAnno(
            jackhmmer_msa_generator, normalize=True
        )
        entropy_pipeline = pipeline.AnnotationPipeline(
            seq_anno=[entropy_annotator]
        )
        entropy_anno = entropy_pipeline.Run(data)
        pipeline.MergeAnnotations(anno, entropy_anno)

    # provean
    variants_cached = list()
    variants_to_compute = list()
    variants_irrelevant = list()  # stuff like FRAMESHIFT etc.
    for v_idx in range(data.GetNumVariants()):
        v = data.GetVariant(v_idx)
        if (
            "provean" in precomputed_anno["var_seq_anno"]
            and v.GetHash() in precomputed_anno["var_seq_anno"]["provean"]
        ):
            variants_cached.append(v)
        elif v.variant_type in [
            "DELETION",
            "INSERTION",
            "SUBSTITUTION",
            "INDEL",
        ]:
            variants_to_compute.append(v)
        else:
            variants_irrelevant.append(v)
    if len(variants_cached):
        if progress_file:
            msg = "Reusing cached Provean values for: \n"
            msg += "\n".join([f" + {v.ToHR()}" for v in variants_cached])
            _UpdateProgress(progress_file, msg)
        anno["var_seq_anno"]["provean"] = precomputed_anno["var_seq_anno"][
            "provean"
        ]
    if len(variants_to_compute):
        if progress_file:
            msg = "Compute Provean values for: \n"
            msg += "\n".join([f" + {v.ToHR()}" for v in variants_to_compute])
            _UpdateProgress(progress_file, msg)
        provean_annotator = var_seq_anno.ProveanVarSeqAnno(
            nr,
            load_from_cache_callback=_load_from_provean_cache,
            save_to_cache_callback=_save_to_provean_cache,
        )
        provean_pipeline = pipeline.AnnotationPipeline(
            var_seq_anno=[provean_annotator]
        )
        hashes = [v.GetHash() for v in variants_to_compute]
        provean_anno = provean_pipeline.Run(data, variant_hashes=hashes)
        pipeline.MergeAnnotations(anno, provean_anno)
    if len(variants_irrelevant):
        # Provean would return return None for FRAMESHIFT etc.
        # so let's just do that manually
        if "provean" not in anno["var_seq_anno"]:
            anno["var_seq_anno"]["provean"] = dict()
        for v in variants_irrelevant:
            anno["var_seq_anno"]["provean"][v.GetHash()] = None

    # accessibility
    s_hashes = data.structure_hashes
    if "accessibility" in precomputed_anno["struct_anno"] and sum(
        [
            1
            for h in s_hashes
            if h in precomputed_anno["struct_anno"]["accessibility"]
        ]
    ) == len(s_hashes):
        if progress_file:
            _UpdateProgress(
                progress_file, "Reusing cached Accessibility values"
            )
        accessibility_anno = dict()
        for h in s_hashes:
            accessibility_anno[h] = precomputed_anno["struct_anno"][
                "accessibility"
            ][h]
        anno["struct_anno"]["accessibility"] = accessibility_anno
    elif len(s_hashes):
        # accessibility for at least one structure is missing, let's just
        # compute it for all
        if progress_file:
            _UpdateProgress(progress_file, "Compute Accessibility values")
        accessibility_annotator = _AccessibilityStructAnno(normalize=True)
        accessibility_pipeline = pipeline.AnnotationPipeline(
            struct_anno=[accessibility_annotator]
        )
        accessibility_anno = accessibility_pipeline.Run(data)
        pipeline.MergeAnnotations(anno, accessibility_anno)

    # transmembrane
    if "transmembrane" in precomputed_anno["struct_anno"] and sum(
        [
            1
            for h in s_hashes
            if h in precomputed_anno["struct_anno"]["transmembrane"]
        ]
    ) == len(s_hashes):
        if progress_file:
            _UpdateProgress(
                progress_file, "Reusing cached Transmembrane values"
            )
        transmembrane_anno = dict()
        for h in s_hashes:
            transmembrane_anno[h] = precomputed_anno["struct_anno"][
                "transmembrane"
            ][h]
        anno["struct_anno"]["transmembrane"] = transmembrane_anno
    elif len(s_hashes):
        # transmembrane for at least one structure is missing, let's just
        # compute it for all
        if progress_file:
            _UpdateProgress(progress_file, "Compute Transmembrane values")
        transmembrane_annotator = struct_anno.TransmembraneStructAnno()
        transmembrane_pipeline = pipeline.AnnotationPipeline(
            struct_anno=[transmembrane_annotator]
        )
        transmembrane_anno = transmembrane_pipeline.Run(data)
        pipeline.MergeAnnotations(anno, transmembrane_anno)

    # plip
    if "plip" in precomputed_anno["struct_anno"] and sum(
        [1 for h in s_hashes if h in precomputed_anno["struct_anno"]["plip"]]
    ) == len(s_hashes):
        if progress_file:
            _UpdateProgress(progress_file, "Reusing cached PLIP values")
        plip_anno = dict()
        for h in s_hashes:
            plip_anno[h] = precomputed_anno["struct_anno"]["plip"][h]
        anno["struct_anno"]["plip"] = plip_anno
    elif len(s_hashes):
        # PLIP for at least one structure is missing, let's just
        # compute it for all
        if progress_file:
            _UpdateProgress(progress_file, "Compute PLIP values")
        plip_annotator = struct_anno.PLIPStructAnno()
        plip_pipeline = pipeline.AnnotationPipeline(
            struct_anno=[plip_annotator]
        )
        plip_anno = plip_pipeline.Run(data)
        pipeline.MergeAnnotations(anno, plip_anno)
    return anno


def _data_import(user_data_json, user_structure_dir, data_json, progress_file):
    """Imports data with two different strategies with first having precedence

    Strategy 1: data_json is an existing JSON file representing a var3d
                DataContainer object => load and return
    Strategy 2: user pipeline mode: imports data given information in 
                user_structure_dir and user_data_json.
                data_json_path is a JSON file containing a dict like
                {\"AC\": ac, \"sequence\": s, \"variants\":[v1,...]}, the 
                variants are strings that can be read by Variant.FromHR
                All PDB files in user_structure_dir will be imported too,
                with the filename as structure_identifier. Meta data is
                loaded from a structure specific json file. See naming
                scheme in the code... To successfully import that
                stuff into MongoDB, the following keys are expected
                in that json file: "method_structure", "oligomeric_state",
                "biounit", "experimental_pdbid", "template_pdbid"
    """
    if progress_file:
        _UpdateProgress(progress_file, "Importing variants and structures")

    # data_json precedence over user_data_json/user_structure_dir
    if os.path.exists(data_json):
        if progress_file:
            _UpdateProgress(progress_file, "Import data from precomputed JSON")

        with open(data_json, "r") as fh:
            data = pipeline.DataContainer.FromJSON(json.load(fh))
        return data  # already done

    # no data_json, let's go with user_data_json/user_structure_dir
    with open(user_data_json, "r") as fh:
        in_data = json.load(fh)

    s = seq.CreateSequence(in_data["AC"], in_data["sequence"])
    data = pipeline.DataContainer(s)

    hr_var_importer = var_importer.HRVarImporter()
    for var_string in in_data["variants"]:
        hr_var_importer.Add(
            s, var_string, meta={"variant_label": "USER_VARIANT"}
        )

    fs_struct_importer = struct_importer.FilesystemStructImporter()
    if user_structure_dir and os.path.exists(user_structure_dir):
        files = os.listdir(user_structure_dir)
        for f in files:
            try:
                entity_path = os.path.join(user_structure_dir, f)
                ent = io.LoadEntity(entity_path)
                # success, let's check for the corresponding meta file
                meta_f = entity_path.rstrip(".gz")
                meta_f = meta_f.rstrip(".pdb").rstrip(".cif").rstrip(".mmcif")
                meta_f += "_meta.json"
                meta_path = os.path.join(user_structure_dir, meta_f)
                if not os.path.exists(meta_path):
                    raise RuntimeError(
                        f"Structure {structure_f} does not come "
                        f"with associated meta file "
                        f"({meta_path})."
                    )
                with open(meta_path, "r") as fh:
                    meta_data = json.load(fh)
                fs_struct_importer.Add(
                    data.sequence, f, entity_path, meta=meta_data
                )
            except:
                pass  # try except pass always feels so refreshing
    data_import_pipeline = pipeline.DataImportPipeline(
        var_importer=[hr_var_importer], struct_importer=[fs_struct_importer]
    )
    data = data_import_pipeline.Import(data)
    return data


def _parse_args():

    desc = (
        "Computes annotations required by tbvar3d. Polls MongoDB if stuff "
        + "is already pre-computed and performs computation of the rest."
    )
    parser = argparse.ArgumentParser(description=desc)

    # parameters that define simplified "user pipeline" input... an alternative
    # way of input would be to define --data_json
    parser.add_argument(
        "--user_data_json",
        help="json file with input data of form: "
        '{"AC": ac, "sequence": s, "variants":[v1,...]}. '
        "Will only be used if --data_json doesn't exist already",
    )
    parser.add_argument(
        "--user_structure_dir",
        default=None,
        help="All files in there that can be loaded with "
        "ost.io.LoadEntity will be imported using the "
        "var3d.struct_importer.FilesystemStructImporter. "
        "Will only be used if --data_json doesn't exist already.",
    )

    # var3d specific data objects containing data and annotations. If given,
    # results will be stored there. Alternatively, if --data_json already exists
    # when starting the pipeline, this is used as starting point and
    # --user_data_json and --user_structure_dir have no effect
    parser.add_argument(
        "--data_json",
        default=None,
        help="Output path for json formatted data - if this "
        "file exists, user_data_json won't be used",
    )
    parser.add_argument(
        "--anno_json",
        default=None,
        help="Output path for json formatted annotations",
    )

    # databases required for annotation
    parser.add_argument(
        "--uniref90",
        help="path to uniref90 db in fasta "
        "format - used to derive alignments for consurf and "
        "entropy.",
    )
    parser.add_argument(
        "--nr",
        help="Path to blast nr db - used to derive " "alignment for provean",
    )

    # Keep track on what's going on
    parser.add_argument(
        "--status_file",
        default=None,
        help="Updates computation status (RUNNING, COMPLETED, "
        "FAILED). If given, the file must already exist",
    )
    parser.add_argument(
        "--progress_file",
        default=None,
        help="Logs progress in human readable text. Will be "
        "created if given but not present",
    )

    # MongoDB parameters, relevant for caching or if you wish to import projects
    # for the tbvar3d frontent
    parser.add_argument(
        "--mongo_host",
        default=None,
        help="Connects to host in MongoDB url format "
        "(e.g. mongodb://localhost:27017/) and accesses db "
        "speciefied by mongo_db argument. Uses the"
        "var3d_annotations collection to cache annotations "
        "based on md5 sequence hashes.",
    )
    parser.add_argument(
        "--mongo_db",
        default=None,
        help="db name used for caching (see --mongo_host)",
    )

    # parameters that are relevant if actual projects for tbvar3d frontend
    # should be imported
    parser.add_argument(
        "--object_id",
        default=None,
        help="ObjectID of Var3D mongo document. If given and "
        "mongo_db is valid, that project in MongoDB will be "
        "updated to container the newly extracted data and "
        "annotations.",
    )
    parser.add_argument(
        "--project_id",
        default=None,
        help="Only considered if object_id is None. If thats "
        "the case and project_id is set, a new document is "
        "added to mongo_db with that project_id",
    )
    parser.add_argument(
        "--do_not_delete",
        default=False,
        action="store_true",
        help="Added to project if its newly added.",
    )
    args = parser.parse_args()

    if args.data_json is not None and os.path.exists(args.data_json):
        pass  # data_json is used as data input => user_data_json,
              # user_structure_dir are irrelevant
    elif not os.path.exists(
        args.user_data_json
    ):  # user_structure_dir is optional...
        raise RuntimeError("--user_data_json is non-existent")

    if not os.path.exists(args.uniref90):
        raise RuntimeError("--uniref90 is non-existent")
    if not glob.glob(
        args.nr + "*"
    ):  # checks for files starting with that prefix
        raise RuntimeError("--nr is non-existent")
    if args.status_file is not None:
        if not os.path.exists(args.status_file):
            raise RuntimeError("--status_file is non-existent")
    return args


def main():

    args = _parse_args()

    try:
        if args.status_file:
            _write_status(args.status_file, "RUNNING")

        ###############
        # SETUP MONGO #
        ###############
        mongo_client = None
        mongo_db = None
        if args.mongo_host and args.mongo_db:
            mongo_client = MongoClient(
                args.mongo_host, tz_aware=True, serverSelectionTimeoutMS=6000
            )
            mongo_db = mongo_client[args.mongo_db]

        ###############
        # DATA IMPORT #
        ###############
        data = _data_import(
            args.user_data_json,
            args.user_structure_dir,
            args.data_json,
            args.progress_file,
        )

        ###############
        # ANNOTATIONS #
        ###############
        anno = _annotate(
            args.anno_json,
            data,
            mongo_db,
            args.progress_file,
            args.uniref90,
            args.nr,
        )

        ##############
        # FINALIZING #
        ##############
        if args.progress_file:
            _UpdateProgress(args.progress_file, "Finalizing")

        if args.data_json is not None:
            with open(args.data_json, "w") as fh:
                json.dump(data.ToJSON(), fh)

        if args.anno_json is not None:
            with open(args.anno_json, "w") as fh:
                json.dump(anno, fh)

        if mongo_db is not None:
            # updates annotation caches and, if the according parameters
            # are given, adds a project to MongoDB that can be accessed by
            # the frontend
            _update_mongo(
                mongo_db,
                data,
                anno,
                args.object_id,
                args.do_not_delete,
                args.project_id,
            )

        if args.status_file:
            _write_status(args.status_file, "COMPLETED")

        if args.progress_file:
            _UpdateProgress(args.progress_file, "Done")

    except Exception as e:
        if args.status_file:
            _write_status(args.status_file, "FAILED")
        traceback.print_exc()
        raise


if __name__ == "__main__":
    main()
