from ost import seq
from ost import io
import hashlib
import os
import json

v3d_basedir = "<PATH_TO_VAR3D_REPO>"
datadir = "<PATH_TO_DATADIR>"  # expected to contain file "uniref90.fasta" and
                               # directory "nr" for PROVEAN

v3d_executable = os.path.join(v3d_basedir, "v3d")
proteom_fasta = os.path.join(
    v3d_basedir, "pipelines", "tbvar3d", "uniprot-proteome_UP000001584.fasta"
)
uniref90 = os.path.join(datadir, "uniref90.fasta")
nr = os.path.join(datadir, "nr") + "/nr"
cwd = os.getcwd()
anno_dir = os.path.join(cwd, "annotations")
provean_supplement_dir = os.path.join(cwd, "provean_supplement_files")
stdout_dir = os.path.join(cwd, "stdout")
script = os.path.join(v3d_basedir, "pipelines", "tbvar3d", "compute_seqanno.py")

if not os.path.exists(anno_dir):
    os.makedirs(anno_dir)

if not os.path.exists(provean_supplement_dir):
    os.makedirs(provean_supplement_dir)

if not os.path.exists(stdout_dir):
    os.makedirs(stdout_dir)

sequence_list = io.LoadSequenceList(proteom_fasta)

commands = list()

for s in sequence_list:
    uniprot_ac = s.GetName().split("|")[1]
    sequence = str(s)
    h = hashlib.md5(sequence.encode()).hexdigest()

    anno_file = os.path.join("annotations", h)

    rerun = False
    if not os.path.exists(anno_file):
        rerun=True
    else:
        with open(anno_file) as fh:
            data = json.load(fh)
        if data["seq_anno"]["consurf"] is None or data["seq_anno"]["entropy"] is None:
            rerun=True

    if not rerun:
        continue

    cmd = [
        v3d_executable,
        "--mount",
        datadir,
        script,
        "--uniref90",
        uniref90,
        "--nr",
        nr,
        "--uniprot_ac",
        uniprot_ac,
        "--sequence",
        sequence,
        "--anno_dir",
        anno_dir,
        "--provean_supplement_dir",
        provean_supplement_dir,
    ]
    commands.append(" ".join(cmd))

with open("compute_seqanno_commands.cmd", "w") as fh:
    fh.write("\n".join(commands))
