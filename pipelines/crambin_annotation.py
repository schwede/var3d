import json

from var3d import base
from var3d import uniprot
from var3d import msa_generator
from var3d import struct_importer
from var3d import seq_anno
from var3d import var_seq_anno
from var3d import var_struct_anno
from var3d import pipeline

# var3d pipelines are a two step process. Data import
# followed by annotation

##############################
# SETUP DATA IMPORT PIPELINE #
##############################

# Structures are imported from SMR. The SMR importer knows
# from the incoming sequence what to import
smr_importer = struct_importer.SMRStructImporter()

# We have this awesome variant just for this sequence...
# Let's sublass VarImporter! For this we implement the
# Run and the Key method
class YoloVarImporter(base.VarImporter):
    def Run(self, sequence, seq_range=None):
        if sequence.GetName() == "P01542":
            v = base.Variant(
                "C", "W", 4, base.VariantType.SUBSTITUTION
            )
            return [v]
        return []

    def Key(self):
        return "yolo"


yolo_importer = YoloVarImporter()

# That's it, we can stitch together a data import pipeline
import_pipeline = pipeline.DataImportPipeline(
    var_importer=[yolo_importer],
    struct_importer=[smr_importer],
)

#############################
# SETUP ANNOTATION PIPELINE #
#############################

# Here we're interested in some sequence annotations. Let's
# do entropy and consurf. Both are based on uniref90. While
# consurf is a fixed pipeline, the alignment for the entropy
# annotation can be constructed flexibly. Let's use
# jackhmmer
uniref90 = "/home/schdaude/data/uniref90.fasta"
jackhmmer_msa_generator = msa_generator.JackhmmerMSAGenerator(
    uniref90
)
entropy_anno = seq_anno.EntropySeqAnno(
    jackhmmer_msa_generator
)
consurf_anno = seq_anno.ConsurfSeqAnno(uniref90)

# Who can resist the sausage forcefield for variant analyis
sausage_anno = var_seq_anno.AAIndexVarSeqAnno(
    "DOSZ010101", "sausage"
)

# and some structure annotation
foldx_anno = var_struct_anno.FoldXVarStructAnno()

# That's it, we can stitch together an annotation pipeline
anno_pipeline = pipeline.AnnotationPipeline(
    seq_anno=[entropy_anno, consurf_anno],
    var_seq_anno=[sausage_anno],
    var_struct_anno=[foldx_anno],
)

#############
# GOGOGOGO! #
#############

# the molecule of life, the universe and everything: crambin
crambin_seq = uniprot.GetUniprotSequence("P01542")

data = pipeline.DataContainer(crambin_seq)
data = import_pipeline.Import(data)

# Let's check the numbers
print("structures imported:", len(data.structures))
print("variants imported:", len(data.variants))

# dump it to disk
with open("my_data.json", "w") as fh:
    json.dump(data.ToJSON(), fh)

# AAAAAAAAND Annotate!
anno = anno_pipeline.Run(
    data,
    variant_hashes=data.variant_hashes,
    structure_hashes=data.structure_hashes,
)

# dump it to disk
with open("my_annotations.json", "w") as fh:
    json.dump(anno, fh)
