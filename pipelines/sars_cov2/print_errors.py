import os
import json

anno_files = os.listdir("anno")

for f in anno_files:
    with open(os.path.join("anno", f), "r") as fh:
        data = json.load(fh)
    if len(data["err"]):
        print(f)
        print(data["err"])
