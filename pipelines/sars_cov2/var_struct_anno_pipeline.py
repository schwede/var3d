import os
import json
import argparse

from var3d import var_struct_anno
from var3d import pipeline


def _parse_args():
    desc = (
        "SARS-CoV2 pipeline, runs FoldX for a variant/structure pair. "
        + "Outsourced from other annotations due to computational complexity."
    )
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "--data", required=True, help="Path to var3d DataContainer file"
    )
    parser.add_argument(
        "--out", required=True, help="Path for json formatted annotation output"
    )
    parser.add_argument(
        "--variant_hash",
        required=True,
        help="Hash of variant in data container",
    )
    parser.add_argument(
        "--structure_hash",
        required=True,
        help="Hash of structure in data container",
    )
    return parser.parse_args()


args = _parse_args()

if not os.path.exists(args.data):
    raise RuntimeError("Input DataContainer file doesn't exist")

with open(args.data, "r") as fh:
    data = pipeline.DataContainer.FromJSON(json.load(fh))

if not args.structure_hash in data.structure_hashes:
    raise RuntimeError("Structure with specified hash not in data container")

if not args.variant_hash in data.variant_hashes:
    raise RuntimeError("Variant with specified hash not in data container")

# Structure based variant annotations
foldx_anno = var_struct_anno.FoldXVarStructAnno()

anno_pipeline = pipeline.AnnotationPipeline(var_struct_anno=[foldx_anno])

anno = anno_pipeline.Run(
    data,
    structure_hashes=[args.structure_hash],
    variant_hashes=[args.variant_hash],
)

with open(args.out, "w") as fh:
    json.dump(anno, fh)
