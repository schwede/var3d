import os
import json

from var3d import pipeline

files = os.listdir("anno")

basenames = list()
for f in files:
    if "base" in f:
        basenames.append(f.split("_base")[0])

basenames.append("P0DTC2")

annotation_files = dict()
for bn in basenames:
    annotation_files[bn] = list()

for f in files:
    for bn in basenames:
        if f.startswith(bn):
            annotation_files[bn].append(os.path.join("anno", f))

print(basenames)
print("P0DTC2" in basenames)


for bn in basenames:
    print("collect", bn)
    anno = pipeline.CollectAnnotations(annotation_files[bn])
    with open(bn + "_anno.json", "w") as fh:
        json.dump(anno, fh)
