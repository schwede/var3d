import os
import json
import sys

from var3d import base
from var3d import pipeline

if len(sys.argv) != 4:
    print("USAGE: v3d anno_commands.py <VAR3D_REPO> <UNIREF90> <NR>")
    sys.exit(1)

var3d_basedir = "/scicore/home/schwede/studga00/prog/var3d"
uniref90 = (
    "/scicore/home/schwede/studga00/workspace/sars_cov2_anno/uniref90.fasta"
)
blast_nr = "/scicore/home/schwede/studga00/workspace/sars_cov2_anno/nr/nr"

if not os.path.exists("anno"):
    os.makedirs("anno")

v3d_exec = os.path.join(var3d_basedir, "v3d")
anno_base_script = os.path.join(
    var3d_basedir, "pipelines", "sars_cov2", "anno_pipeline_base.py"
)
anno_var_struct_script = os.path.join(
    var3d_basedir, "pipelines", "sars_cov2", "var_struct_anno_pipeline.py"
)

# CREATE COMMANDS
#################
commands = list()
data_files = os.listdir("data")

for f in data_files:

    data_path = os.path.join("data", f)

    # base anno pipeline
    out_path = os.path.join("anno", f.split(".")[0] + "_base.json")
    cmd = [
        v3d_exec,
        anno_base_script,
        "--data",
        data_path,
        "--out",
        out_path,
        "--uniref90",
        uniref90,
        "--nr",
        blast_nr,
    ]
    commands.append(" ".join(cmd))

    # structure based variant anno pipeline requires splitting
    with open(data_path, "r") as fh:
        data = pipeline.DataContainer.FromJSON(json.load(fh))
    for v_h in data.variant_hashes:
        v = data.GetVariantFromHash(v_h)
        v_range = v.GetRange()
        for s_h in data.structure_hashes:
            s = data.GetStructureFromHash(s_h)
            if not s.InRange(v_range):
                continue
            tmp = "_" + "_".join([v_h, s_h])
            out_path = os.path.join("anno", f.split(".")[0] + tmp + ".json")
            cmd = [
                v3d_exec,
                anno_var_struct_script,
                "--data",
                data_path,
                "--out",
                out_path,
                "--variant_hash",
                v_h,
                "--structure_hash",
                s_h,
            ]
            commands.append(" ".join(cmd))

with open("anno.cmd", "w") as fh:
    fh.write("\n".join(commands))
