SARS-CoV-2 annotation pipeline
==============================

The pipeline annotates sequences and available structures of uniprot reference 
proteome [UP000464024](https://www.uniprot.org/proteomes/UP000464024). The
ORF1ab polyprotein is split into mature proteins prior to annotation.
Sequences and manually curated variants are imported from the covid19 specific
[pre-release uniprot resource](https://covid-19.uniprot.org) in order to get
the latest available data. Structural data are imported from the 
[SWISS-MODEL Repository](https://swissmodel.expasy.org/repository).
The full annotation pipeline is a two step process: 1) data import 2) annotation

Data import
-----------

The following command performs the split of the polyprotein and generates a 
DataContainer for each mature protein. The results are dumped as json files in
the directory `data` which is created in the current working directory if not
already there.

```terminal
<PATH_TO_VAR3D_REPO>/v3d data_import.py
```

Annotation
----------

Annotations relate to the previously imported data. There are two annotation 
scripts. Reason for that is the computational complexity for structure based
annotations. The first, `anno_pipeline_base.py`, is concerned with sequence 
and sequence based variant annotations. The second, 
`var_struct_anno_pipeline.py`, performs structure based variant annotations.

Commands to be executed are generated with `anno_commands.py`. The script
requires the path to the var3d repository and paths to the following databases
as input:

* uniref90 - Downloaded from [uniprot](https://www.uniprot.org/downloads), 
             sequence database in fasta format. Required to perform entropy and
             consurf sequence annotations. The parameter to provide is the path
             to the fasta file.
* nr - BLAST nr database to perform sequence based provean variant annotations.
       Currently, we're using the freezed nr database they used in their
       publication [url](http://provean.jcvi.org/downloads.php).
       If the database is in directory `<PATH>` and has prefix `nr`, the
       parameter you need to provide is: `<PATH>/nr`

```terminal
<PATH_TO_VAR3d_REPO>/v3d anno_commands.py <PATH_TO_VAR3D_REPO> <UNIREF90> <NR>
```

This creates the directory `anno` in the current working directory and the file
`anno.cmd`. The latter must be submitted as batch job. The following submission
script should do the job on the sciCORE infrastructure (specify <N> as numbers
of lines in `anno.cmd` and make sure to create `stdout` directory):

```terminal
#!/bin/bash
#SBATCH --job-name=sars_cov2_anno
#SBATCH --qos=1day
#SBATCH --time=24:00:00
#SBATCH --output=stdout/%A_%a.out
#SBATCH --mem=16G
#SBATCH --array=1-<N>

ml Python/3.6.6-foss-2018b
SEEDFILE=anno.cmd
SEED=$(sed -n ${SLURM_ARRAY_TASK_ID}p $SEEDFILE)
eval $SEED
```
Once done, you may want to check if an output JSON has been generated
for each command. Errors caught during processing are stored in these JSONs,
so run: 
```terminal
<PATH_TO_VAR3d_REPO>/v3d print_errors.py
```

This script crawls through every file in the anno directory and prints
potential errors. Some ConSurf failures have been observed. They're
related to a non-sufficient number of found sequences in nr90 
(ConSurf wants 50). A couple of FoldX errors have been observed too
but not further investigated. A few input structures seem to crash FoldX,
i.e. no variants at all could be computed for them.

In a last step you want to collect the annotations for each target
sequence:
```terminal
<PATH_TO_VAR3d_REPO>/v3d collect_annotations.py
```
