import os
import json
import argparse

from var3d import msa_generator
from var3d import seq_anno
from var3d import struct_anno
from var3d import var_seq_anno
from var3d import pipeline


def _parse_args():
    desc = (
        "SARS-CoV2 pipeline, runs normalized Shannon Entropy based on "
        + "jackhmmer alignment and Consurf-DB to annotate sequence, "
        + "relative per residue solvent accessibility annotation on all "
        + "structures and Provean on all variants in DataContainer. "
        + "Structural based variant annotations are outsourced due to "
        + "computational complexity in case of many structures (e.g. Spike)"
    )
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "--data", required=True, help="Path to var3d DataContainer file"
    )
    parser.add_argument(
        "--out", required=True, help="Path for json formatted annotation output"
    )
    parser.add_argument(
        "--uniref90",
        required=True,
        help="Path to uniref90 in fasta format used for "
        "Shannon Entropy and Consurf-DB",
    )
    parser.add_argument(
        "--nr", required=True, help="Path to BLAST nr database used by Provean"
    )
    return parser.parse_args()


args = _parse_args()

if not os.path.exists(args.data):
    raise RuntimeError("Input DataContainer file doesn't exist")
if not os.path.exists(args.uniref90):
    raise RuntimeError("Uniref90 file doesn't exist")

# sequence annotations
jackhmmer_msa_generator = msa_generator.JackhmmerMSAGenerator(args.uniref90)
entropy_anno = seq_anno.EntropySeqAnno(jackhmmer_msa_generator)
consurf_anno = seq_anno.ConsurfSeqAnno(args.uniref90)

# structure annotations
accessibility_anno = struct_anno.AccessibilityStructAnno(normalize=True)

# Sequence based variant annotations
# constructor of Provean annotator checks for existence of args.nr
provean_anno = var_seq_anno.ProveanVarSeqAnno(args.nr)

anno_pipeline = pipeline.AnnotationPipeline(
    seq_anno=[entropy_anno, consurf_anno],
    struct_anno=[accessibility_anno],
    var_seq_anno=[provean_anno],
)

with open(args.data, "r") as fh:
    data = pipeline.DataContainer.FromJSON(json.load(fh))

anno = anno_pipeline.Run(data)

with open(args.out, "w") as fh:
    json.dump(anno, fh)
