import os
import json

from ost import io
from ost import seq

from var3d import uniprot
from var3d import pipeline
from var3d import var_importer
from var3d import struct_importer

if not os.path.exists("data"):
    os.makedirs("data")

uniprot_url = "https://www.ebi.ac.uk/uniprot/api/covid-19/uniprotkb/accession"

# Define data import pipeline which fetches curated variants from uniprot
# and structures from SMR
v_imp = var_importer.UniprotEntryVarImporter(url=uniprot_url)
s_imp = struct_importer.SMRStructImporter()
import_pipeline = pipeline.DataImportPipeline(
    var_importer=[v_imp], struct_importer=[s_imp]
)

# Process polyprotein
P0DTD1 = uniprot.FetchUniprotEntry("P0DTD1", url=uniprot_url)
P0DTD1_seq = seq.CreateSequence("P0DTD1", uniprot.ParseUniprotSequence(P0DTD1))
P0DTD1_chain_ft = uniprot.ParseUniprotFT(P0DTD1, "CHAIN")
for ft in P0DTD1_chain_ft:
    if ft.note_qualifier == "Replicase polyprotein 1ab":
        continue  # only process mature proteins
    seq_identifier = ft.note_qualifier
    seq_range = (ft.ft_start, ft.ft_end)
    print("processing", "P0DTD1", seq_range)
    data = pipeline.DataContainer(
        P0DTD1_seq, seq_identifier=seq_identifier, seq_range=seq_range
    )
    data = import_pipeline.Import(data)
    fn = "P0DTD1_" + "_".join([str(seq_range[0]), str(seq_range[1])]) + ".json"
    with open(os.path.join("data", fn), "w") as fh:
        json.dump(data.ToJSON(), fh)

# Process remaining entries in uniprot proteome UP000464024
def Process(AC):
    print("processing", AC)
    entry = uniprot.FetchUniprotEntry(AC, url=uniprot_url)
    sequence = seq.CreateSequence(AC, uniprot.ParseUniprotSequence(entry))
    data = pipeline.DataContainer(sequence)
    data = import_pipeline.Import(data)
    fn = AC + ".json"
    with open(os.path.join("data", fn), "w") as fh:
        json.dump(data.ToJSON(), fh)


acs = [
    "A0A663DJA2",
    "P0DTC2",
    "P0DTC3",
    "P0DTC4",
    "P0DTC5",
    "P0DTC6",
    "P0DTC7",
    "P0DTC8",
    "P0DTC9",
    "P0DTD2",
    "P0DTD3",
    "P0DTD8",
    "P0DTF1",
    "P0DTG0",
    "P0DTG1",
]

for ac in acs:
    Process(ac)
