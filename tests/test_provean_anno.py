import unittest
import os
import tempfile
import stat

from ost import seq
from ost import io

from var3d.base import *
from var3d.var_seq_anno import ProveanVarSeqAnno


class TestProveanAnno(unittest.TestCase):
    def setUp(self):
        self.tmp_dir = tempfile.TemporaryDirectory()

        # generate fake nrdb
        self.nrdb_dir = os.path.join(self.tmp_dir.name, "nr")
        self.nrdb_prefix = "nr"
        self.nrdb = os.path.join(self.nrdb_dir, self.nrdb_prefix)
        os.makedirs(self.nrdb_dir)
        open(self.nrdb, 'a').close()

        # generate fake executables
        self.provean_exe = os.path.join(self.tmp_dir.name, "provean")
        self.psiblast_exe = os.path.join(self.tmp_dir.name, "psiblast")
        self.cdhit_exe = os.path.join(self.tmp_dir.name, "cd-hit")
        self.blastdbcmd_exe = os.path.join(self.tmp_dir.name, "blastdbcmd")
        open(self.provean_exe, "a").close()
        open(self.psiblast_exe, "a").close()
        open(self.cdhit_exe, "a").close()
        open(self.blastdbcmd_exe, "a").close()

        # make them executable
        st = os.stat(self.provean_exe)
        os.chmod(self.provean_exe, st.st_mode | stat.S_IEXEC)
        st = os.stat(self.psiblast_exe)
        os.chmod(self.psiblast_exe, st.st_mode | stat.S_IEXEC)
        st = os.stat(self.cdhit_exe)
        os.chmod(self.cdhit_exe, st.st_mode | stat.S_IEXEC)
        st = os.stat(self.blastdbcmd_exe)
        os.chmod(self.blastdbcmd_exe, st.st_mode | stat.S_IEXEC)

        # generate another bunch of fake executables but don't make them
        # executable...
        self.provean_nox = os.path.join(self.tmp_dir.name, "provean_nox")
        self.psiblast_nox = os.path.join(self.tmp_dir.name, "psiblast_nox")
        self.cdhit_nox = os.path.join(self.tmp_dir.name, "cd-hit_nox")
        self.blastdbcmd_nox = os.path.join(self.tmp_dir.name, "blastdbcmd_nox")
        open(self.provean_nox, "a").close()
        open(self.psiblast_nox, "a").close()
        open(self.cdhit_nox, "a").close()
        open(self.blastdbcmd_nox, "a").close()

    def tearDown(self):
        self.tmp_dir.cleanup()

    def testProveanVarSeqAnnoConstructor(self):
        # try with non-existent nrdb
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                "/i/do/not/exist",
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        # try only with nrdb dir but without prefix
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb_dir,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

        # see whether check of non executable exe files is performed. Search for
        # executables in path is not tested as this is system dependent.
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_nox,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe="i/do/not/exist",
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_nox,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe="i/do/not/exist",
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_nox,
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe="i/do/not/exist",
                blastdbcmd_exe=self.blastdbcmd_exe,
            )
        ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe=self.blastdbcmd_nox,
            )
        with self.assertRaises(ValueError):
            ProveanVarSeqAnno(
                self.nrdb,
                tmp_dir=self.tmp_dir.name,
                provean_exe=self.provean_exe,
                psiblast_exe=self.psiblast_exe,
                cdhit_exe=self.cdhit_exe,
                blastdbcmd_exe="i/do/not/exist",
            )
        ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

    def testProveanVarSeqAnnoGetKey(self):
        anno = ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )
        self.assertEqual(anno.Key(), "provean")
        anno = ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            key="asdf",
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )
        self.assertEqual(anno.Key(), "asdf")

    def testProveanVarSeqAnnoInputPrep(self):
        # check the two input preparation functions _GetProveanInputEncoding
        # and _PrepareProveanInput. We're not actually executing provean here...
        s = seq.CreateSequence(
            "A",
            "MEEPQSDPSVEPPLSQETFSDLWKLLPENNVLSPLPSQAMDD"
            "LMLSPDDIEQWFTEDPGPDEAPRMPEAAPPVAPAPAAPTPAA"
            "PAPAPSWPLSSSVPSQKTYQGSYGFRLGFLHSGTAKSVTCTY"
            "SPALNKMFCQLAKTCPVQLWVDSTPPPGTRVRAMAIYKQSQH"
            "MTEVVRRCPHHERCSDSDGLAPPQHLIRVEGNLRVEYLDDRN"
            "TFRHSVVVPYEPPEVGSDCTTIHYNYMCNSSCMGGMNRRPIL"
            "TIITLEDSSGNLLGRNSFEVRVCACPGRDRRTEEENLRKKGE"
            "PHHELPPGSTKRALPNNTSSSPQPKKKPLDGEYFTLQIRGRE"
            "RFEMFRELNEALELKDAQAGKEPGGSRAHSSHLKSKKGQSTS"
            "RHKKLMFKTEGPDSD",
        )
        anno = ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

        # thats the variants from the example distributed with provean
        variants = list()
        variants.append(Variant("P", "R", 72, VariantType.SUBSTITUTION))
        variants.append(Variant("G", "C", 105, VariantType.SUBSTITUTION))
        # sneak in a variant that won't be processed
        variants.append(Variant("P", "*", 72, VariantType.FRAMESHIFT))
        variants.append(Variant("K", "", 370, VariantType.DELETION))
        variants.append(Variant("H", "HPHP", 178, VariantType.INSERTION))
        variants.append(Variant("LW", "QS", 22, VariantType.INDEL))

        input_encoding = anno._GetProveanInputEncoding(variants)
        input_encoding = sorted([[item[0], item[1]] for item in input_encoding])
        self.assertEqual(
            input_encoding,
            [
                [0, "72,P,R"],
                [1, "105,G,C"],
                [3, "370,K,."],
                [4, "178,H,HPHP"],
                [5, "22,LW,QS"],
            ],
        )

        # yet another test for encoding (internally, the exact same function
        # gets called)
        seq_file, var_file, encoding, supporting_set_file = anno._PrepareProveanInput(
            s, variants
        )
        tmp = sorted([[item[0], item[1]] for item in encoding])
        self.assertEqual(tmp, input_encoding)

        # check the content of the files that serve as provean input
        loaded_s = io.LoadSequence(seq_file)
        self.assertEqual(s.GetName(), loaded_s.GetName())
        self.assertEqual(str(s), str(loaded_s))

        with open(var_file, "r") as fh:
            loaded_var = fh.readlines()
        loaded_var = [line.strip() for line in loaded_var]
        tmp = [item[1] for item in input_encoding]
        self.assertEqual(sorted(loaded_var), sorted(tmp))

    def testProveanVarSeqAnnoGetCommand(self):
        anno = ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )
        anno_cmd = anno._GetCommand(
            "dummy_seq_file.fasta", "dummy_var_file.var", "non_existent"
        )
        self.assertEqual(
            anno_cmd,
            [
                self.provean_exe,
                "-d",
                self.nrdb,
                "-q",
                "dummy_seq_file.fasta",
                "-v",
                "dummy_var_file.var",
                "--cdhit",
                self.cdhit_exe,
                "--psiblast",
                self.psiblast_exe,
                "--blastdbcmd",
                self.blastdbcmd_exe,
                "--tmp_dir",
                self.tmp_dir.name,
            ],
        )

    def testProveanVarSeqAnnoResultParsing(self):
        anno = ProveanVarSeqAnno(
            self.nrdb,
            tmp_dir=self.tmp_dir.name,
            provean_exe=self.provean_exe,
            psiblast_exe=self.psiblast_exe,
            cdhit_exe=self.cdhit_exe,
            blastdbcmd_exe=self.blastdbcmd_exe,
        )

        provean_out = [
            "## PROVEAN v1.1 output ##",
            "## Input Arguments ##",
            "/usr/local/bin/provean -q /home/schdaude/prog/var3d/container/provean/P04637.fasta -v /home/schdaude/prog/var3d/container/provean/P04637_test.var -d /home/schdaude/workspace/ARvar3D/data/nr/nr --cdhit /usr/bin/cd-hit --psiblast /usr/bin/psiblast --blastdbcmd /usr/bin/blastdbcmd --tmp_dir /home/schdaude/prog/var3d/container/provean/tmp",
            "",
            "## Parameters ##",
            "# Query sequence file:	/home/schdaude/prog/var3d/container/provean/P04637.fasta",
            "# Variation file:	/home/schdaude/prog/var3d/container/provean/P04637_test.var",
            "# Protein database:	/home/schdaude/workspace/ARvar3D/data/nr/nr",
            "# Supporting sequence set file (optional):	Not provided",
            "# Supporting sequence set file for storing (optional):	Not provided",
            "# Substitution matrix:	BLOSUM62",
            "# Gap costs:	10, 1",
            "# Clustering threshold:	0.750",
            "# Maximum number of clusters:	30",
            "",
            "[12:03:53] loading query sequence from a FASTA file...",
            "[12:03:53] loading variations...",
            "[12:03:53] searching related sequences...",
            "[12:06:12] retrieving subject sequence information...",
            "[12:06:12] clustering subject sequences...",
            "[12:06:12] selecting clusters...",
            "[12:06:12] 363 subject sequences in 30 clusters were selected for supporting sequences.",
            "[12:06:12] loading subject sequences from a FASTA file...",
            "# Number of clusters:	30",
            "# Number of supporting sequences used:	363",
            "[12:06:12] computing delta alignment scores...",
            "[12:06:14] printing PROVEAN scores...",
            "## PROVEAN scores ##",
            "# VARIATION	SCORE",
            "72,P,R	0.155",
            "105,G,C	-8.019",
            "370,K,.	-1.545",
            "178,H,HPHP	-10.293",
            "22,LW,QS	-10.399",
        ]

        parsed_provean_out = anno._ParseProveanResults(provean_out)
        parsed_provean_out = [[k, v] for k, v in parsed_provean_out.items()]

        exp_provean_out = [
            ["72,P,R", 0.155],
            ["105,G,C", -8.019],
            ["370,K,.", -1.545],
            ["178,H,HPHP", -10.293],
            ["22,LW,QS", -10.399],
        ]
        self.assertEqual(sorted(parsed_provean_out), sorted(exp_provean_out))


if __name__ == "__main__":
    unittest.main()
