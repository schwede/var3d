import unittest
import os
import random
import time

from ost import seq
from ost import io
from ost import geom

from var3d.base import *


class TestStructure(unittest.TestCase):
    def setUp(self):
        unit_test_dir = os.path.dirname(os.path.realpath(__file__))
        data_dir = os.path.join(unit_test_dir, "data")
        self.assembly = io.LoadPDB(os.path.join(data_dir, "4nf2.1.pdb"))
        self.assembly_view = self.assembly.CreateFullView()
        self.seqres = io.LoadSequence(os.path.join(data_dir, "4nf2.1.fasta"))
        self.aln = seq.alg.AlignToSEQRES(
            self.assembly.FindChain("A"), self.seqres
        )

    def testConstructorTypeChecks(self):

        # sequence param
        with self.assertRaises(TypeError):
            s = Structure(42, "asdf", self.assembly, "cname=A", aln=self.aln)
        # structure_identifier param
        with self.assertRaises(TypeError):
            s = Structure(
                self.seqres, 42, self.assembly, "cname=A", aln=self.aln
            )
        # assembly param
        with self.assertRaises(TypeError):
            s = Structure(self.seqres, "asdf", 42, "cname=A", aln=self.aln)
        # segment_query param
        with self.assertRaises(TypeError):
            s = Structure(self.seqres, "asdf", self.assembly, 42, aln=self.aln)
        # aln param
        with self.assertRaises(TypeError):
            s = Structure(self.seqres, "asdf", self.assembly, "cname=A", aln=42)
        # strict param
        with self.assertRaises(TypeError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=self.aln,
                strict=42,
            )

        # thats the types that should work
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=self.aln,
            strict=False,
        )
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            ["cname=A"],
            aln=self.aln,
            strict=False,
        )
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=[self.aln],
            strict=False,
        )

    def testStructureConstructor(self):

        # provide aln with 1 sequence or 3 sequences (expects exactly 2)
        aln = seq.CreateAlignment()
        aln.AddSequence(self.aln.GetSequence(0))
        with self.assertRaises(RuntimeError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=aln,
                strict=False,
            )
        aln.AddSequence(self.aln.GetSequence(1))
        s = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", aln=aln, strict=False
        )  # that should work
        aln.AddSequence(seq.CreateSequence("X", "-" * len(aln.GetSequence(0))))
        with self.assertRaises(RuntimeError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=aln,
                strict=False,
            )

        # sequences in structure aln are set to self.seqres.GetName() and A
        a = s.aln[0]
        self.assertTrue(a.GetSequence(0).GetName() == self.seqres.GetName())
        self.assertTrue(a.GetSequence(1).GetName() == "A")

        # introduce mismatches in SEQRES and ATOMSEQ
        mismatch_seqres = str(self.aln.GetSequence(0)).replace("A", "X")
        mismatch_atomseq = str(self.aln.GetSequence(1)).replace("A", "X")

        # check mismatch in SEQRES
        aln = seq.CreateAlignment()
        aln.AddSequence(seq.CreateSequence("seqres", mismatch_seqres))
        aln.AddSequence(self.aln.GetSequence(1))
        with self.assertRaises(RuntimeError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=aln,
                strict=False,
            )

        # check mismatch in ATOMSEQ
        aln = seq.CreateAlignment()
        aln.AddSequence(self.aln.GetSequence(0))
        aln.AddSequence(seq.CreateSequence("atomseq", mismatch_atomseq))
        with self.assertRaises(RuntimeError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=aln,
                strict=False,
            )

        # check whether we're doing fine also without providing an alignment
        # (with and without strict)
        s = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", strict=False
        )
        s = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", strict=True
        )

        # add gap to SEQRES which should give an error in "strict mode"
        gap_seqres = "-" + str(self.aln.GetSequence(0))
        gap_atomseq = "-" + str(self.aln.GetSequence(1))
        aln = seq.CreateAlignment()
        aln.AddSequence(seq.CreateSequence("seqres", gap_seqres))
        aln.AddSequence(seq.CreateSequence("atomseq", gap_atomseq))
        with self.assertRaises(RuntimeError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=aln,
                strict=True,
            )
        s = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", aln=aln, strict=False
        )  # strict equals False allows this

        # modify amino acid which should give an error in "strict mode"
        assembly = self.assembly.Copy()
        assembly.FindChain("A").residues[42].SetOneLetterCode("Y")
        with self.assertRaises(RuntimeError):
            s = Structure(self.seqres, "asdf", assembly, "cname=A", strict=True)
        # strict equals False allows this
        s = Structure(self.seqres, "asdf", assembly, "cname=A", strict=False)

        # provide several segment queries, number of alignments must either be 0
        # or equal in this case
        with self.assertRaises(ValueError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                "cname=A",
                aln=[self.aln, self.aln],
            )

        with self.assertRaises(ValueError):
            s = Structure(
                self.seqres,
                "asdf",
                self.assembly,
                ["cname=A", "cname=B"],
                aln=[self.aln, self.aln, self.aln],
            )

        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            ["cname=A", "cname=B", "cname=C"],
            aln=[self.aln, self.aln, self.aln],
        )

    def testGetNumSegments(self):
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            ["cname=A", "cname=B", "cname=C"],
            aln=[self.aln, self.aln, self.aln],
        )
        self.assertEqual(s.GetNumSegments(), 3)

    def testMapToReference(self):
        segment_queries = ["cname=A and rnum=30:33", "cname=A and rnum=42:100"]
        s = Structure(self.seqres, "asdf", self.assembly, segment_queries)
        values1 = [random.randint(0, 100) for r in s.segments[0].residues]
        values2 = [random.randint(0, 100) for r in s.segments[1].residues]
        expected1 = [None] * 29 + values1
        expected1 += [None] * (len(s.sequence) - len(expected1))
        expected2 = [None] * 41 + values2
        expected2 += [None] * (len(s.sequence) - len(expected2))
        self.assertEqual(s.MapToReference(values1, 0), expected1)
        self.assertEqual(s.MapToReference(values2, 1), expected2)

    def testInRange(self):
        segment_queries = ["cname=A and rnum=30:33", "cname=A and rnum=42:100"]
        s = Structure(self.seqres, "asdf", self.assembly, segment_queries)
        with self.assertRaises(RuntimeError):
            s.InRange((3, 6), segment_idx=2)
        self.assertTrue(s.InRange(None))
        self.assertFalse(s.InRange((1, 29)))
        self.assertFalse(s.InRange((34, 41)))
        self.assertFalse(s.InRange((101, 200)))
        self.assertTrue(s.InRange((1, 30)))
        self.assertTrue(s.InRange((34, 42)))
        self.assertTrue(s.InRange((1, 30), 0))
        self.assertTrue(s.InRange((34, 42), 1))
        self.assertFalse(s.InRange((1, 30), 1))
        self.assertFalse(s.InRange((34, 42), 0))

    def testGetResidue(self):
        segment_queries = ["cname=A and rnum=30:33", "cname=A and rnum=42:100"]
        s = Structure(self.seqres, "asdf", self.assembly, segment_queries)
        with self.assertRaises(RuntimeError):
            s.GetResidue(2, 42)
        with self.assertRaises(RuntimeError):
            s.GetResidue(0, 0)
        with self.assertRaises(RuntimeError):
            s.GetResidue(0, 10000)

        self.assertEqual(s.GetResidue(0, 29), None)
        r = self.assembly.FindResidue("A", mol.ResNum(30))
        self.assertEqual(
            s.GetResidue(0, 30).GetQualifiedName(), r.GetQualifiedName()
        )

    def testJSON(self):
        s = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", strict=False
        )
        s.AddMetaData("foo", "bar")
        serialized = s.ToJSON()
        loaded_s = Structure.FromJSON(serialized)
        self.assertTrue(s == loaded_s)

        # no matter what meta data we attached, the equality operator is not
        # affected by it... so let's check separately
        self.assertTrue(
            "foo" in loaded_s.meta and loaded_s.meta["foo"] == "bar"
        )

    def testHash(self):
        s1 = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", strict=False
        )
        seqres_wrong_name = seq.CreateSequence(
            self.seqres.GetName(), str(self.seqres)
        )
        seqres_wrong_sequence = seq.CreateSequence(
            self.seqres.GetName(), "A" + str(self.seqres)
        )
        s2 = Structure(
            seqres_wrong_name, "asdf", self.assembly, "cname=B", strict=False
        )
        s3 = Structure(
            seqres_wrong_sequence,
            "asdf",
            self.assembly,
            "cname=B",
            strict=False,
        )
        s4 = Structure(
            self.seqres, "asdfg", self.assembly, "cname=A", strict=False
        )
        s5 = Structure(
            self.seqres, "asdf", self.assembly, "cname=B", strict=False
        )
        s6 = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", strict=True
        )

        # they should all be different
        s1_hash = s1.GetHash()
        self.assertTrue(s1_hash != s2.GetHash())
        self.assertTrue(s1_hash != s3.GetHash())
        self.assertTrue(s1_hash != s4.GetHash())
        self.assertTrue(s1_hash != s5.GetHash())
        self.assertTrue(s1_hash != s6.GetHash())

        # but the assembly is the same
        self.assertTrue(s1.GetAssemblyHash() == s2.GetAssemblyHash())

        # generate same structure as s1 but decorate it with meta data
        # as an additional measure we're adding a sleep here reason for that
        # is that gzip (used to compress assembly) adds a timestamp which
        # has been turned off in Structure.EncodeAssembly so we can also check
        # for that
        time.sleep(1)
        s7 = Structure(
            self.seqres, "asdf", self.assembly, "cname=A", strict=False
        )
        s7.AddMetaData("foo", "bar")
        self.assertTrue("foo" in s7.meta and s7.meta["foo"] == "bar")
        # metadata should not affect hash
        self.assertTrue(s1_hash == s7.GetHash())

    def testGetResidue(self):
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            ["cname=A", "cname=B"],
            strict=False,
        )
        with self.assertRaises(RuntimeError):
            s.GetResidue(-1, 1)
        with self.assertRaises(RuntimeError):
            s.GetResidue(2, 1)
        with self.assertRaises(RuntimeError):
            s.GetResidue(1, 0)
        with self.assertRaises(RuntimeError):
            s.GetResidue(1, len(self.seqres) + 1)

        self.assertTrue(s.GetResidue(0, 1) is None)
        self.assertTrue(s.GetResidue(1, 1) is None)
        self.assertTrue(s.GetResidue(0, 28) is None)
        self.assertTrue(s.GetResidue(1, 28) is None)
        self.assertTrue(s.GetResidue(0, 29).one_letter_code == "Q")
        self.assertTrue(s.GetResidue(1, 29).one_letter_code == "Q")
        self.assertTrue(s.GetResidue(0, 335).one_letter_code == "N")
        self.assertTrue(s.GetResidue(1, 335).one_letter_code == "N")
        self.assertTrue(s.GetResidue(0, 336) is None)
        self.assertTrue(s.GetResidue(1, 336) is None)

    def testGetPos(self):
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            ["cname=A", "cname=B"],
            strict=False,
        )
        with self.assertRaises(RuntimeError):
            s.GetCAPos(-1, 1)
        with self.assertRaises(RuntimeError):
            s.GetCAPos(2, 1)
        with self.assertRaises(RuntimeError):
            s.GetCAPos(1, 0)
        with self.assertRaises(RuntimeError):
            s.GetCAPos(1, len(self.seqres) + 1)

        self.assertTrue(s.GetCAPos(0, 1) is None)
        self.assertTrue(s.GetCAPos(1, 1) is None)
        self.assertTrue(s.GetCAPos(0, 28) is None)
        self.assertTrue(s.GetCAPos(1, 28) is None)

        ref = geom.Vec3(3.564, 21.969, 26.087)
        d = geom.Distance(s.GetCAPos(0, 29), ref)
        self.assertAlmostEqual(d, 0.0, delta=0.001)

        ref = geom.Vec3(38.472, 8.464, 45.072)
        d = geom.Distance(s.GetCAPos(1, 29), ref)
        self.assertAlmostEqual(d, 0.0, delta=0.001)

        ref = geom.Vec3(1.241, 11.858, 17.312)
        d = geom.Distance(s.GetCAPos(0, 335), ref)
        self.assertAlmostEqual(d, 0.0, delta=0.001)

        ref = geom.Vec3(38.005, 16.825, 35.012)
        d = geom.Distance(s.GetCAPos(1, 335), ref)
        self.assertAlmostEqual(d, 0.0, delta=0.001)

        self.assertTrue(s.GetCAPos(0, 336) is None)
        self.assertTrue(s.GetCAPos(1, 336) is None)

    def testGetRangeCovered(self):
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            ["cname=A", "cname=B"],
            strict=False,
        )
        with self.assertRaises(RuntimeError):
            s.GetRangeCovered(-1)
        with self.assertRaises(RuntimeError):
            s.GetRangeCovered(2)
        self.assertEqual(s.GetRangeCovered(0), (29, 335))
        self.assertEqual(s.GetRangeCovered(1), (29, 335))


if __name__ == "__main__":
    unittest.main()
