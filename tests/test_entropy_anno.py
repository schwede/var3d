import unittest
import os
import tempfile
import stat
import math

from ost import seq

from var3d.base import *
from var3d.seq_anno import EntropySeqAnno
from var3d.msa_generator import *


class DummyMSAGenerator(MSAGenerator):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        if len(sequence) < 4:
            raise RuntimeError("Dummies are longer")
        tmp = [olc for olc in sequence]
        if tmp[3] == "A":
            tmp[3] = "C"
        else:
            tmp[3] = "A"
        aln = seq.CreateAlignment(
            sequence, seq.CreateSequence("A", "".join(tmp))
        )
        return aln


class TestEntropyAnno(unittest.TestCase):
    def testAwesomeThings(self):
        msa_generator = DummyMSAGenerator()
        annotator = EntropySeqAnno(msa_generator)
        self.assertEqual(annotator.Key(), "entropy")
        annotator = EntropySeqAnno(msa_generator, key="hello")
        self.assertEqual(annotator.Key(), "hello")
        dummy_seq = seq.CreateSequence("A", "HELLYEAH")
        entropies = annotator.Annotate(dummy_seq)
        expected_entropies = [0.0] * len(dummy_seq)
        # let's calculate the value at index 3 manually
        # we have two different amino acids => both have probability 0.5
        expected_entropies[3] = -(2 * 0.5 * math.log(0.5))
        for a, b in zip(entropies, expected_entropies):
            self.assertAlmostEqual(a, b, delta=0.0001)
        # check normalization
        annotator = EntropySeqAnno(msa_generator, normalize=True)
        entropies = annotator.Annotate(dummy_seq)
        expected_entropies = [0.0] * len(dummy_seq)
        expected_entropies[3] = 1.0
        for a, b in zip(entropies, expected_entropies):
            self.assertAlmostEqual(a, b, delta=0.0001)


if __name__ == "__main__":
    unittest.main()
