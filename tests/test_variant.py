import unittest

from ost import seq

from var3d.base import *


class TestVariant(unittest.TestCase):
    def testConstructorTypeChecks(self):
        # typecheck ref param
        with self.assertRaises(TypeError):
            var = Variant(42, "AB", 42, VariantType.INSERTION)
        var = Variant("A", "AB", 42, VariantType.INSERTION)

        # typecheck alt param
        with self.assertRaises(TypeError):
            var = Variant("A", 42, 42, VariantType.INSERTION)
        var = Variant("A", "AB", 42, VariantType.INSERTION)

        # typecheck pos param
        with self.assertRaises(TypeError):
            var = Variant("A", "AB", "A", VariantType.INSERTION)
        var = Variant("A", "AB", 42, VariantType.INSERTION)

        # typecheck variant_type param
        with self.assertRaises(TypeError):
            var = Variant("A", "AB", 42, "INSERTION")
        var = Variant("A", "AB", 42, VariantType.INSERTION)

    def testConstructorRefAltLengthChecks(self):
        # check INSERTION
        # expects ref to have length 1 and alt to have length > 1
        # first character in alt must match ref
        with self.assertRaises(ValueError):
            var = Variant("", "AB", 42, VariantType.INSERTION)
        with self.assertRaises(ValueError):
            var = Variant("A", "A", 42, VariantType.INSERTION)
        with self.assertRaises(ValueError):
            var = Variant("A", "BB", 42, VariantType.INSERTION)
        var = Variant("A", "AB", 42, VariantType.INSERTION)

        # check DELETION
        # expects ref to be non-empty and alt to be empty
        with self.assertRaises(ValueError):
            var = Variant("A", "A", 42, VariantType.DELETION)
        with self.assertRaises(ValueError):
            var = Variant("", "", 42, VariantType.DELETION)
        var = Variant("A", "", 42, VariantType.DELETION)

        # check INDEL
        # expects ref AND alt to be non-empty
        with self.assertRaises(ValueError):
            var = Variant("", "", 42, VariantType.INDEL)
        with self.assertRaises(ValueError):
            var = Variant("A", "", 42, VariantType.INDEL)
        with self.assertRaises(ValueError):
            var = Variant("", "A", 42, VariantType.INDEL)
        var = Variant("A", "A", 42, VariantType.INDEL)

        # Everything else (SUBSTITUTION, STOP_GAINED, STOP_LOST, START_LOST,
        # FRAMESHIFT) expects character of length 1 for both
        for vt in [
            VariantType.SUBSTITUTION,
            VariantType.STOP_GAINED,
            VariantType.STOP_LOST,
            VariantType.START_LOST,
            VariantType.FRAMESHIFT,
        ]:
            with self.assertRaises(ValueError):
                var = Variant("", "A", 42, vt)
            with self.assertRaises(ValueError):
                var = Variant("AA", "A", 42, vt)
            with self.assertRaises(ValueError):
                var = Variant("A", "", 42, vt)
            with self.assertRaises(ValueError):
                var = Variant("A", "AA", 42, vt)
            var = Variant("A", "A", 42, vt)

    def testConstructorValidPos(self):
        # Constructor only checks for resnums < 1, 0 is only allowed for
        # INSERTION
        with self.assertRaises(ValueError):
            var = Variant("A", "", -1, VariantType.DELETION)
        with self.assertRaises(ValueError):
            var = Variant("A", "", 0, VariantType.DELETION)
        var = Variant("A", "", 1, VariantType.DELETION)

        with self.assertRaises(ValueError):
            var = Variant("A", "AB", -1, VariantType.INSERTION)
        with self.assertRaises(ValueError):
            var = Variant("A", "AB", 0, VariantType.INSERTION)
        var = Variant("A", "AB", 1, VariantType.INSERTION)

    def testVariantSerialization(self):
        var = Variant("A", "AB", 1, VariantType.INSERTION)
        var.AddMetaData("foo", "bar")
        json_data = var.ToJSON()
        loaded_var = Variant.FromJSON(json_data)
        self.assertEqual(var, loaded_var)

        # no matter what meta data we attached, the equality operator is not
        # affected by it... so let's check separately
        self.assertTrue(
            "foo" in loaded_var.meta and loaded_var.meta["foo"] == "bar"
        )

    def testVariantMatchesSEQRES(self):
        seqres = seq.CreateSequence("A", "ABCDEFGHIJK")

        with self.assertRaises(ValueError):
            var = Variant("K", "KA", len(seqres) + 1, VariantType.INSERTION)
            var.MatchesSEQRES(seqres)
        var = Variant("K", "KA", len(seqres), VariantType.INSERTION)
        var.MatchesSEQRES(seqres)

        with self.assertRaises(ValueError):
            var = Variant("K", "", len(seqres) + 1, VariantType.DELETION)
            var.MatchesSEQRES(seqres)
        with self.assertRaises(ValueError):
            var = Variant("A", "", len(seqres), VariantType.DELETION)
            var.MatchesSEQRES(seqres)
        var = Variant("K", "", len(seqres), VariantType.DELETION)
        var.MatchesSEQRES(seqres)

        with self.assertRaises(ValueError):
            var = Variant("BB", "AAAA", 2, VariantType.INDEL)
            var.MatchesSEQRES(seqres)
        var = Variant("BC", "AAAA", 2, VariantType.INDEL)
        var.MatchesSEQRES(seqres)

    def testHash(self):
        v1 = Variant("A", "AB", 1, VariantType.INSERTION)
        v1_hash = v1.GetHash()

        # the following variants all contain minimal changes with respect to v1
        v2 = Variant("B", "BB", 1, VariantType.INSERTION)
        v3 = Variant("A", "AB", 2, VariantType.INSERTION)
        v4 = Variant("A", "AB", 1, VariantType.INDEL)
        self.assertTrue(v1_hash != v2.GetHash())
        self.assertTrue(v1_hash != v3.GetHash())
        self.assertTrue(v1_hash != v4.GetHash())

        # create a new variant same as v1 but decorate with some meta data
        v5 = Variant("A", "AB", 1, VariantType.INSERTION)
        v5.AddMetaData("foo", "bar")
        self.assertTrue("foo" in v5.meta and v5.meta["foo"] == "bar")
        # the hash should stay the same
        self.assertTrue(v1_hash, v5.GetHash())

    def testHR(self):
        v = Variant.FromHR("A123B")
        self.assertEqual(v.variant_type, "SUBSTITUTION")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "B")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123B")

        v = Variant.FromHR("A123BCD")
        self.assertEqual(v.variant_type, "INDEL")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "BCD")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123BCD")

        v = Variant.FromHR("AX123BCD")
        self.assertEqual(v.variant_type, "INDEL")
        self.assertEqual(v.ref, "AX")
        self.assertEqual(v.alt, "BCD")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "AX123BCD")

        v = Variant.FromHR("A123AB")
        self.assertEqual(v.variant_type, "INSERTION")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "AB")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123AB")

        v = Variant.FromHR("A123")
        self.assertEqual(v.variant_type, "DELETION")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123")

        v = Variant.FromHR("A123-")
        self.assertEqual(v.variant_type, "FRAMESHIFT")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "-")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123-")

        v = Variant.FromHR("A123*")
        self.assertEqual(v.variant_type, "STOP_GAINED")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "*")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123*")

        v = Variant.FromHR("A123?")
        self.assertEqual(v.variant_type, "START_LOST")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "?")
        self.assertEqual(v.pos, 123)
        self.assertEqual(v.ToHR(), "A123?")

        with self.assertRaises(RuntimeError):
            v = Variant.FromHR("123A")

        with self.assertRaises(RuntimeError):
            v = Variant.FromHR("A#A")

        with self.assertRaises(RuntimeError):
            v = Variant.FromHR("A1A1A")

        with self.assertRaises(RuntimeError):
            v = Variant.FromHR("A1#")

    def testHGVS(self):
        # Here we test pretty much every example given at the HGVS definition
        # found here: http://varnomen.hgvs.org

        v = Variant.FromHGVS("p.Trp24Cys", "AAAAAAAAAAAAAYYYYYYYYYAWA")
        self.assertEqual(v.ref, "W")
        self.assertEqual(v.alt, "C")
        self.assertEqual(v.pos, 24)
        self.assertEqual(v.variant_type, VariantType.SUBSTITUTION)

        v = Variant.FromHGVS("p.(Trp24Cys)", "AAAAAAAAAAAAAYYYYYYYYYAWA")
        self.assertEqual(v.ref, "W")
        self.assertEqual(v.alt, "C")
        self.assertEqual(v.pos, 24)
        self.assertEqual(v.variant_type, VariantType.SUBSTITUTION)

        v = Variant.FromHGVS("p.(Trp24Trp)", "AAAAAAAAAAAAAYYYYYYYYYAWA")
        self.assertEqual(v.ref, "W")
        self.assertEqual(v.alt, "W")
        self.assertEqual(v.pos, 24)
        self.assertEqual(v.variant_type, VariantType.SYNONYMOUS)

        v = Variant.FromHGVS("p.Trp24Ter", "AAAAAAAAAAAAAYYYYYYYYYAWA")
        self.assertEqual(v.ref, "W")
        self.assertEqual(v.alt, "*")
        self.assertEqual(v.pos, 24)
        self.assertEqual(v.variant_type, VariantType.STOP_GAINED)

        v = Variant.FromHGVS("p.Trp24*", "AAAAAAAAAAAAAYYYYYYYYYAWA")
        self.assertEqual(v.ref, "W")
        self.assertEqual(v.alt, "*")
        self.assertEqual(v.pos, 24)
        self.assertEqual(v.variant_type, VariantType.STOP_GAINED)

        v = Variant.FromHGVS("p.Ala3=", "AAAAAAAAAAAAAYYYYYYYYYAWA")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "A")
        self.assertEqual(v.pos, 3)
        self.assertEqual(v.variant_type, VariantType.SYNONYMOUS)

        v = Variant.FromHGVS("p.0", "MAAAAAAAA")
        self.assertEqual(v.ref, "M")
        self.assertEqual(v.alt, "?")
        self.assertEqual(v.pos, 1)
        self.assertEqual(v.variant_type, VariantType.START_LOST)

        v = Variant.FromHGVS("p.0?", "MAAAAAAAAA")
        self.assertEqual(v.ref, "M")
        self.assertEqual(v.alt, "?")
        self.assertEqual(v.pos, 1)
        self.assertEqual(v.variant_type, VariantType.START_LOST)

        v = Variant.FromHGVS("p.Val7del", "AAAAAAVA")
        self.assertEqual(v.ref, "V")
        self.assertEqual(v.alt, "")
        self.assertEqual(v.pos, 7)
        self.assertEqual(v.variant_type, VariantType.DELETION)

        v = Variant.FromHGVS("p.(Val7del)", "AAAAAAVA")
        self.assertEqual(v.ref, "V")
        self.assertEqual(v.alt, "")
        self.assertEqual(v.pos, 7)
        self.assertEqual(v.variant_type, VariantType.DELETION)

        v = Variant.FromHGVS("p.Trp4del", "AAAWAAVA")
        self.assertEqual(v.ref, "W")
        self.assertEqual(v.alt, "")
        self.assertEqual(v.pos, 4)
        self.assertEqual(v.variant_type, VariantType.DELETION)

        v = Variant.FromHGVS("p.Trp4_Val7del", "AAAWAAVA")
        self.assertEqual(v.ref, "WAAV")
        self.assertEqual(v.alt, "")
        self.assertEqual(v.pos, 4)
        self.assertEqual(v.variant_type, VariantType.DELETION)

        v = Variant.FromHGVS("p.Ala3dup", "AAAWAAVA")
        self.assertEqual(v.ref, "A")
        self.assertEqual(v.alt, "AA")
        self.assertEqual(v.pos, 3)
        self.assertEqual(v.variant_type, VariantType.INSERTION)

        v = Variant.FromHGVS("p.Ala3_Ser5dup", "AAAWSAVA")
        self.assertEqual(v.ref, "S")
        self.assertEqual(v.alt, "SAWS")
        self.assertEqual(v.pos, 5)
        self.assertEqual(v.variant_type, VariantType.INSERTION)

        v = Variant.FromHGVS("p.His4_Gln5insAla", "AAAHQAVA")
        self.assertEqual(v.ref, "H")
        self.assertEqual(v.alt, "HA")
        self.assertEqual(v.pos, 4)
        self.assertEqual(v.variant_type, VariantType.INSERTION)

        v = Variant.FromHGVS("p.Lys2_Gly3insGlnSerLys", "AKGHQAVA")
        self.assertEqual(v.ref, "K")
        self.assertEqual(v.alt, "KQSK")
        self.assertEqual(v.pos, 2)
        self.assertEqual(v.variant_type, VariantType.INSERTION)

        v = Variant.FromHGVS(
            "p.Cys28delinsTrpVal", "AAAAAAAAAAAAAAAAAAAAAAAKGHQCVA"
        )
        self.assertEqual(v.ref, "C")
        self.assertEqual(v.alt, "WV")
        self.assertEqual(v.pos, 28)
        self.assertEqual(v.variant_type, VariantType.INDEL)

        v = Variant.FromHGVS(
            "p.(Glu3_Ala5delinsGlyLeuHisArgPheIleValLeu)", "AAEKAHQCVA"
        )
        self.assertEqual(v.ref, "EKA")
        self.assertEqual(v.alt, "GLHRFIVL")
        self.assertEqual(v.pos, 3)
        self.assertEqual(v.variant_type, VariantType.INDEL)

        v = Variant.FromHGVS("p.Arg9fs", "AAEKAHQCRAAA")
        self.assertEqual(v.ref, "R")
        self.assertEqual(v.alt, "-")
        self.assertEqual(v.pos, 9)
        self.assertEqual(v.variant_type, VariantType.FRAMESHIFT)

        v = Variant.FromHGVS("p.Arg9ProfsTer23", "AAEKAHQCRAAA")
        self.assertEqual(v.ref, "R")
        self.assertEqual(v.alt, "-")
        self.assertEqual(v.pos, 9)
        self.assertEqual(v.variant_type, VariantType.FRAMESHIFT)

    def testInRange(self):
        v = Variant("AB", "ABC", 20, VariantType.INDEL)
        self.assertFalse(v.InRange((1, 19)))
        self.assertTrue(v.InRange((1, 20)))
        self.assertTrue(v.InRange((1, 21)))
        self.assertTrue(v.InRange((20, 20)))
        self.assertTrue(v.InRange((21, 21)))
        self.assertFalse(v.InRange((22, 22)))
        self.assertTrue(v.InRange((21, 100)))
        self.assertFalse(v.InRange((22, 100)))
        self.assertTrue(v.InRange((1, 100)))


if __name__ == "__main__":
    unittest.main()
