import unittest
import os

from ost import seq
from ost import io

from var3d.base import *


class VarImporterValid(VarImporter):
    def __init__(self):
        pass

    def Run(self, sequence, uniprot_ac=None, seq_range=None):
        return [Variant(sequence[0], "Q", 1, VariantType.SUBSTITUTION)]


class VarImporterNoRun(VarImporter):
    def __init__(self):
        pass


class VarImporterWrongReturnType(VarImporter):
    def __init__(self):
        pass

    def Run(self, sequence, uniprot_ac=None, seq_range=None):
        return 42


class VarImporterTriggeringValidation(VarImporter):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        return [Variant("Q", "E", 1, VariantType.SUBSTITUTION)]


class StructImporterNoRun(StructImporter):
    def __init__(self):
        pass


class StructImporterWrongReturnTypeOne(StructImporter):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        return 42


class StructImporterWrongReturnTypeTwo(StructImporter):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        return [42]


class StructImporterValid(StructImporter):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        # neglects sequence argument, just returns what we have in
        # the data dir
        unit_test_dir = os.path.dirname(os.path.realpath(__file__))
        data_dir = os.path.join(unit_test_dir, "data")
        assembly = io.LoadPDB(os.path.join(data_dir, "4nf2.1.pdb"))
        seqres = io.LoadSequence(os.path.join(data_dir, "4nf2.1.fasta"))
        return [Structure(seqres, "asdf", assembly, "cname=A")]


class SeqAnnoValid(SeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, seq_range=None):
        return [42] * len(sequence)


class SeqAnnoNoKey(SeqAnno):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        return [42] * len(sequence)


class SeqAnnoNoRun(SeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"


class SeqAnnoWrongReturnType(SeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, seq_range=None):
        return 42


class SeqAnnoWrongReturnLength(SeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, seq_range=None):
        return [42] * (len(sequence) + 1)


class VarSeqAnnoValid(VarSeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variant_list, seq_range=None):
        return [42] * len(variant_list)


class VarSeqAnnoNoKey(VarSeqAnno):
    def __init__(self):
        pass

    def Run(self, sequence, seq_range=None):
        return [42] * (len(sequence) + 1)


class VarSeqAnnoNoRun(VarSeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"


class VarSeqAnnoWrongReturnType(VarSeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variant_list, seq_range=None):
        return 42


class VarSeqAnnoWrongReturnLength(VarSeqAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variant_list, seq_range=None):
        return [42] * (len(variant_list) + 1)


class StructAnnoPerResidueValid(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return True

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        return [[[42] * len(sequence)] * s.GetNumSegments() for s in structures]


class StructAnnoFullStructureValid(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return False

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        anno = [42] * len(structures)
        return anno


class StructAnnoFullStructureWrongReturnType(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return False

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        return {}


class StructAnnoFullStructureWrongReturnLength(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return False

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        anno = [42] * (len(structures) + 1)
        return anno


class StructAnnoPerResidueWrongReturnType(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return False

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        return {}


class StructAnnoPerResidueWrongReturnLength(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return False

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        anno = [[[42] * len(sequence)] * s.GetNumSegments() for s in structures]
        anno = anno + anno
        return anno


class StructAnnoPerResidueWrongSegmentNumber(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return True

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        n_segments = 42
        return [[[42] * len(sequence)] * n_segments for s in structures]


class StructAnnoPerResidueWrongSequenceLength(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return True

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        seq_length = 42
        return [[[42] * seq_length] * s.GetNumSegments() for s in structures]


class StructAnnoNoPerResidue(StructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, structures, seq_range=None):
        anno = [42] * len(structures)
        return anno


class StructAnnoNoKey(StructAnno):
    def __init__(self):
        pass

    def PerResidue(self):
        return False

    def Run(self, sequence, structures, seq_range=None):
        anno = [42] * len(structures)
        return anno


class StructAnnoNoRun(StructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def PerResidue(self):
        return False


class VarStructAnnoValid(VarStructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variants, structures):
        return [[42] * len(structures)] * len(variants)


class VarStructAnnoNoKey(VarStructAnno):
    def __init__(self):
        pass

    def Run(self, sequence, variants, structures):
        [[42] * len(structures)] * len(variants)


class VarStructAnnoNoRun(VarStructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"


class VarStructAnnoWrongReturnType(VarStructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variants, structures):
        return {}


class VarStructAnnoWrongVariantReturnLength(VarStructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variants, structures):
        return [[42] * len(structures)] * (2 * len(variants))


class VarStructAnnoWrongStructureReturnLength(VarStructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variants, structures):
        return [[42] * (2 * len(structures))] * len(variants)


class VarStructAnnoWrongPerVariantReturnType(VarStructAnno):
    def __init__(self):
        pass

    def Key(self):
        return "Mythen"

    def Run(self, sequence, variants, structures):
        return [42] * len(variants)


class TestInterfaces(unittest.TestCase):
    def setUp(self):
        unit_test_dir = os.path.dirname(os.path.realpath(__file__))
        data_dir = os.path.join(unit_test_dir, "data")
        self.assembly = io.LoadPDB(os.path.join(data_dir, "4nf2.1.pdb"))
        self.seqres = io.LoadSequence(os.path.join(data_dir, "4nf2.1.fasta"))
        self.aln = seq.alg.AlignToSEQRES(
            self.assembly.FindChain("A"), self.seqres
        )

    def testVariantImport(self):
        # check for Error when Run method is not implemented in child class
        importer = VarImporterNoRun()
        with self.assertRaises(NotImplementedError):
            importer.Import(seq.CreateSequence("A", "ABCD"))

        # check for error if child class returns no list as result
        anno = VarImporterWrongReturnType()
        with self.assertRaises(RuntimeError):
            anno.Import(seq.CreateSequence("A", "ABCD"))

        # If an importer returns variants that are inconsistent with given
        # SEQRES, an error should be thrown if validate is True
        # (default behaviour)
        importer = VarImporterTriggeringValidation()
        with self.assertRaises(ValueError):
            importer.Import((seq.CreateSequence("A", "ABCD")))
        corrupt_variants = importer.Import(
            (seq.CreateSequence("A", "ABCD")), validate=False
        )
        self.assertTrue(isinstance(corrupt_variants, list))
        self.assertEqual(len(corrupt_variants), 1)

        importer = VarImporterValid()
        variants = importer.Import(seq.CreateSequence("A", "ABCD"))
        self.assertTrue(isinstance(variants, list))
        self.assertEqual(len(variants), 1)

    def testStructImporter(self):
        importer = StructImporterNoRun()
        with self.assertRaises(RuntimeError):
            importer.Import(seq.CreateSequence("A", "ABCD"))

        importer = StructImporterWrongReturnTypeOne()
        with self.assertRaises(RuntimeError):
            importer.Import(seq.CreateSequence("A", "ABCD"))
        importer = StructImporterWrongReturnTypeTwo()
        with self.assertRaises(RuntimeError):
            importer.Import(seq.CreateSequence("A", "ABCD"))

        # StructImporterValid returns Structure with fixed sequence
        importer = StructImporterValid()
        with self.assertRaises(RuntimeError):
            importer.Import(seq.CreateSequence("A", "ABCD"))

        importer = StructImporterValid()
        structures = importer.Import(self.seqres)
        self.assertEqual(len(structures), 1)

    def testSeqAnnoNoKey(self):
        # check for Error when Key method not implemented in child class
        anno = SeqAnnoNoKey()
        with self.assertRaises(NotImplementedError):
            anno.Key()

        # check for Error when Run method not implemented in child class
        anno = SeqAnnoNoRun()
        s = seq.CreateSequence("A", "HELLYEAH")
        with self.assertRaises(NotImplementedError):
            anno.Annotate(s)

        # check for error if child class returns no list as result
        anno = SeqAnnoWrongReturnType()
        s = seq.CreateSequence("A", "HELLYEAH")
        with self.assertRaises(RuntimeError):
            anno.Annotate(s)

        # check for error if child class returns no list as result
        anno = SeqAnnoWrongReturnLength()
        s = seq.CreateSequence("A", "HELLYEAH")
        with self.assertRaises(RuntimeError):
            anno.Annotate(s)

        # check functionality of a valid SeqAnno implementation
        anno = SeqAnnoValid()

        # Must raise if no SequenceHandle is passed as arg
        with self.assertRaises(RuntimeError):
            anno.Annotate("HELLYEAH")

        # Let's try the working thing
        s = seq.CreateSequence("A", "HELLYEAH")
        annotation = anno.Annotate(s)

        self.assertTrue(isinstance(annotation, list))
        self.assertEqual(len(annotation), len(s))
        self.assertEqual(anno.Key(), "Mythen")

    def testVarSeqAnnoNoKey(self):
        # check for Error when Key method not implemented in child class
        anno = VarSeqAnnoNoKey()
        with self.assertRaises(NotImplementedError):
            anno.Key()

        # check for Error when Run method not implemented in child class
        anno = VarSeqAnnoNoRun()
        sequence = seq.CreateSequence("A", "ABCD")
        variant_importer = VarImporterValid()
        variants = variant_importer.Import(sequence)
        with self.assertRaises(NotImplementedError):
            anno.Annotate(sequence, variants)

        # Run method of child class must return a list
        anno = VarSeqAnnoWrongReturnType()
        sequence = seq.CreateSequence("A", "ABCD")
        variant_importer = VarImporterValid()
        variants = variant_importer.Import(sequence)
        with self.assertRaises(RuntimeError):
            anno.Annotate(sequence, variants)

        # Run method of child class must return a list of same length
        # than the input variants
        anno = VarSeqAnnoWrongReturnLength()
        sequence = seq.CreateSequence("A", "ABCD")
        variant_importer = VarImporterValid()
        variants = variant_importer.Import(sequence)
        with self.assertRaises(RuntimeError):
            anno.Annotate(sequence, variants)

        # check functionality of a valid VarSeqAnno implementation
        anno = VarSeqAnnoValid()
        sequence = seq.CreateSequence("A", "ABCD")
        variant_importer = VarImporterValid()
        variants = variant_importer.Import(sequence)

        # Must raise if no SequenceHandle is passed as first arg
        with self.assertRaises(RuntimeError):
            anno.Annotate("ABCD", variants)

        # Must raise if no list is passed as second arg
        with self.assertRaises(RuntimeError):
            anno.Annotate(sequence, variants[0])

        # Must raise if any of the items in second arg is not of type
        # Variant
        with self.assertRaises(RuntimeError):
            anno.Annotate(sequence, [variants[0], 42])

        # Let's try the working thing
        annotations = anno.Annotate(sequence, variants)

        self.assertTrue(isinstance(annotations, list))
        self.assertEqual(len(annotations), len(variants))
        self.assertEqual(anno.Key(), "Mythen")

    def testStructAnnoNoPerResidue(self):
        # check for Error when PerResidue method not implemented in child class
        anno = StructAnnoNoPerResidue()
        with self.assertRaises(NotImplementedError):
            anno.PerResidue()

        # check for Error when Key method not implemented in child class
        anno = StructAnnoNoKey()
        with self.assertRaises(NotImplementedError):
            anno.Key()

        # check for Error when Run method not implemented in child class
        anno = StructAnnoNoRun()
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=self.aln,
            strict=False,
        )
        with self.assertRaises(NotImplementedError):
            anno.Annotate(self.seqres, [s])

        # feed in wrong types
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=self.aln,
            strict=False,
        )
        anno = StructAnnoFullStructureValid()
        self.assertTrue(anno.PerResidue() is False)
        with self.assertRaises(RuntimeError):
            anno.Annotate(42, [s, s])
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [42])
        # feed in inconsistent sequence
        with self.assertRaises(RuntimeError):
            anno.Annotate(seq.CreateSequence("A", "AAA"), [s, s])

        # that should finally work
        annotations = anno.Annotate(self.seqres, [s, s])

        # some more annos that have issues that must be checked in base class
        anno = StructAnnoFullStructureWrongReturnType()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [s, s])

        anno = StructAnnoFullStructureWrongReturnLength()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [s, s])

        # feed in wrong types
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=self.aln,
            strict=False,
        )
        anno = StructAnnoPerResidueValid()
        self.assertTrue(anno.PerResidue())
        with self.assertRaises(RuntimeError):
            anno.Annotate(42, [s, s])
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [42])
        # feed in inconsistent sequence
        with self.assertRaises(RuntimeError):
            anno.Annotate(seq.CreateSequence("A", "AAA"), [s, s])

        # that should finally work
        annotations = anno.Annotate(self.seqres, [s, s])

        # some more annos that have issues that must be checked in base class
        anno = StructAnnoPerResidueWrongReturnType()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [s, s])

        anno = StructAnnoPerResidueWrongReturnLength()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [s, s])

        # all of the above is actually copy paste from the full structure
        # counterpart... here starts the per residue specific checking
        anno = StructAnnoPerResidueWrongSegmentNumber()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [s, s])

        anno = StructAnnoPerResidueWrongSequenceLength()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [s, s])

        # check whether setting a sequence range has the desired effect
        # (setting everything outside to None)
        anno = StructAnnoPerResidueValid()
        annotations = anno.Annotate(self.seqres, [s, s], seq_range=[2, 3])
        seq_length = len(self.seqres)
        n_segments = s.GetNumSegments()
        expected_annotations = [[[None] * seq_length] * n_segments] * 2
        for structure_idx in range(2):
            for segment_idx in range(n_segments):
                expected_annotations[structure_idx][segment_idx][1] = 42
                expected_annotations[structure_idx][segment_idx][2] = 42
        self.assertEqual(annotations, expected_annotations)

    def testVarStructAnnoNoKey(self):
        # check for Error when Key method not implemented in child class
        anno = VarStructAnnoNoKey()
        with self.assertRaises(NotImplementedError):
            anno.Key()

        # check for Error when Run method not implemented in child class
        anno = VarStructAnnoNoRun()
        v = Variant(self.seqres[3], "F", 4, VariantType.SUBSTITUTION)
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=self.aln,
            strict=False,
        )
        with self.assertRaises(NotImplementedError):
            anno.Annotate(self.seqres, [v], [s])

        anno = VarStructAnnoValid()
        self.assertEqual(anno.Key(), "Mythen")
        v = Variant(self.seqres[3], "F", 4, VariantType.SUBSTITUTION)
        s = Structure(
            self.seqres,
            "asdf",
            self.assembly,
            "cname=A",
            aln=self.aln,
            strict=False,
        )
        annotations = anno.Annotate(self.seqres, [v], [s])

        # feed it with wrong types
        with self.assertRaises(RuntimeError):
            anno.Annotate(42, [v], [s])
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, v, [s])
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [v], s)
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [42], [s])
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [v], [42])

        # variants must match SEQRES
        fake_v = Variant("Y", "F", 4, VariantType.SUBSTITUTION)
        with self.assertRaises(ValueError):
            anno.Annotate(self.seqres, [fake_v], [s])

        # SEQRES must match the reference sequence in s
        with self.assertRaises(RuntimeError):
            anno.Annotate(seq.CreateSequence("A", self.seqres[:10]), [v], [s])

        anno = VarStructAnnoWrongReturnType()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [v], [s])

        anno = VarStructAnnoWrongVariantReturnLength()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [v], [s])

        anno = VarStructAnnoWrongStructureReturnLength()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [v], [s])

        anno = VarStructAnnoWrongPerVariantReturnType()
        with self.assertRaises(RuntimeError):
            anno.Annotate(self.seqres, [v], [s])


if __name__ == "__main__":
    unittest.main()
