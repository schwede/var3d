import os
import re

from var3d.base import *
from var3d import uniprot


class HRVarImporter(VarImporter):
    """The Human Readable (HR) var importer

    Parses and imports variants based on human readable strings
    (see :func:`var3d.base.Variant.FromHR`) 
    """

    def __init__(self):
        self.data = dict()

    def Add(self, sequence, var_string, meta=dict()):
        """Adds variant defined by *var_string* for later retrieval

        The variant gets associated to the name of *sequence*. When a
        sequence is processed at import later on, all variants associated to
        that input sequence name are imported.

        :param sequence: Sequence associated to variant defined in *var_string*.
                         Sequence/variant mapping happens with sequence name.
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param var_string: Defines a variant, passed to 
                           :func:`var3d.base.Variant.FromHR`
        :type var_string: :class:`str`
        :param meta: Added as meta data to variant
        :type meta: :class:`dict`
        :raises: Whatever :func:`var3d.base.Variant.FromHR` raises
        """
        v = Variant.FromHR(var_string)
        for meta_key, meta_value in meta.items():
            v.AddMetaData(meta_key, meta_value)
        sname = sequence.GetName()
        if sname not in self.data:
            self.data[sname] = list()
        self.data[sname].append(v)

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface

        Imports variants as described above
        """
        sname = sequence.GetName()
        if sname not in self.data:
            return list()
        return [v for v in self.data[sname] if v.InRange(seq_range)]


class UniprotEntryVarImporter(VarImporter):
    """Fetches and parses variants from uniprot entry. 
    
    Expects name of the processed sequence to be a valid uniprot accession code. 
    Data is either loaded remotely, or from a local uniprot entry file in txt 
    format. Parses VARIANT feature tables (FT).

    :param url: Used for remote fetching. Given AC (uniprot accession code) 
                from input sequence, the constructed url is:
                <url>/<AC>.<suffix>. Given default values and the AC
                P51480 that would be https://www.uniprot.org/uniprot/P51480.txt
    :type url: :class:`str`
    :param local_directory: If given, uniprot entry will be fetched from local
                            disk not considering *url*. Given default values and
                            the AC P51480 the local path would be 
                            <local_directory>/P51480.<suffix>
    :type local_directory: :class:`str`
    :param suffix: Used to construct url or path in local_directory
    :type suffix: :class:`str`
    """

    def __init__(
        self,
        url="https://www.uniprot.org/uniprot",
        local_directory=None,
        suffix="txt",
    ):
        self.url = url
        self.local_directory = local_directory
        self.suffix = suffix

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface

        Imports variants as described above
        """
        entry_data = self._FetchEntry(sequence)
        variants = self._ParseEntry(
            entry_data, exp_seq=sequence, seq_range=seq_range
        )
        return variants

    def _FetchEntry(self, sequence):
        """Fetches uniprot entry in txt format given sequence. 
        
        If *local_directory* has been specified at construction, that's where 
        we look for the corresponding file. The entry is fetched over the web
        otherwise.

        :param sequence: Sequence for which to fetch the entry. In case you load
                         from local disk, the sequence name is used to infer the 
                         filename. If you fetch the entry online, the sequence
                         name is expected to be a valid uniprot AC and uses
                         :func:`uniprot.FetchUniprotEntry`
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :returns: :class:`list` of :class:`str` with the entry content
        :raises: In case of remote loading: :class:`ValueError` if sequence name
                 is no valid uniprot AC, url is not recognized as actual url or
                 :class:`urllib.error.HTTPError` if http request fails.
                 In case of loading from disk: :class:`RuntimeError` if file
                 doesn't exist.  
        """
        if self.local_directory:
            entry_path = os.path.join(
                self.local_directory, f"{sequence.GetName()}.{self.suffix}"
            )
            if not os.path.exists(entry_path):
                raise RuntimeError(f"Cannot find uniprot entry at {entry_path}")
            with open(entry_path, "r") as fh:
                return fh.readlines()
        else:
            return uniprot.FetchUniprotEntry(
                sequence.GetName(), self.url, self.suffix
            )

    def _ParseEntry(self, entry_data, exp_seq=None, seq_range=None):
        """Parse uniprot entry content as fetched by FetchEntry method

        :param entry_data: Return value from FetchEntry method
        :type entry_data: :class:`list` of :class:`str`
        :class exp_sequence: Expected sequence, function checks for equality
                             if given
        :type exp_sequence: :class:`ost.seq.SequenceHandle` /
                            :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: Only returns variants that affect this range
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: parsed variants (:class:`list` of :class:`Variant`)
        """
        canonical_seq = uniprot.ParseUniprotSequence(entry_data)
        var_ft = uniprot.ParseUniprotFT(entry_data, "VARIANT")
        if canonical_seq is None:
            raise RuntimeError(
                "Uniprot entry did not contain canonical " "sequence"
            )
        if exp_seq:
            if str(exp_seq) != str(canonical_seq):
                raise RuntimeError(
                    f"Sequence parsed from uniprot entry "
                    f"({str(canonical_seq)}) is not equal the "
                    f"expected sequence ({str(exp_seq)})"
                )

        return_list = list()
        for table in var_ft:
            parsed_variant = self._ConstructVariant(
                table, canonical_seq, seq_range
            )
            if parsed_variant is not None:
                return_list.append(parsed_variant)
        return return_list

    def _ConstructVariant(self, table, canonical_sequence, seq_range):
        # PARSE LOCATION
        ################

        if table.ft_start is None or table.ft_end is None:
            return None

        if table.ft_start_beyond is None or table.ft_end_beyond is None:
            return None

        if seq_range is not None and (
            table.ft_end < seq_range[0] or table.ft_start > seq_range[1]
        ):
            return None

        # get pubmed ids
        search_results = re.findall(r"(PubMed):(\d+)", table.evidence_qualifier)
        pmid = [item[1] for item in search_results]

        # PARSE NOTE QUALIFIER (variant_type, ref, alt, and labels)
        ###########################################################
        note = table.note_qualifier
        missing_pattern = r"[Mm][Ii][Ss][Ss][Ii][Nn][Gg]"  # is this stupid?
        subst_pattern = r"([A-Z]+) -> ([A-Z]+)"
        missing_match = re.search(missing_pattern, note)
        subst_match = re.search(subst_pattern, note)

        if missing_match is not None and missing_match.start(0) == 0:
            variant_type = VariantType.DELETION
            ref = canonical_sequence[table.ft_start - 1 : table.ft_end]
            alt = ""
        elif subst_match is not None and subst_match.start(0) == 0:
            ref = subst_match.group(1)
            alt = subst_match.group(2)
            if len(ref) == 1 and len(alt) == 1:
                variant_type = VariantType.SUBSTITUTION
            else:
                variant_type = VariantType.INDEL
        else:
            raise RuntimeError(
                f"Expect note qualifier that starts with "
                f'"Missing" or X -> Y, got {note}. It\'s very '
                f"well possible though that we don't yet "
                f"support everything present in uniprot..."
            )

        # We assume labels to be everything in note qualifier that is enclosed
        # in brackets and separated by ";"
        labels_pattern = r"\((.+)\)"
        labels_match = re.search(labels_pattern, note)
        if labels_match is not None:
            labels = labels_match.group(1).split(";")
            labels = [item.strip() for item in labels]
        else:
            labels = list()

        variant = Variant(ref, alt, table.ft_start, variant_type)
        variant.AddMetaData("pmid", pmid)
        variant.AddMetaData("labels", labels)
        return variant
