import os
import tempfile
import hashlib
import subprocess
import collections
from ost import settings
from ost import io
from ost import seq


# SOME HELPERS
##############
def _SetupTMPDir(tmp_dir):
    if tmp_dir:
        if not os.path.isdir(tmp_dir):
            raise ValueError(
                f"Provided tmp_dir ({tmp_dir}) is not a " "directory"
            )
        return (tmp_dir, None)
    else:
        # as soon as self.tmp_dir_context is garabage collected,
        # the directory is deleted on disk
        tmp_dir_context = tempfile.TemporaryDirectory()
        tmp_dir = tmp_dir_context.name
        return (tmp_dir, tmp_dir_context)


def _CheckDB(db_path, prefix_db=False):
    if prefix_db:
        dirname = os.path.dirname(db_path)
        prefix = os.path.basename(db_path)
        if not os.path.exists(dirname):
            raise ValueError(
                f"Directory containing specified database does "
                f"not exist ({dirname})"
            )
        files = os.listdir(dirname)
        n = sum([f.startswith(prefix) for f in files])
        if n == 0:
            raise ValueError(
                f"No files starting with expected prefix "
                f"({prefix}) in databse directory ({dirname})"
            )
        return db_path
    else:
        if not os.path.exists(db_path):
            raise ValueError(f"Specified database does not exist ({db_path})")
        return db_path


def _PrepareSequence(tmp_dir, sequence):
    seq_hash = hashlib.md5(str(sequence).encode()).hexdigest()
    query_seq_path = os.path.join(tmp_dir, seq_hash + ".fasta")
    io.SaveSequence(sequence, query_seq_path)
    return (query_seq_path, seq_hash)


class MSAGenerator:
    """General interface to generate MSAs for a given sequence. 
    """

    def __init__(self):
        pass

    def GetMSA(self, sequence):
        """Generate the alignment. 
        
        The first sequence in the return alignment 
        is guaranteed to correspond to the input sequence. However, there is NO
        guarantee that name/offset stay the same.

        :param sequence: Sequence for which to generate MSA  
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :returns: :class:`ost.seq.AlignmentHandle` with first sequence
                  corresponding to *sequence*
        """
        msa = self.Run(sequence)
        if msa.GetSequence(0).GetGaplessString() != str(sequence):
            raise RuntimeError(
                f"First sequence of returned aln must match "
                f"input sequence. Input sequence: {sequence} "
                f"First sequence in aln: "
                f"{str(msa.GetSequence(0))}"
            )
        return msa

    def Run(self, sequence):
        """Must be implemented by child class

        :param sequence: Sequence for which to generate an MSA
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :returns: :class:`ost.seq.AlignmentHandle` with first sequence
                  corresponding to *sequence*
        """
        raise NotImplementedError("Incomplete child class")


class MMseqs2MSAGenerator(MSAGenerator):
    """Generates MSA based on MMseqs2

    Steinegger M, Soeding J MMseqs2 enables sensitive protein sequence searching
    for the analysis of massive data sets. Nature Biotechnology, 35(11), 
    1026-1028 (2017)

    :param mmseqs2_db: MMseqs2 database to retrieve homologue sequences. See 
                       "<mmseqs2_exe> createdb -h" for more info or check
                       "<mmseqs2_exe> databases -h" to fetch some standard db. 
                       Once generated, search speed is significantly increased 
                       when precalculating index files, see 
                       "<mmseqs2_exe> createindex -h"
    :type mmseqs2_db: :class:`str`
    :param mmseqs2_exe: Path to mmseqs2 executable. The constructor searches for 
                        "mmseqs2"/"mmseqs" executables in search path if not 
                        given
    :type mmseqs2_exe: :class:`str`
    :param tmp_dir: Used by MMseqs2 and to dump own temporary files. Created 
                    (and cleaned up at garbage collection) if not given.
    :type tmp_dir: :class:`str`
    :param sensitivity: Sensitivity param (:code:`-s` param) used in MMseqs2
                        search [1 (fast) to 7.5 (sensitive)]. Uses MMseqs2
                        default if not given.
    :type sensitivity: :class:`float`
    """

    def __init__(
        self, mmseqs2_db, mmseqs2_exe=None, tmp_dir=None, sensitivity=None
    ):

        self.mmseqs2_db = _CheckDB(mmseqs2_db, prefix_db=True)
        self.mmseqs2_exe = settings.Locate(
            ["mmseqs2", "mmseqs"], explicit_file_name=mmseqs2_exe
        )

        if sensitivity:
            if sensitivity < 1.0 or sensitivity > 7.5:
                raise ValueError(f"sensitivity must be in range [1.0, 7.5]")
        self.sensitivity = sensitivity

        self.tmp_dir, self.tmp_dir_context = _SetupTMPDir(tmp_dir)

    def Run(self, sequence):
        """Implementation of parent interface.
        """
        # PREPARE
        #########
        query_seq_path, seq_hash = _PrepareSequence(self.tmp_dir, sequence)
        query_db = os.path.join(self.tmp_dir, seq_hash + "_db")
        build_query_db_cmd = [
            self.mmseqs2_exe,
            "createdb",
            query_seq_path,
            query_db,
        ]
        subprocess.run(
            build_query_db_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        # SEARCH
        ########
        result_db = os.path.join(self.tmp_dir, seq_hash + "_resdb")
        search_cmd = [self.mmseqs2_exe, "search", "-a", "1"]
        if self.sensitivity:
            search_cmd += ["-s", str(self.sensitivity)]
        search_cmd += [query_db, self.mmseqs2_db, result_db, self.tmp_dir]
        p = subprocess.run(
            search_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        aln_out = result_db + ".m8"
        aln_cmd = [
            self.mmseqs2_exe,
            "convertalis",
            query_db,
            self.mmseqs2_db,
            result_db,
            aln_out,
            "--format-output",
            "taln,qaln,qstart,qend",
        ]
        subprocess.run(aln_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if not os.path.exists(aln_out):
            raise RuntimeError("MMseqs2 execution failed")

        # PARSE AND RETURN
        ##################
        with open(aln_out) as fh:
            aln_data = fh.readlines()
        aln_list = seq.AlignmentList()
        for line in aln_data:
            taln, qaln, qstart, qend = line.split()
            qstart = int(qstart)
            qend = int(qend)
            assert str(sequence)[qstart - 1 : qend] == qaln.replace("-", "")
            query_pre = str(sequence)[: qstart - 1]
            query_post = str(sequence)[qend:]
            query_aln = query_pre + qaln + query_post
            s1 = seq.CreateSequence(sequence.GetName(), query_aln)
            target_pre = "-" * len(query_pre)
            target_post = "-" * len(query_post)
            s2 = seq.CreateSequence("A", target_pre + taln + target_post)
            aln = seq.CreateAlignment()
            aln.AddSequence(s1)
            aln.AddSequence(s2)
            aln_list.append(aln)

        return seq.alg.MergePairwiseAlignments(aln_list, sequence)


class JackhmmerMSAGenerator(MSAGenerator):
    """Generates MSA based on Jackhmmer

    Eddy SR Accelerated Profile HMM Searches.PLOS Comp. Biol. (2017)

    :param jackhmmer_db: Database to retrieve homologues. In principle a big fat
                         fasta file (e.g. uniref90 downloaded from uniprot)
    :type jackhmmer_db: :class:`str`
    :param jackhmmer_exe: Path to jackhmmer executable, searches for "jackhmmer"
                          in path if not given.
    :type jackhmmer_exe: :class:`str`
    :param tmp_dir: Used by jackhmmer and to dump own temporary files. Created 
                    (and cleaned up at garbage collection) if not given.
    :type tmp_dir: :class:`str`
    :param num_iterations: Number of iterations in jackhmmer sequence search
    :type num_iterations: :class:`int`
    :param evalue_thresh: E-Value threshold for sequence to be included in 
                          result (or retained for next iteration if
                          *num_ierations*>1)
    :type evalue_thresh: :class:`float`
    :param n_cpu: Number of cores available to jackhmmer 
    :type n_cpu: :class:`int`
    """

    def __init__(
        self,
        jackhmmer_db,
        jackhmmer_exe=None,
        tmp_dir=None,
        num_iterations=1,
        evalue_thresh=0.0001,
        n_cpu=1,
    ):

        if not os.path.exists(jackhmmer_db):
            raise ValueError(f"jackhmmer_db ({jackhmmer_db}) does not exist")
        self.jackhmmer_db = _CheckDB(jackhmmer_db, prefix_db=False)

        self.jackhmmer_exe = settings.Locate(
            "jackhmmer", explicit_file_name=jackhmmer_exe
        )

        if num_iterations < 1:
            raise ValueError("num_iterations must be >= 1")
        self.num_iterations = num_iterations
        if evalue_thresh <= 0:
            raise ValueError("evalue_thresh must be > 0")
        self.evalue_thresh = evalue_thresh
        if n_cpu < 1:
            raise ValueError("n_cpu must be >= 1")
        self.n_cpu = n_cpu

        self.tmp_dir, self.tmp_dir_context = _SetupTMPDir(tmp_dir)

    def Run(self, sequence):
        """Implementation of parent interface.
        """
        # PREPARE
        #########
        query_seq_path, seq_hash = _PrepareSequence(self.tmp_dir, sequence)

        # SEARCH
        ########
        aln_file = os.path.join(self.tmp_dir, seq_hash + ".sto")
        jackhmmer_cmd = [
            self.jackhmmer_exe,
            "-N",
            str(self.num_iterations),
            "-E",
            str(self.evalue_thresh),
            "--domE",
            str(self.evalue_thresh),
            "--incE",
            str(self.evalue_thresh),
            "-o",
            "/dev/null",
            "-A",
            aln_file,
            "--cpu",
            str(self.n_cpu),
            query_seq_path,
            self.jackhmmer_db,
        ]
        subprocess.run(jackhmmer_cmd)

        if not os.path.exists(aln_file):
            raise RuntimeError("Jackhmmer execution failed")

        # PARSE AND RETURN
        ##################
        aln_sequences = collections.OrderedDict()
        with open(aln_file, "r") as fh:
            for line in fh:
                if line.startswith("//"):
                    break
                if line.strip() == "" or line.startswith("#"):
                    continue
                sname, s = line.split()
                if sname in aln_sequences:
                    aln_sequences[sname] += s
                else:
                    aln_sequences[sname] = s

        aln = seq.CreateAlignment()
        for k, v in aln_sequences.items():
            aln.AddSequence(seq.CreateSequence(k, v))
        return aln


class HHblitsMSAGenerator(MSAGenerator):
    """Generates MSA based on HHblits

    Steinegger M, Meier M, Mirdita M, Vöhringer H, Haunsberger S J, and Söding J 
    HH-suite3 for fast remote homology detection and deep protein annotation. 
    BMC Bioinformatics (2019)

    :param hhblits_db: Path to database searchable by HHblits
    :type hhblits_db: :class:`str`
    :param hhblits_exe: Path to HHblits executable, searches for "hhblits"
                        in path if not given.
    :type hhblits_exe: :class:`str`
    :param tmp_dir: Used by HHblits and to dump own temporary files. Created 
                    (and cleaned up at garbage collection) if not given.
    :type tmp_dir: :class:`str`
    :param n_cpu: Passed as :code:`-cpu` param to HHblits executable
    :type n_cpu: :class:`str`
    :param num_iterations: Passes as :code:`-n` param to HHblits executable
    :type num_iterations: :class:`int`
    :param evalue_thresh: Passed as :code:`-e` param to HHblits executable
    :type evalue_thresh: :class:`float` 
    """

    def __init__(
        self,
        hhblits_db,
        hhblits_exe=None,
        tmp_dir=None,
        n_cpu=1,
        num_iterations=1,
        evalue_thresh=0.001,
    ):

        self.hhblits_db = _CheckDB(hhblits_db, prefix_db=True)
        self.hhblits_exe = settings.Locate(
            "hhblits", explicit_file_name=hhblits_exe
        )

        if num_iterations < 1:
            raise ValueError("num_iterations must be >= 1")
        self.num_iterations = num_iterations
        if evalue_thresh <= 0:
            raise ValueError("evalue_thresh must be > 0")
        self.evalue_thresh = evalue_thresh
        if n_cpu < 1:
            raise ValueError("n_cpu must be >= 1")
        self.n_cpu = n_cpu

        self.tmp_dir, self.tmp_dir_context = _SetupTMPDir(tmp_dir)

    def Run(self, sequence):
        """Implementation of parent interface.
        """
        # PREPARE
        #########
        query_seq_path, seq_hash = _PrepareSequence(self.tmp_dir, sequence)

        # SEARCH
        ########
        aln_file = os.path.join(self.tmp_dir, seq_hash + ".a3m")
        hhblits_cmd = [
            self.hhblits_exe,
            "-i",
            query_seq_path,
            "-oa3m",
            aln_file,
            "-d",
            self.hhblits_db,
            "-cpu",
            str(self.n_cpu),
            "-n",
            str(self.num_iterations),
            "-e",
            str(self.evalue_thresh),
        ]
        subprocess.run(
            hhblits_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        if not os.path.exists(aln_file):
            raise RuntimeError("HHblits execution failed")

        # PARSE AND RETURN
        ##################
        with open(aln_file, "r") as fh:
            a3m_data = fh.readlines()

        msa_head = list()
        msa_seq = list()
        for line in a3m_data:
            if line.startswith("#") or len(line.strip()) == 0:
                continue
            if line.startswith(">"):
                msa_head.append(line[1:].strip())
                msa_seq.append("")
                continue
            if len(msa_seq) == 0:
                raise RuntimeError("corrupt a3m file")
            msa_seq[-1] += line.strip()
        assert msa_seq[0] == str(sequence)

        if len(msa_seq) == 1:
            # no other sequences found...
            return seq.CreateAlignment(sequence)

        aln_list = seq.AlignmentList()
        for i in range(1, len(msa_seq)):
            aln_query = ""
            aln_target = ""
            k = 0
            for c in msa_seq[i]:
                if c.islower():
                    aln_query += "-"
                    aln_target += c.upper()
                else:
                    aln_query += msa_seq[0][k]
                    aln_target += c
                    k += 1
            aln = seq.CreateAlignment(
                seq.CreateSequence(msa_head[0], aln_query),
                seq.CreateSequence(msa_head[i], aln_target),
            )
            aln_list.append(aln)
        return seq.alg.MergePairwiseAlignments(aln_list, sequence)
