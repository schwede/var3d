import hashlib
import subprocess
import json
import os
import tempfile
import pymongo

import ost
from ost import seq
from ost.seq.alg import aaindex

from var3d.base import *

import pdb


class ConsurfSeqAnno(SeqAnno):
    """Implementation of SeqAnno - Annotation with ConsurfDB pipeline.

    Chorin AB, Masrati G, Kessel A, Narunsky A, Sprinzak J, Lahav S, 
    Ashkenazy H, Ben-Tal N ConSurf-DB: An accessible repository for the 
    evolutionary conservation patterns of the majority of PDB proteins
    Protein Sci. (2019) 

    The code is not opensource but kindly provided by the Ben-Tal group
    and wrapped into a Singularity container by us.

    ConSurf conservation scores are in range [1-9]. However, the returned values
    additionally encode confidence. A score is considered non-confident if less
    than 6 non-gaped homologue sequences have been found at that location or if
    the ConSurf internal confidence interval is >= 4. Non-confident scores are
    multiplied by -1. So a non-confident score of 8 would be represented as -8.

    :param seq_db: Path to sequence db that can be read by Jackhmmer, i.e. a big 
                   fat fasta file. Typically uniref90
    :type seq_db: :class:`str`
    :param key: Return value of parent class Key function
    :type key: :class:`str`
    :param tmp_dir: Directory to be used as working directory by consurf, used
                    to store intermediate results. Will be created (and cleaned 
                    up) if not provided.
    :type tmp_dir: :class:`str`
    """

    def __init__(self, seq_db, key="consurf", tmp_dir=None):

        if not os.path.exists(seq_db):
            raise RuntimeError(f"seq_db does not exist ({seq_db})")
        self.seq_db = seq_db
        self.seq_db_dir = os.path.dirname(self.seq_db)

        self.key = key
        if tmp_dir:
            if not os.path.isdir(tmp_dir):
                raise ValueError(
                    f"Provided tmp_dir ({tmp_dir}) is not a " "directory"
                )
            self.tmp_dir = os.path.abspath(tmp_dir)
        else:
            # as soon as self.tmp_dir_context is garabage collected,
            # the directory is deleted on disk
            self.tmp_dir_context = tempfile.TemporaryDirectory()
            self.tmp_dir = self.tmp_dir_context.name

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface 
        """
        working_seq = self.SubSeq(sequence, seq_range)
        seq_hash = hashlib.md5(str(working_seq).encode()).hexdigest()
        workdir = os.path.join(self.tmp_dir, seq_hash)
        os.makedirs(workdir)
        result_json = os.path.join(workdir, "result.json")

        cmd = [
            "python3",
            "/consurf/consurf_wrapper.py",
            "--workdir",
            workdir,
            "--seqres",
            str(working_seq),
            "--seq_db",
            self.seq_db,
            "--result_json",
            result_json,
        ]

        p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if p.returncode != 0 or not os.path.exists(result_json):
            e_path = os.path.join(workdir, "uniqueChains", "AAAAA", "err.log")
            if os.path.exists(e_path):
                with open(e_path) as fh:
                    log_error = fh.read()
                raise RuntimeError(
                    "Consurf failed - For full info, specify "
                    "tmp_dir and manually crawl through the "
                    "generated logs. Error log: " + log_error
                )
            else:
                raise RuntimeError(
                    "Consurf failed - Consurf errors are hard "
                    "to catch. There's probably no way around "
                    "specifying a tmp_dir and crawl through the "
                    "generated logs"
                )

        with open(result_json, "r") as fh:
            result = json.load(fh)

        color = result["COLOR"]
        confidence = result["CONFIDENCE"]
        scores = [a if b else -a for a,b in zip(color, confidence)]
        return self.MapAnno(sequence, scores, seq_range)


class EntropySeqAnno(SeqAnno):
    """Implementation of SeqAnno - Shannon entroy based on MSA.

    MSA is generated with a :class:`var3d.msa_generator.MSAGenerator` instance
    which needs to be provided at construction. Calculates the shannon entropy
    of all columns and maps them onto target sequence, i.e. extracts entropy
    values from columns in the alignment in which the target sequence has no
    gaps.

    :param msa_generator: Generates an MSA using a sequence as input
    :type msa_generator: :class:`MSAGenerator`
    :param key: Return value of parent class Key function, default: "entropy"
    :type key: :class:`str`
    :param normalize: Normalize return values if True, i.e. divide all entropies
                      by max entropy
    :type normalize: :class:`bool`             
    """

    def __init__(self, msa_generator, key="entropy", normalize=False):
        self.msa_generator = msa_generator
        self.key = key
        self.normalize = normalize

    def Key(self):
        """Implementation of parent interface. 
        """
        return self.key

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface. 
        """
        working_seq = self.SubSeq(sequence, seq_range)
        msa = self.msa_generator.GetMSA(working_seq)
        entropies = seq.alg.ShannonEntropy(msa)
        mapped_entropies = list()
        for olc, e in zip(str(msa.GetSequence(0)), entropies):
            if olc != "-":
                mapped_entropies.append(e)
        if self.normalize:
            max_entropy = max(mapped_entropies)
            if max_entropy > 0.0:
                mapped_entropies = [e / max_entropy for e in mapped_entropies]
        return self.MapAnno(sequence, mapped_entropies, seq_range)


class AAIndexSeqAnno(SeqAnno):
    """Implementation of SeqAnno - Sequence annotations based on the AAindex DB 

    Kawashima, S. and Kanehisa, M.; AAindex: amino acid index database. 
    Nucleic Acids Res. 28, 374 (2000).

    AAindex contains entries for single amino acid scores and pairwise scores. 
    The :class:`AAIndexSeqAnno` class can only deal with the former.
    The returned annotations are extracted from AAindex, None if there is no
    entry for the according one letter code.

    :param aaindex_ac: AAindex database accesion number (e.g.ANDN920101)
    :type aaindex_ac: :class:`str`
    :param key: Return value of parent class Key function
    :type key: :class:`str`
    :param aaindex_files: Passed to ost.seq.alg.aaindex.AAIndex constructor,
                          default database is used if not given (recommended).
    :type aaindex_files: :class:`list` of :class:`str`
    :raises: :class:`RuntimeError` if AAindex database entry is not of type
             :class:`ost.seq.alg.aaindex.AnnoType.SINGLE`
    """

    def __init__(self, aaindex_ac, key, aaindex_files=None):
        self.key = key
        self.index = aaindex.AAIndex(aaindex_files=aaindex_files)
        self.index_data = self.index[aaindex_ac]
        if self.index_data.anno_type != aaindex.AnnoType.SINGLE:
            raise RuntimeError(
                "AAIndexSeqAnno can only deal with single amino "
                "scores from the AAindex database"
            )

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface 
        """
        working_seq = self.SubSeq(sequence, seq_range)
        anno = list()
        for olc in working_seq:
            try:
                score = self.index_data.GetScore(olc)
            except:
                score = None
            anno.append(score)
        return self.MapAnno(sequence, anno, seq_range)
