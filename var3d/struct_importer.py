import json
import requests
import time

from ost import seq
from ost import io
from ost import conop

from var3d.base import *
from var3d import common
from var3d import uniprot

import pdb


class FilesystemStructImporter(StructImporter):
    """Import structures from the filesystem

    You can basically hardcode what structures from disk should be imported
    for a sequence with given name. Call :func:`Add` for each structure you
    want to associate with a certain sequence name.
    """

    def __init__(self):
        self.data = dict()

    def Add(
        self,
        sequence,
        structure_identifier,
        structure_file,
        segment_query=None,
        aln=None,
        meta=dict(),
    ):
        """Add structure that should be imported from disk

        Associates name of *sequence* with structure data. If a sequence with
        this name enters the Run function, a :class:`var3d.base.Structure`
        object gets created and imported. No parameter checking is performed
        here. So you'll get trouble when :meth:`Run` is called if for example
        *structure_file* doesn't exist or when the :class:`var3d.base.Structure`
        constructor is not happy with the input.

        :param sequence: Only used to extract name to relate it with the other
                         parameters
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param structure_identifier: Will be passed to :class:`Structure`
                                     constructor
        :type structure_identifier: :class:`str`
        :param structure_file: Path to file on disk which can be loaded with
                               :meth:`ost.io.LoadEntity`. The loaded structure
                               will be passed as *assembly* param to 
                               :class:`var3d.base.Structure` constructor.
        :type structure_file: :class:`str`/:class:`list` of :class:`str`
        :param segment_query: Will be passed to :class:`var3d.base.Structure`
                              constructor. If set to None, a query in form 
                              'peptide=True and cname=X' for each chain X
                              that can be fully aligned to sequence is
                              added. Fully aligned means successful alignment
                              with :func:`ost.seq.alg.AlignToSEQRES`
        :type segment_query: :class:`str`
        :param aln: Will be passed to :class:`var3d.base.Structure` constructor.
                    Must be None if *segment_query* is None. Must have same
                    number of elements otherwise.
        :type aln: :class:`ost.seq.AlignmentHandle`/:class:`list` of
                   :class:`AlignmentHandle`
        :param meta: Meta data for imported structure. Elements must be JSON
                     serializable, will raise at import otherwise.
        :type meta: :class:`dict`
        """
        if segment_query is None and aln is not None:
            raise RuntimeError("Cannot provide aln if segment_query is None")

        if segment_query is not None and aln is not None:
            if len(segment_query) != len(aln):
                raise RuntimeError("segment_query and aln must have same size")

        sname = sequence.GetName()
        if sname not in self.data:
            self.data[sname] = list()
        self.data[sname].append(
            {
                "structure_identifier": structure_identifier,
                "structure_file": structure_file,
                "segment_query": segment_query,
                "aln": aln,
                "meta": meta,
            }
        )

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface
        """
        structures = list()
        sname = sequence.GetName()
        if sname in self.data:
            for item in self.data[sname]:
                assembly = io.LoadEntity(item["structure_file"])
                s = None
                if item["segment_query"] is None:
                    segment_query = list()
                    for ch in assembly.Select("peptide=true").chains:
                        try:
                            seq.alg.AlignToSEQRES(
                                ch,
                                sequence,
                                try_resnum_first=True,
                                validate=False,
                            )
                            query = f"peptide=true and cname={ch.GetName()}"
                            segment_query.append(query)
                        except:
                            pass
                    s = Structure(
                        sequence,
                        item["structure_identifier"],
                        assembly,
                        segment_query,
                        aln=None,
                    )
                else:
                    s = Structure(
                        sequence,
                        item["structure_identifier"],
                        assembly,
                        item["segment_query"],
                        aln=item["aln"],
                    )
                if s.InRange(seq_range):
                    for k, v in item["meta"].items():
                        s.AddMetaData(k, v)
                    structures.append(s)
        return structures


class SMRStructImporter(StructImporter):
    """Imports structures from the SWISS-MODEL Repository

    See Run documentation for more information

    :param url: The SWISS-MODEL repository url
    :type url: :class:`str`
    :param fix_atomseq: Atomseq fetched from SMR can differ from structure data.
                        If true, atomseq will be adapted to what we have in the
                        structure while preserving the gaps from the SMR
                        alignment.
                        One example are microheterogeneities. E.g. rnum 25 in
                        pdb entry 3nir is either LEU or ILE. Both identities are
                        present in separate segments but the structure parsed
                        with OpenStructure only has LEU. If *fix_atomseq* is
                        true, we still have separate segments but both have LEU
                        in the atomseq, thus covering that LEU twice.
    :type fix_atomseq: :class:`bool`
    """

    def __init__(
        self, url="https://swissmodel.expasy.org/repository", fix_atomseq=True
    ):
        self.url = url
        self.fix_atomseq = fix_atomseq

    def Run(self, sequence, seq_range=None):
        """Implementation of parent interface
    
        Assumes name of sequence to correspond to the uniprot AC of interest and
        imports everything available from SWISS-MODEL Repository, given that they
        cover *seg_range*. Experimental data as well as theoretical models. 
        The structure_identifier of the returned Structures is of the form 
        SMR_<provider>_<id> where provider can be PDB or SWISSMODEL. id is
        either the pdb id of the actual structure (provider: PDB) or the pdb id
        of the template used for modelling (provider: SWISS-MODEL). 

        :returns: :class:`list` of :class:`var3d.base.Structure` objects
                  representing the SWISS-MODEL repository data
        """
        entry_data = self._FetchEntryData(sequence)
        structures = list()
        for structure_data in entry_data["result"]["structures"]:
            s = self._ProcessEntry(sequence, structure_data, seq_range)
            if (
                s is not None
            ):  # None returned if entry doesnt cover seq_range...
                structures.append(s)
        return structures

    def _FetchEntryData(self, sequence):
        """Fetches info on SMR data for uniprot AC specified in sequence name

        Checks for SMR API version and raises if not known, i.e. not 2.0
        """
        if not uniprot.ValidACPattern(sequence.GetName()):
            raise ValueError(
                f"Sequence name ({sequence.GetName()}) seems no "
                f"valid uniprot AC"
            )

        data_url = f"{self.url}/uniprot/{sequence.GetName()}.json"
        r = requests.get(data_url)
        if r.status_code == 429:
            # we hit rate-limiting... Let's wait and try again
            time.sleep(3)
            r = requests.get(data_url)
        if r.status_code != 200:
            raise RuntimeError(
                "SMRStructImporter failed to fetch entry data "
                "from " + data_url
            )
        data = json.loads(r.text)

        api_version = data["api_version"]
        if api_version != "2.0":
            raise RuntimeError(
                f"SMRStructImporter implemented for SMR API "
                f"version 2.0, got response with API version "
                f"{api_version}"
            )
        return data

    def _FetchCoordinates(self, structure_data):
        """Fetches coordinates specified in entry data from _FetchEntryData
        """
        url = structure_data["coordinates"]
        r = requests.get(url)
        if r.status_code == 429:
            # we hit rate-limiting... Let's wait and try again
            time.sleep(3)
            r = requests.get(url)
        if r.status_code != 200:
            raise RuntimeError(
                "SMRStructImporter failed to fetch coordinates " "from " + url
            )
        if conop.GetDefaultLib():
            processor = conop.RuleBasedProcessor(conop.GetDefaultLib())
        else:
            processor = conop.HeuristicProcessor()
        profile = io.IOProfile(
            dialect="PDB",
            fault_tolerant=False,
            quack_mode=False,
            processor=processor,
        )
        return io.PDBStrToEntity(r.text, profile=profile, process=True)

    def _ProcessEntry(self, sequence, structure_data, seq_range):
        """Processes each chain in entry that maps to *sequence*
        """
        chain_names, segments = self._GetSegmentData(structure_data, seq_range)
        if len(chain_names) == 0:
            return None  # no segment survived the seq_range filter...

        coordinates = self._FetchCoordinates(structure_data)

        # create meta data directory
        cur_meta = {}
        cur_meta["coverage"] = structure_data["coverage"]
        cur_meta["created_date"] = structure_data["created_date"]
        cur_meta["method"] = structure_data["method"]
        cur_meta["oligo-state"] = structure_data["oligo-state"]
        cur_meta["provider"] = structure_data["provider"]
        cur_meta["template"] = structure_data["template"]

        if structure_data["method"] == "HOMOLOGY MODELLING":
            cur_meta["similarity"] = structure_data["similarity"]
            cur_meta["found_by"] = structure_data["found_by"]
            cur_meta["identity"] = structure_data["identity"]
            cur_meta["gmqe"] = structure_data["gmqe"]

        alignments = list()
        segment_queries = list()

        for chain_name, segment in zip(chain_names, segments):
            query = None
            aln = None
            chain = coordinates.FindChain(chain_name)
            if not chain.IsValid():
                raise RuntimeError(
                    f"Didnt find expected chain {chain_name} "
                    f"in downloaded structure"
                )
            if structure_data["provider"] == "SWISSMODEL":
                query = (
                    f"cname={chain.GetName()}"
                )  # it's just the whole chain..
                atomseq = ["-"] * len(sequence)
                for r in chain.residues:
                    atomseq[r.GetNumber().GetNum() - 1] = r.one_letter_code
                aln = seq.CreateAlignment()
                aln.AddSequence(sequence)
                aln.AddSequence(
                    seq.CreateSequence(chain_name, "".join(atomseq))
                )
                alignments.append(aln)
                segment_queries.append(query)
            elif structure_data["provider"] == "PDB":
                segment_atomseq = segment["pdb"]["aligned_sequence"]
                atomseq_from = segment["pdb"]["from"]
                atomseq_to = segment["pdb"]["to"]
                segment_seqres = segment["uniprot"]["aligned_sequence"]
                seqres_from = segment["uniprot"]["from"]
                seqres_to = segment["uniprot"]["to"]

                query = f"cname={chain.GetName()} "
                query += f"and rnum={atomseq_from}:{atomseq_to}"

                if self.fix_atomseq:
                    segment_atomseq = self._FixATOMSEQ(
                        coordinates, segment_atomseq, query
                    )

                segment_queries.append(query)

                seqres_before = sequence[: seqres_from - 1]
                seqres_after = sequence[seqres_to:]
                seqres = seqres_before + segment_seqres + seqres_after
                atomseq_before = "-" * len(seqres_before)
                atomseq_after = "-" * len(seqres_after)
                atomseq = atomseq_before + segment_atomseq + atomseq_after
                aln = seq.CreateAlignment()
                aln.AddSequence(seq.CreateSequence("seqres", seqres))
                aln.AddSequence(seq.CreateSequence("atomseq", atomseq))
                alignments.append(aln)
            else:
                raise RuntimeError(
                    "Uknown structure provider in SMR Struct"
                    "Importer: " + structure_data["provider"]
                )

        structure_identifier = "_".join(
            ["SMR", structure_data["provider"], structure_data["template"]]
        )
        s = Structure(
            sequence,
            structure_identifier,
            coordinates,
            segment_queries,
            aln=alignments,
        )

        for key in cur_meta.keys():
            s.AddMetaData(key, cur_meta[key])

        return s

    def _GetSegmentData(self, structure_data, seq_range):
        """Extracts all segments in *structure_data* that map to *sequence*

        Returns two lists, first one being chain names of each segment and the
        second one the actual segments.
        """
        chain_names = list()
        segments = list()
        for chain_data in structure_data["chains"]:
            for segment in chain_data["segments"]:
                if seq_range is None:
                    chain_names.append(chain_data["id"])
                    segments.append(segment)
                else:
                    seqres_from = segment["uniprot"]["from"]
                    seqres_to = segment["uniprot"]["to"]
                    out = seqres_to < seq_range[0] or seqres_from > seq_range[1]
                    if not out:
                        chain_names.append(chain_data["id"])
                        segments.append(segment)
        return (chain_names, segments)

    def _FixATOMSEQ(self, coordinates, segment_atomseq, query):
        """Basically overwrites what we have in segment_atomseq with what we
        have in coordinates while keeping the gaps.
        """
        segment = coordinates.Select(query)
        assert len(segment.residues) == len(segment_atomseq.replace("-", ""))
        res_olcs = [r.one_letter_code for r in segment.residues]
        return_seq = ["-"] * len(segment_atomseq)
        r_idx = 0
        for olc_idx in range(len(return_seq)):
            if segment_atomseq[olc_idx] != "-":
                return_seq[olc_idx] = res_olcs[r_idx]
                r_idx += 1
        return "".join(return_seq)
