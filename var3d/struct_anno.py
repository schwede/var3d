import numpy as np
import tempfile
import os
import subprocess
import shutil

import ost
from ost import conop
from ost import geom
from ost import mol
from ost.mol.alg import Accessibility

from promod3 import modelling

from var3d.base import *


class AccessibilityStructAnno(StructAnno):
    """Implementation of StructAnno - Annotates solvent accessibility

    Uses the Openstructure Accessibility function and returns annotations 
    either in square angstrom or normalized to range [0, 1]

    :param normalize: Whether to normalize
    :type normalize: :class:`bool`
    :param key: Return value of parent class Key function, default: 
                "accessibility"
    :type key: :class:`str`
    """

    def __init__(
        self,
        probe_radius=1.4,
        include_hydrogens=False,
        include_hetatm=False,
        include_water=False,
        normalize=False,
        key="accessibility",
    ):
        self.probe_radius = probe_radius
        self.include_hydrogens = include_hydrogens
        self.include_hetatm = include_hetatm
        self.include_water = include_water
        self.normalize = normalize
        self.key = key

    def Run(self, sequence, structures, seq_range=None):
        """Implementation of parent interface
        """
        anno = list()
        for structure in structures:
            Accessibility(
                structure.assembly,
                probe_radius=self.probe_radius,
                include_hydrogens=self.include_hydrogens,
                include_hetatm=self.include_hetatm,
                include_water=self.include_water,
            )
            prop_name = "asaAbs"
            if self.normalize:
                prop_name = "asaRel"
            structure_anno = list()
            for segment_idx in range(structure.GetNumSegments()):
                segment_anno = list()
                for r in structure.segments[segment_idx].residues:
                    if r.HasProp(prop_name):
                        segment_anno.append(r.GetFloatProp(prop_name))
                    else:
                        segment_anno.append(None)
                structure_anno.append(
                    structure.MapToReference(segment_anno, segment_idx)
                )
            anno.append(structure_anno)
        return anno

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def PerResidue(self):
        """Implementation of parent interface
        """
        return True


class TransmembraneStructAnno(StructAnno):
    """Implementation of StructAnno - Identiefies transmembrane structures

    Returns a dictionary as annotation that informs whether a structure
    is likely to be a transmembrane structure. If yes, the parameters to
    construct two planes representing the membranes are given too. The
    additional parameter, the axis_transform, defines a transformation matrix
    to align the membrane normal with the z-axis.
    E.g. {"is_transmem": False, "mem_param": {}}
    or {"is_transmem": True, "mem_param": {"axis": [x,y,z],
    "plane_center_one": [x,y,z], "plane_center_two": [x,y,z],
    "radius": r, "width": w, "energy": e, "axis_transform": m}}

    :param key: Return value of parent class Key function, default:
                "transmembrane"
    :type key:  :class:`str`
    """

    def __init__(self, key="transmembrane"):
        self.key = key

    def Run(self, sequence, structures, seq_range=None):
        """Implementation of parent interface
        """
        anno = list()
        comp_lib = conop.GetDefaultLib()
        for structure in structures:
            anno.append(
                self._GetMembraneJSON(structure.assembly, comp_lib=comp_lib)
            )
        return anno

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def PerResidue(self):
        """Implementation of parent interface
        """
        return False

    def _GetMembraneJSON(self, prot, comp_lib):
        """Returns JSON serializable dict.
    
        :param prot: The protein to analyze
        Returns a dictionary with membrane annotations
        """
        membrane_json = dict()

        mem_result = self._IsTransMem(prot, comp_lib)
        membrane_json = {"is_transmem": mem_result[0], "mem_param": {}}

        if mem_result[0]:
            mem_axis = mem_result[1].membrane_axis
            if self._InvertMembraneAxis(prot, mem_result[1]):
                membrane_json["mem_param"]["axis"] = [
                    -mem_axis[0],
                    -mem_axis[1],
                    -mem_axis[2],
                ]
            else:
                membrane_json["mem_param"]["axis"] = [
                    mem_axis[0],
                    mem_axis[1],
                    mem_axis[2],
                ]
            membrane_json["mem_param"]["width"] = round(mem_result[1].width, 4)
            membrane_json["mem_param"]["energy"] = round(
                mem_result[1].energy, 4
            )

            plane_one = geom.Plane(
                mem_result[1].pos * mem_axis
                + mem_axis * mem_result[1].width / 2,
                mem_axis,
            )
            plane_two = geom.Plane(
                mem_result[1].pos * mem_axis
                - mem_axis * mem_result[1].width / 2,
                mem_axis,
            )

            mem_selection = mem_result[2].Select("grtransmem:0=1")
            center = mem_selection.geometric_center
            central_line = geom.Line3(center, center + mem_axis)

            plane_one_center = geom.IntersectionPoint(central_line, plane_one)
            plane_two_center = geom.IntersectionPoint(central_line, plane_two)
            radius = 0.0
            for a in mem_selection.atoms:
                radius = max(radius, geom.Distance(central_line, a.GetPos()))

            membrane_json["mem_param"]["plane_one_center"] = [
                plane_one_center[0],
                plane_one_center[1],
                plane_one_center[2],
            ]

            membrane_json["mem_param"]["plane_two_center"] = [
                plane_two_center[0],
                plane_two_center[1],
                plane_two_center[2],
            ]
            membrane_json["mem_param"]["radius"] = round(radius, 4)

            membrane_json["mem_param"][
                "axis_transform"
            ] = self._GetAxisTransform(membrane_json["mem_param"]["axis"])

        return membrane_json

    def _IsTransMem(self, prot, comp_lib):
        """
        Takes an entity as input and runs the OpenStructure FindMembrane
        algorithm. Based on the results, the entity is classified as
        transmembrane or not.
        
        Relevant criteria:
    
        * Energy value from FindMembrane
        * Energy value relative to membrane accessible surface from FindMembrane
        * Width of the identified membrane
        * Fraction of membrane associated secondary structure elements that are
          roughly aligned to the membrane normal from FindMembrane
    
        :param prot:        Entity to be classified
        :param comp_lib:    Compound library to cleanup *prot*
        :type prot:         :class:`ost.mol.EntityHandle`/
                            :class:`ost.mol.EntityView`
        :type comp_lib:     :class:`ost.conop.CompoundLib`
        :returns:           :class:`tuple` with first element being the boolean
                            classification, the second element the result from
                            FindMembrane and the third element an EntityView
                            that has been used as input to FindMembrane where
                            transmembrane residue have a 'transmem' generic int
                            prop set to 1. The EntityView is based on a copy of
                            prot which has been processed prior to calling
                            FindMembrane.
        """

        # make sure that we have at least the backbone atoms for each residue
        # sidechain remodelling might fail otherwise
        prot = prot.Select("peptide=true")
        for r in prot.residues:
            valid_n = r.FindAtom("N").IsValid()
            valid_ca = r.FindAtom("CA").IsValid()
            valid_c = r.FindAtom("C").IsValid()
            valid_o = r.FindAtom("O").IsValid()
            # ReconstructSidechains can deal with missing CB atoms
            if valid_n and valid_ca and valid_c and valid_o:
                r.SetIntProp("complete", 1)
            else:
                r.SetIntProp("complete", 0)
        prot = mol.CreateEntityFromView(prot.Select("grcomplete=1"), False)

        # reconstruct sidechains
        modelling.ReconstructSidechains(
            prot, keep_sidechains=True, rotamer_model="rrm"
        )

        # secondary structures must be set when calling
        # GetMemSegments
        try:
            mol.alg.AssignSecStruct(prot)
        except:
            # This might fail for CA trace and all residues have been removed
            return (False, None, prot)

        # detect and remove residues which are still incomplete
        for r in prot.residues:
            complete = True
            try:
                complete = comp_lib.IsResidueComplete(r.handle)
            except:
                complete = False
            if complete and r.name != "UNK":
                r.SetIntProp("complete", 1)
            else:
                r.SetIntProp("complete", 0)
        prot = prot.Select("grcomplete=1")

        try:
            mem_param = mol.alg.FindMembrane(prot)
        except:
            return (False, None, prot)

        sheet_angles = list()
        helix_angles = list()
        membrane_axis = mem_param.membrane_axis
        central_plane = geom.Plane(mem_param.pos * membrane_axis, membrane_axis)

        for ch in prot.chains:
            sheet_segments = self._GetSegments(
                ch, mem_param, ss_type="sheet", min_length=5, min_mem_exposed=2
            )
            for segment in sheet_segments:
                ca_positions = geom.Vec3List()
                for idx in range(segment.first, segment.last + 1):
                    ca_positions.append(
                        ch.residues[idx].FindAtom("CA").GetPos()
                    )
                dir_vec = ca_positions.principal_axes.GetRow(2)
                angle = geom.Angle(dir_vec, membrane_axis)
                sheet_angles.append(min(angle, np.pi - angle))

            helical_segments = self._GetSegments(
                ch, mem_param, ss_type="helix", min_length=10, min_mem_exposed=5
            )
            for segment in helical_segments:
                ca_positions = geom.Vec3List()
                for idx in range(segment.first, segment.last + 1):
                    ca_positions.append(
                        ch.residues[idx].FindAtom("CA").GetPos()
                    )
                dir_vec = ca_positions.principal_axes.GetRow(2)
                angle = geom.Angle(dir_vec, membrane_axis)
                helix_angles.append(min(angle, np.pi - angle))

        n_helix = len(helix_angles)
        n_sheet = len(sheet_angles)
        helix_frac = 0
        sheet_frac = 0
        if n_helix > 0:
            helix_frac = np.mean(
                [int(a / np.pi * 180 < 50) for a in helix_angles]
            )
        if n_sheet > 0:
            sheet_frac = np.mean(
                [int(a / np.pi * 180 < 50) for a in sheet_angles]
            )

        e = mem_param.energy
        asa = mem_param.membrane_asa
        w = mem_param.width

        mem_param_criteria = e < -30000 and e / asa < -8.0 and w > 18.0
        angle_criteria = (n_sheet > n_helix and sheet_frac >= 0.5) or (
            n_helix > n_sheet and helix_frac >= 0.5
        )
        is_transmem = bool(mem_param_criteria and angle_criteria)

        if is_transmem:
            self._SetTransmemProp(prot, mem_param)

        return (is_transmem, mem_param, prot)

    def _GetSegments(
        self, chain, mem, ss_type="sheet", min_length=5, min_mem_exposed=2
    ):
        segments = None
        if ss_type == "sheet":
            segments = mol.alg.ExtractExtendedSegments(chain)
        elif ss_type == "helix":
            segments = mol.alg.ExtractHelicalSegments(chain)
        else:
            raise RuntimeError("ss_type must either be sheet or helix")

        mem_segments = mol.alg.SecStructureSegments()
        plane = geom.Plane(mem.pos * mem.membrane_axis, mem.membrane_axis)

        for segment in segments:
            res_lists = list()
            current_res_list = list()
            for idx in range(segment.first, segment.last + 1):
                chain.residues[idx].SetIntProp("idx", idx)
                ca = chain.residues[idx].FindAtom("CA")
                if ca.IsValid():
                    dist_to_center = abs(geom.Distance(plane, ca.GetPos()))
                    if dist_to_center < (mem.width / 2):
                        if len(current_res_list) > 0:
                            if not mol.InSequence(
                                current_res_list[-1].handle,
                                chain.residues[idx].handle,
                            ):
                                res_lists.append(current_res_list)
                                current_res_list = list()
                        current_res_list.append(chain.residues[idx])
            if len(current_res_list) > 0:
                res_lists.append(current_res_list)

            for rl in res_lists:
                if len(rl) >= min_length:
                    property_counter = 0
                    for r in rl:
                        has_prop = False
                        for a in r.atoms:
                            if a.HasProp("membrane_e"):
                                has_prop = True
                                break
                        if has_prop:
                            property_counter += 1
                    if property_counter >= min_mem_exposed:
                        start = rl[0].GetIntProp("idx")
                        end = rl[-1].GetIntProp("idx")
                        t = segment.ss_type
                        new_segment = mol.alg.SecStructureSegment(start, end, t)
                        mem_segments.append(new_segment)

        return mem_segments

    def _SetTransmemProp(self, prot, mem_param):
        membrane_residues = dict()
        plane = geom.Plane(
            mem_param.pos * mem_param.membrane_axis, mem_param.membrane_axis
        )
        for r in prot.residues:
            ca = r.FindAtom("CA")
            if ca.IsValid():
                dist_to_center = abs(geom.Distance(plane, ca.GetPos()))
                if dist_to_center < (mem_param.width / 2):
                    r.SetIntProp("transmem", 1)

    def _InvertMembraneAxis(self, prot, mem_param):
        """
        Awesome heuristic to check whether the membrane axis points in the 
        direction with more residues outside the membrane. This looks better
        when you use the membrane axis to orient a transmembrane structure in a
        viewer. 
        """
        plane = geom.Plane(
            mem_param.pos * mem_param.membrane_axis, mem_param.membrane_axis
        )
        half_width = mem_param.width / 2.0
        this_count = 0
        other_count = 0
        for r in prot.residues:
            ca = r.FindAtom("CA")
            if ca.IsValid():
                dist_to_center = geom.Distance(plane, ca.GetPos())
                if dist_to_center > half_width:
                    this_count += 1
                elif abs(dist_to_center) > half_width:
                    other_count += 1
        return other_count > this_count

    def _GetAxisTransform(self, axis):
        """
        Estimate transformation that membrane normal points in direction of
        Z-axis. Useful to nicely render in 3D viewers. Returns transformation
        as list of 16 elements, representing a 4x4 transformation matrix.

        :param axis: The membrane normal, some indexable object with 3 elements,
                     i.e. a vector
        :returns: 16 element list representing a 4x4 transformation matrix
        """
        # rotation around x-axis to end up in xy-plane
        x_axis = geom.Vec3(1.0, 0.0, 0.0)
        y_axis = geom.Vec3(0.0, 1.0, 0.0)
        z_axis = geom.Vec3(0.0, 0.0, 1.0)
        if abs(axis[2]) > 0.0001:
            # first vector is only y&z component of the initial axis
            a = geom.SignedAngle(
                geom.Vec3(0.0, axis[1], axis[2]), y_axis, x_axis
            )
            sina = np.sin(a)
            cosa = np.cos(a)
        else:
            # we are already in xy-plane... no rotation required...
            a = 0.0
            sina = 0.0
            cosa = 1.0
        axis = geom.Vec3(
            axis[0],
            cosa * axis[1] - sina * axis[2],
            sina * axis[1] + cosa * axis[2],
        )
        # rotation around z-axis
        b = geom.SignedAngle(axis, y_axis, z_axis)
        sinb = np.sin(b)
        cosb = np.cos(b)

        return [
            cosb,
            -cosa * sinb,
            sina * sinb,
            0,
            sinb,
            cosa * cosb,
            -sina * cosb,
            0,
            0,
            sina,
            cosa,
            0,
            0,
            0,
            0,
            1,
        ]


class InterfaceStructAnno(StructAnno):
    """Implementation of StructAnno - Annotates Protein-Protein interfaces

    Uses the Openstructure Accessibility function to compute accessibilities
    on the full complex and on the chains in isolation. If the accessibility of
    a residue changes by at least *thresh* square angstrom, it's considered an
    interface residue.

    :param thresh: Threshold for accessibility change in A2 to be considered
                   an interface residue
    :type thresh: :class:`float`
    :param key: Return value of parent class Key function, default: 
                "accessibility"
    :type key: :class:`str`
    """

    def __init__(self, thresh=0.06, key="interface"):
        self.thresh = thresh
        self.key = key

    def Run(self, sequence, structures, seq_range=None):
        """Implementation of parent interface
        """
        anno = list()
        for structure in structures:
            Accessibility(
                structure.assembly.Select("peptide=True"), oligo_mode=True
            )
            structure_anno = list()
            for segment_idx in range(structure.GetNumSegments()):
                segment_anno = list()
                for r in structure.segments[segment_idx].residues:
                    if r.HasProp("asaAbs") and r.HasProp("asaAbs_single_chain"):
                        asa_complex = r.GetFloatProp("asaAbs")
                        asa_single_chain = r.GetFloatProp("asaAbs_single_chain")
                        diff = asa_single_chain - asa_complex
                        if diff > self.thresh:
                            segment_anno.append(1)
                        else:
                            segment_anno.append(0)
                    else:
                        segment_anno.append(None)
                structure_anno.append(
                    structure.MapToReference(segment_anno, segment_idx)
                )
            anno.append(structure_anno)
        return anno

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def PerResidue(self):
        """Implementation of parent interface
        """
        return True
    

class DistStructAnno(StructAnno):
    """Implementation of StructAnno - Annotates distances to user selections

    Computes the minimal distance to any of the residue atoms towards any of
    the atoms in the user defined selection.

    :param query: User defined selection to which distances should be computed
                  default: "peptide=true"
    :type query: :class:`str`
    :param intrachain: Whether to include distances towards atoms in the same
                       chain
    :type intrachain: :class:`bool`
    :param max_dist: Everything above *max_dist* will be set to *max_dist*
    :type max_dist: :class:`float`
    :param key: Return value of parent class Key function, default: "dist"
    :type key: :class:`str`
    """

    def __init__(self, query="peptide=true", intrachain=False, 
                 max_dist = 20.0, key="dist"):
        self.query = query
        self.intrachain = intrachain
        self.max_dist = max_dist
        self.key = key

    def Run(self, sequence, structures, seq_range=None):
        """Implementation of parent interface
        """
        anno = list()
        for structure in structures:
            structure_anno = list()
            for segment_idx in range(structure.GetNumSegments()):
                query = self.query
                if self.intrachain is False:
                    cname = structure.aln[segment_idx].GetSequence(1).GetName()
                    query = "(cname!=" + cname + ") and (" + query + ')'
                sel = structure.assembly.Select(query)
                sel_pos = geom.Vec3List([a.GetPos() for a in sel.atoms])
                segment_anno = list()
                for r in structure.segments[segment_idx].residues:
                    res_pos = geom.Vec3List([a.GetPos() for a in r.atoms])
                    d = res_pos.GetMinDist(sel_pos)
                    if d > self.max_dist:
                        d = self.max_dist
                    segment_anno.append(d)
                structure_anno.append(
                    structure.MapToReference(segment_anno, segment_idx)
                )
            anno.append(structure_anno)
        return anno

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def PerResidue(self):
        """Implementation of parent interface
        """
        return True


class PLIPStructAnno(StructAnno):
    """Implementation of StructAnno - Annotates Protein-ligand interactions

    Uses Protein Ligand Interaction Profiler (PLIP) as called in SWISS-MODEL.
    The advantage compared to simply calling the PLIP binary is better handling
    of huge assemblies by only processing the close environment of ligands and
    splitting the processed ligands into chunks for which PLIP is called
    separately. Furthermore, polymer ligands are identifed based on numbering.
    I.e. Several ligand residues in the same chain that share the same residue
    number but have distinct insertion codes are considered polymers. In the
    end, the PLIP XML output is translated into aJSON serializable dictionary.
    The big drawback: Assumes all ligands (and nothing else) to be in chain with
    name "_".

    :param sm_run_plip: SWISS-MODEL run-plip action, can in principle be the
                        path to the action in a full blown SWISS-MODEL
                        installation. However, the default points to the
                        location of a copy thereof in the var3d container.
    :type sm_run_plip: :class:`str`
    :param plip_bin: PLIP executable
    :type plip_bin: :class:`str`
    :param select_proximity: Distance in A that defines ligand environment that
                             is processed in PLIP.
    :type select_proximity: :class:`float`
    :param split: Number of ligands that are processed in one PLIP call
    :type split: :class:`int`
    :param key: Return value of parent class Key function
    :type key: :class:`str`
    """

    def __init__(
        self,
        sm_run_plip="/plip/sm-run-plip",
        plip_bin="/plip/plip/plipcmd.py",
        select_proximity=15.0,
        split=15,
        key="plip",
    ):
        if not os.access(sm_run_plip, os.X_OK):
            raise ValueError(
                f"Provided sm_run_plip action ({sm_run_plip}) "
                f"does not exist or is not executable"
            )
        if not os.access(plip_bin, os.X_OK):
            raise ValueError(
                f"Provided plip_bin ({plip_bin}) "
                f"does not exist or is not executable"
            )
        self.plip_wrapper = sm_run_plip
        self.plip_bin = plip_bin
        self.select_proximity = select_proximity
        self.split = split
        self.key = key

    def Run(self, sequence, structures, seq_range=None):
        """Implementation of parent interface
        """
        anno = list()
        for structure in structures:
            anno.append(self._GetPLIPJSON(structure))
        return anno

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def PerResidue(self):
        """Implementation of parent interface
        """
        return False

    def _GetPLIPJSON(self, structure):
        plip_json = None
        work_dir = tempfile.mkdtemp()
        name = structure.GetHash()
        plip_json_fn = os.path.join(work_dir, f"{name}.json")
        pdb_fn = os.path.join(work_dir, f"{name}.pdb")
        io.SavePDB(structure.assembly, pdb_fn)

        plip_cmd = self._GetPLIPCommand(pdb_fn, work_dir, name)
        ps = subprocess.run(
            plip_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        if ps.returncode != 0:
            err_msg = f"PLIP anno subprocess failed. Processed structure: "
            err_msg += f"{name}. {str(ps.stderr.decode())}"
            ost.LogError(err_msg)
        else:
            try:
                with open(plip_json_fn) as fh:
                    plip_json = json.load(fh)
            except (IOError, ValueError) as e:
                err_msg = f"PLIP anno subprocess failed. Processed structure: "
                err_msg += f"{name}. {str(e)}"
                ost.LogError(err_msg)
        shutil.rmtree(work_dir)
        return plip_json

    def _GetPLIPCommand(self, pdb_file, workdir, name):
        return [
            "ost",
            self.plip_wrapper,
            "-f",
            pdb_file,
            "--name",
            name,
            "-o",
            workdir,
            "-j",  # JSON output,
            "--select-proximity",
            str(self.select_proximity),
            "--split",
            str(self.split),
            "--plip-bin",
            self.plip_bin,
            "--nofixfile",  # Don't save the plipfixed*.pdb file
            "--nopdbcanmap",
        ]
