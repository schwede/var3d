import re
import urllib.request

from ost import seq


def ValidACPattern(ac):
    """Checks whether ac corresponds to what we expect from a uniprot AC.

    Only the pattern is checked, no check whether ac actually exists.

    :param ac: The accession code to check
    :type ac: :class:`str`
    :returns: Whether *ac* looks like a valid accession code
    :rtype: :class:`bool` 
    """
    # the following is a regex pattern which identifies uniport accession
    # codes stolen from https://www.uniprot.org/help/accession_numbers
    p = "[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}"
    if re.match(p, ac):
        return True
    return False


def FetchUniprotEntry(
    uniprot_ac, url="https://www.uniprot.org/uniprot", suffix="txt"
):
    """Fetches and returns uniprot entry in txt format
    
    :param uniprot_ac: Uniprot AC to fetch entry, will be checked with
                       :func:`ValidACPattern`
    :type uniprot_ac: :class:`str`
    :param url: Used for remote fetching, final url follows the pattern
                <url>/<uniprot_ac>.<suffix>. Given default values and uniprot AC
                P51480, that would be https://www.uniprot.org/uniprot/P51480.txt
    :type url: :class:`str`
    :param suffix: Don't touch
    :type suffix: :class:`str`
    :returns: The content of the entry
    :rtype: :class:`list` of :class:`str`
    :raises: :class:`ValueError` if *uniprot_ac* is no valid uniprot AC, url is
             not recognized as actual url or :class:`urllib.error.HTTPError` if
             http request fails.
    """
    if not ValidACPattern(uniprot_ac):
        raise ValueError(
            f"uniprot_ac ({uniprot_ac}) seems no " f"valid uniprot AC"
        )
    entry_url = f"{url}/{uniprot_ac}.{suffix}"
    with urllib.request.urlopen(entry_url) as response:
        return response.read().decode().splitlines()


def ParseUniprotSequence(uniprot_entry):
    """Parses the canonical sequence from a uniprot entry in txt format

    :param uniprot_entry: Uniprot entry in txt format, for example fetched with
                          :func:`FetchUniprotEntry`
    :type uniprot_entry: :class:`list` of :class:`str`
    :returns: Canonical sequence, None if not found
    :rtype: :class:`str`
    """
    n_AA = None
    in_seq = False
    canonical_seq = None
    for line in uniprot_entry:
        if in_seq:
            if line[:2] != "  ":
                in_seq = False  # done reading sequence
            else:
                canonical_seq += line.strip().replace(" ", "")
        if line.startswith("SQ   "):
            if canonical_seq is not None:
                raise RuntimeError("Expect one canonical seq per uniprot entry")
            canonical_seq = ""
            in_seq = True
            n_AA = int(line.split()[2])
    if canonical_seq is None:
        return None
    if n_AA != len(canonical_seq):
        raise RuntimeError(
            f"Uniprot entry specifies canonical sequence length "
            f"{n_AA}, read {len(canonical_seq)}"
        )
    return canonical_seq


class FeatureTable:
    """Feature Table representation from FT lines in uniprot txt format

    :param ft_type: FT type, e.g. VARIANT
    :type ft_type: :class:`str`
    :param ft_start: Start of feature location, potentially None ('?' is allowed
                  in uniprot format)
    :type ft_start: :class:`int`
    :param ft_end: End of feature location, potentially None (see start), if the
                feature describes a single position, you can set end = start
    :type ft_end: :class:`int`
    :param note_qualifier: Information stored in 'note' qualifier
    :type note_qualifier: :class:`str`
    :param evidence_qualifier: Information stored in 'evidence' qualifier
    :type evidence_qualifier: :class:`str` 
    :param id_qualifier: Information stored in 'id' qualifier
    :type id_qualifier: :class:`str`
    :param isoform_id: Optional, None if FT refers to canonical sequence
    :type isoform_id: :class:`str`
    :param ft_start_beyond: Start position is optionally preceded with '<' 
                            indicating that start point is beyond ft_start
    :type ft_start_beyond: :class:`bool`
    :param ft_end_beyond: End position is optionally preceded with '>' 
                          indicating that end point is beyond ft_end
    :type ft_end_beyond: :class:`bool`
    """

    def __init__(
        self,
        ft_type,
        ft_start,
        ft_end,
        note_qualifier="",
        evidence_qualifier="",
        id_qualifier="",
        isoform_id=None,
        ft_start_beyond=False,
        ft_end_beyond=False,
    ):
        self.ft_type = ft_type
        self.ft_start = ft_start
        self.ft_end = ft_end
        self.note_qualifier = note_qualifier
        self.evidence_qualifier = evidence_qualifier
        self.id_qualifier = id_qualifier
        self.isoform_id = isoform_id
        self.ft_start_beyond = ft_start_beyond
        self.ft_end_beyond = ft_end_beyond

    @staticmethod
    def FromLines(lines):
        """Reads FT from lines in uniprot txt format

        :param lines: Lines from uniprot txt file defining one feature table
        :type lines: :class:`list` of :class:`str`
        :returns: :class:`FeatureTable`
        """
        if len(lines) == 0:
            raise RuntimeError("Cannot parse feature table from no lines")

        # parse header
        split_header = lines[0].split()

        if len(split_header) != 3:
            raise RuntimeError(
                "Expect feature table header to contain 3 "
                "components: FT, type, location"
            )

        if split_header[0] != "FT":
            raise RuntimeError("Expect feature table header to start with FT")

        ft_type = split_header[1]

        isoform_id = None
        if ":" in split_header[2]:
            split_location = split_header[2].split(":")
            isoform_id = split_location[0]  # TODO: testing
            split_header[2] = split_location[1]

        ft_start = None
        ft_end = None
        ft_start_beyond = False
        ft_end_beyond = False
        if ".." in split_header[2]:
            # It's a range!
            ft_start = split_header[2].split("..")[0]
            if ft_start.startswith("<"):
                ft_start_beyond = True
                ft_start = ft_start.strip("<")
            if ft_start != "?":
                ft_start = int(ft_start)
            ft_end = split_header[2].split("..")[1]
            if ft_end.startswith(">"):
                ft_end_beyond = True
                ft_end = ft_end.strip(">")
            if ft_end != "?":
                ft_end = int(ft_end)
        else:
            ft_start = split_header[2]
            if ft_start.startswith("<"):
                ft_start_beyond = True
                ft_start = ft_start.strip("<")
            if ft_start != "?":
                ft_start = int(ft_start)
            ft_end = ft_start

        # to parse the qualifers, all lines are concatenated (FT prefix removed)
        ft_string = " ".join([l[2:].strip() for l in lines[1:]])
        note_qualifier = FeatureTable._ParseQualifier(ft_string, "note")
        evidence_qualifier = FeatureTable._ParseQualifier(ft_string, "evidence")
        id_qualifier = FeatureTable._ParseQualifier(ft_string, "id")

        ft = FeatureTable(
            ft_type,
            ft_start,
            ft_end,
            note_qualifier=note_qualifier,
            evidence_qualifier=evidence_qualifier,
            id_qualifier=id_qualifier,
            isoform_id=isoform_id,
            ft_start_beyond=ft_start_beyond,
            ft_end_beyond=ft_end_beyond,
        )
        return ft

    @staticmethod
    def _ParseQualifier(ft_string, qualifier):
        """parses a qualifier that supplies information for a feature,
        e.g. /note="Q -> H (in strain: A23.1)" /evidence="ECO:0000305" 
        have the following two qualifiers: note and evidence
        This function searches for *qualifier* in *ft_string* and returns
        the subsequent content that is enclosed in \"
        
        :param ft_string: All ft info as single string without the FT 
                          prefixes
        :type ft_string: :class:`str`
        :param qualifier: Qualifer to extract, e.g. note
        :returns: :class:`str` with info for *qualifier*, empty string if 
                  *qualifier* could not be found.
        """
        # [^=] means every character except = to make sure that next qualifier
        # is not included
        re_str = f'/{qualifier}="[^=]+"'
        m = re.search(re_str, ft_string)
        if m is None:
            return ""
        return m.group(0).split("=")[1].strip('"').strip()


def ParseUniprotFT(uniprot_entry, ft_type):
    """Parses feature tables (FT) from uniprot entry in txt format

    A feature table starts with a header line of the form:

    FT   <ft_type>          <Location>

    With optionally several subsequent lines referring to that header,
    i.e. Lines starting with FT but no <ft_type>. E.g.:

    FT                   /note="Phosphothreonine"

    This function reads and returns all feature tables of a certain type. 

    :param uniprot_entry: Uniprot entry in txt format, for example fetched with
                          :func:`FetchUniprotEntry`
    :type uniprot_entry: :class:`list` of :class:`str`
    :param ft_type: E.g. VARIANT
    :type ft_type: :class:`str`
    :returns: The feature tables of specified type
    :rtype: :class:`list` of :class:`FeatureTable`
    """
    ft_lines = list()
    in_ft = False
    current_ft_lines = list()
    for line in uniprot_entry:
        if in_ft:
            if not line.startswith("FT          "):
                in_ft = False  # done reading ft
                if len(current_ft_lines) > 0:
                    ft_lines.append(current_ft_lines)
                current_ft_lines = list()
            else:
                current_ft_lines.append(line.strip())
        if line.startswith("FT") and line.split()[1] == ft_type:
            # finish previous ft
            if len(current_ft_lines) > 0:
                ft_lines.append(current_ft_lines)
                current_ft_lines = list()
            in_ft = True
            current_ft_lines.append(line.strip())
    # still ft data left?
    if len(current_ft_lines) > 0:
        ft_lines.append(current_ft_lines)
    feature_tables = [FeatureTable.FromLines(lines) for lines in ft_lines]
    return feature_tables


def ParseRecommendedNames(uniprot_entry):
    """Parses Full recommended name and, if available, the Short version

    Searches for the first occurence of header line "DE   RecName", falls
    back to "DE   SubName" if not available. Reads the names without evidence 
    in curly brackets

    :param uniprot_entry: Uniprot entry in txt format, for example fetched with
                          :func:`FetchUniprotEntry`
    :type uniprot_entry: :class:`list` of :class:`str`
    :returns: Tuple with two strings, first is Full name second Short name.
              Short name is None if not found
    :rtype: :class:`tuple`
    :raises:
    """
    name_lines = list()
    in_names = False
    for line in uniprot_entry:
        if in_names:
            if not line.startswith("DE          "):
                break  # only first occurence...
            name_lines.append(line)
        if line.startswith("DE   ") and line.split()[1].startswith("RecName"):
            in_names = True
            name_lines.append(line)
        if line.startswith("DE   ") and line.split()[1].startswith("SubName"):
            in_names = True
            name_lines.append(line)

    full_name = None
    short_name = None
    for line in name_lines:
        if "Full=" in line:
            full_name = line.split("Full=")[1].strip("; ")
        if "Short=" in line:
            short_name = line.split("Short=")[1].strip("; ")
    if full_name is None:
        raise RuntimeError("Uniprot entry has not recommended full name")
    # remove stuff between curly brackets
    full_name = re.sub("\{.*\}", "", full_name).strip()
    if short_name is not None:
        short_name = re.sub("\{.*\}", "", short_name).strip()

    return (full_name, short_name)


def GetUniprotSequence(uniprot_ac, url="https://www.uniprot.org/uniprot"):
    """Fetches uniprot entry, extracts the sequence and returns it

    :param uniprot_ac: Uniprot AC for which you want the sequence
    :type uniprot_ac: :class:`str`
    :param url: Used for remote fetching, final url follows the pattern
                <url>/<uniprot_ac>.txt. Given default values and uniprot AC
                P51480, that would be https://www.uniprot.org/uniprot/P51480.txt
    :type url: :class:`str`
    :returns: The fetched sequence with *uniprot_ac* as name
    :rtype: :class:`ost.seq.SequenceHandle`
    :raises: see :func:`FetchUniprotEntry`
    """
    uniprot_entry = FetchUniprotEntry(uniprot_ac, url=url)
    s = ParseUniprotSequence(uniprot_entry)
    return seq.CreateSequence(uniprot_ac, s)
