import os
import subprocess
import tempfile
import shutil
import stat
import copy
import pandas as pd
from sklearn.cluster import AgglomerativeClustering
from scipy.stats import hypergeom
import ost
from ost import io
from var3d.base import *


class FoldXVarStructAnno(VarStructAnno):
    """Implementation of VarStructAnno - Variant annotation based on FoldX.

    Delgado J, Radusky LG, Cianferoni D, Serrano L FoldX 5.0: working with RNA,
    small molecules and a new graphical interface. Bioinformatics (2019)

    Calls the FoldX BuildModel command and annotates variants as difference
    in requested FoldX scores between wild-type and mutant, i.e. as dicts with
    score names as keys and the respective score differences as values. Only
    annotates variants that are covered by the input structure(s) and that are
    of type :class:`var3d.base.VariantType.SUBSTITUTION`. Everything else is set
    to None.

    .. warning::
        Annotations are score differences between reference and variant. 
        Reference in this case is given by the structural residue at the variant
        location. This is usually the same as the variant reference but there is
        no guarantee for that (except you use *strict* when constructing the
        corresponding :class:`var3d.base.Structure` objects).

    :param foldx_exe: Path to FoldX executable
    :type foldx_exe: :class:`str`
    :param key: Return value of parent class Key function, default: "foldx"
    :type key: :class:`str`
    :param tmp_dir: Directory to dump input for the foldx executable. Will be
                    created (and cleaned up) if not provided.
    :type tmp_dir: :class:`str`
    :param foldx_scores: Scores you want as annotations. Each annotation is a
                         :class:`dict` with keys which can be anything 
                         that FoldX spits out (FoldX 5.0: "total energy", 
                         "Backbone Hbond", "Sidechain Hbond", "Van der Waals",
                         "Electrostatics", "Solvation Polar", 
                         "Solvation Hydrophobic", "Van der Waals clashes", 
                         "entropy sidechain", "entropy mainchain", 
                         "sloop_entropy", "mloop_entropy", "cis_bond",
                         "torsional clash", "backbone clash", 
                         "helix dipole", "water bridge", "disulfide", 
                         "electrostatic kon", "partial covalent bonds",
                         "energy Ionisation", "Entropy Complex"). Parsing
                         fails if you request anything that doesn't exist in the
                         output file.
    :param num_runs: Passed as numberOfRuns parameter to FoldX BuildModel
    :type num_runs: :class:`int`
    """

    def __init__(
        self,
        foldx_exe="/foldx/foldx",
        key="foldx",
        tmp_dir=None,
        foldx_scores=[
            "total energy",
            "Van der Waals",
            "Electrostatics",
            "energy Ionisation",
        ],
        num_runs=5,
    ):

        if not os.path.exists(foldx_exe):
            raise ValueError("Provided foldx_exe does not exist")

        if not os.path.basename(foldx_exe) == "foldx":
            raise ValueError("FoldX executable must be called foldx")
        self.foldx_exe = foldx_exe
        self.key = key
        if tmp_dir:
            if not os.path.exists(tmp_dir):
                os.makedirs(tmp_dir)
            self.tmp_dir = tmp_dir
        else:
            # as soon as self.tmp_dir_context is garabage collected,
            # the directory is deleted on disk
            self.tmp_dir_context = tempfile.TemporaryDirectory()
            self.tmp_dir = self.tmp_dir_context.name
        self.foldx_scores = foldx_scores
        self.num_runs = num_runs

    def Key(self):
        """Implementation of parent interface"""
        return self.key

    def Run(self, sequence, variants, structures):
        """Implementation of parent interface
        """
        # Parent expects results on a variant first basis. For practical
        # reasons we do the computation on a structure first basis
        # and perform mapping afterwards
        annotations = [list() for v in variants]
        for s in structures:
            structure_annotations = self._ProcessVariants(variants, s)
            for variant_idx, anno in enumerate(structure_annotations):
                annotations[variant_idx].append(anno)
        return annotations

    def _ProcessVariants(self, variants, structure):
        """Calculates annotations for all variants given structure

        Returns a list of length len(variants) with all non-relevant variants
        set to None. A relevant variant is covered by structure and of type 
        SUBSTITUTION.
        """
        relevant_variants_idx = list()
        foldx_encodings = list()
        for v_idx, v in enumerate(variants):
            if v.variant_type == "SUBSTITUTION":
                variant_encodings = list()
                for segment_idx in range(structure.GetNumSegments()):
                    segment_aln = structure.aln[segment_idx]
                    col_idx = segment_aln.GetSequence(0).GetPos(v.pos - 1)
                    col = segment_aln[col_idx]
                    if col[1] != "-":
                        res = col.GetResidue(1)
                        rnum = res.GetNumber().GetNum()
                        cname = res.GetChain().GetName()
                        encoding = f"{res.one_letter_code}{cname}{rnum}{v.alt}"
                        if encoding not in variant_encodings:
                            variant_encodings.append(encoding)
                if len(variant_encodings) > 0:
                    relevant_variants_idx.append(v_idx)
                    foldx_encodings.append(",".join(variant_encodings) + ";")
        annotations = [None] * len(variants)
        if len(relevant_variants_idx) > 0:
            try:
                foldx_result = self._RunFoldX(
                    foldx_encodings, structure.assembly
                )
            except Exception as e:
                foldx_result = [None] * len(relevant_variants_idx)
                err_msg = f"FoldX anno subprocess failed. Processed structure "
                err_msg += f"{structure.GetHash()}, variants: "
                err_msg += f"{[v.GetHash() for v in variants]}. {str(e)}"
                ost.LogError(err_msg)
            assert len(foldx_result) == len(relevant_variants_idx)
            for idx, anno in zip(relevant_variants_idx, foldx_result):
                annotations[idx] = anno
        return annotations

    def _RunFoldX(self, foldx_encodings, assembly):
        """Returns annotations for each element in foldx_encoding given assembly
        
        One annotation is a dict with requested keys (self.foldx_values). 
        Computation happens in a separate directory in *tmp_dir*. Dir names are
        ascending numbers (0,1,2,3...). So you get one directory per FoldX run.
        The following files are stored in there: FoldX executable, PDB file
        representing the full assembly and a FoldX readable file defining the
        variants. FoldX executable is deleted upon completion.
        """
        # PREPARE
        #########
        workdir = self._Setup(foldx_encodings, assembly)
        cmd = [
            "./foldx",
            "--command=BuildModel",
            "--pdb=assembly.pdb",
            "--mutant-file=individual_list.txt",
            "--complexWithDNA=true",
            f"--numberOfRuns={self.num_runs}",
            "--out-pdb=false",
        ]

        # RUN
        #####
        p = subprocess.run(
            cmd, cwd=workdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        if p.returncode != 0:
            raise RuntimeError(
                f"stdout: {p.stdout.decode()}, stderr: " f"{p.stderr.decode()}"
            )

        # PARSE AND RETURN
        ##################
        os.remove(os.path.join(workdir, "foldx"))
        return self._Parse(workdir)

    def _Setup(self, foldx_encodings, assembly):
        dir_idx = 0
        workdir = None
        while True:
            wd = os.path.join(self.tmp_dir, str(dir_idx))
            if not os.path.exists(wd):
                os.makedirs(wd)
                workdir = wd
                break
            dir_idx += 1
        # copy executable
        shutil.copy(self.foldx_exe, workdir)
        foldx_exe = os.path.join(workdir, "foldx")
        st = os.stat(foldx_exe)
        os.chmod(foldx_exe, st.st_mode | stat.S_IEXEC)
        # dump full assembly
        io.SavePDB(assembly, os.path.join(workdir, "assembly.pdb"))
        # generate FoldX input file
        with open(os.path.join(workdir, "individual_list.txt"), "w") as fh:
            fh.write("\n".join(foldx_encodings))
        return workdir

    def _Parse(self, workdir):
        with open(os.path.join(workdir, "Average_assembly.fxout"), "r") as fh:
            data = fh.readlines()
        annotations = list()
        hot = False
        line_idx = 0
        score_mapper = dict()
        while line_idx < len(data):
            if "BuildModel" in data[line_idx]:
                # parse header
                split_header = data[line_idx + 1].split("\t")
                for score_idx, score in enumerate(split_header):
                    score_mapper[score] = score_idx
                # check whether we have all indices we need
                for score in self.foldx_scores:
                    if score not in score_mapper:
                        raise RuntimeError(
                            f"Requested score ({score}) not "
                            f"in FoldX output. There is"
                            f"{score_mapper.keys()}"
                        )
                hot = True
                line_idx += 2
                continue
            if not hot:
                line_idx += 1
                continue
            annotation = dict()
            split_line = data[line_idx].split("\t")
            for score in self.foldx_scores:
                annotation[score] = float(split_line[score_mapper[score]])
            annotations.append(annotation)
            line_idx += 1
        return annotations


class EnvironmentVarStructAnno(VarStructAnno):
    """Implementation of VarStructAnno - structural environment of variants

    Gets the structural environment around the variant residues using OST

    :param search_distance: distance in angstrom around which other variants are
                            searched, default 5 angstrom
    :type search_distance: :class:`int`
    """

    def __init__(self, search_distance=5, key="environment"):

        self.search_distance = search_distance
        self.key = key

    def Key(self):
        """Implementation of parent interface"""
        return self.key

    def Run(self, sequence, variants, structures):
        """Implementation of parent interface
        """
        # Parent expects results on a variant first basis. For practical
        # reasons we do the computation on a structure first basis
        # and perform mapping afterwards

        annotations = [list() for v in variants]
        for s in structures:
            structure_annotations = self._ProcessVariants(variants, s)
            for variant_idx, anno in enumerate(structure_annotations):
                annotations[variant_idx].append(anno)
        return annotations

    def _ProcessVariants(self, variants, structure):

        ent = structure.assembly

        variants_idx = list()
        environment_encodings = list()
        for v_idx, v in enumerate(variants):
            variant_encodings = list()
            for segment_idx in range(structure.GetNumSegments()):
                segment_aln = structure.aln[segment_idx]
                col_idx = segment_aln.GetSequence(0).GetPos(v.pos - 1)
                col = segment_aln[col_idx]
                if col[1] != "-":
                    res = col.GetResidue(1)
                    rnum = res.GetNumber().GetNum()
                    cname = res.GetChain().GetName()
                    encoding = {
                        "ref": res.one_letter_code,
                        "chain": cname,
                        "pos": str(rnum),
                        "alt": v.alt,
                    }
                    if encoding not in variant_encodings:
                        variant_encodings.append(encoding)
            if len(variant_encodings) > 0:
                variants_idx.append(v_idx)
                environment_encodings.append(variant_encodings)

        # get environments
        annotations = [None] * len(variants)
        for i, v in enumerate(environment_encodings):
            cur_v = v[0]
            v_ent = ent.Select(
                str(self.search_distance)
                + " <> [cname="
                + cur_v["chain"]
                + " and rnum="
                + cur_v["pos"]
                + "]"
            )
            dict_environment = self._AnalyseEnvironment(
                v_ent, cur_v, variants, structure
            )
            annotations[variants_idx[i]] = {
                "environment_analysis": dict_environment
            }

        return annotations

    def _AnalyseEnvironment(self, env, cur_v, variants, structure):
        """
        Find out what the environment of this particular variant position contains
        Returns dict with the following key-value pairs

        {"nucleotides" : {"nucleotide_in_env" : [boolean]} 
         "ligands" : {"ligand_in_env" : [boolean],
                      "ligand_names" : [list of str]}
         "drug" : {"drug_in_env" : [boolean], "drug_names" : [list of str]}
         "interface" : {"interface_homomer" : [boolean],
                        "interface_heteromer" : [boolean]}
         "variants" : {"#resvar_in_env" : [int], "#neutvar_in_env" : [int],
                       "#resvarpos_in_env" : [int],
                       "#neutvarpos_in_env" : [int],
                       "hypergeom_res" : [float],
                       "hypergeom_neut" : [float],
                       "hypergeom_respos" : [float],
                       "hypergeom_neutpos" : [float]}
        """

        dict_out = {}
        # nucleotides
        if sum([res.chem_type.IsNucleotide() for res in env.residues]) > 0:
            dict_out["nucleotides"] = {"nucleotide_in_env": True}
        else:
            dict_out["nucleotides"] = {"nucleotide_in_env": False}

        # ligand
        if "_" in [res.chain.name for res in env.residues]:
            ligand_chain = env.FindChain("_")
            dict_out["ligands"] = {
                "ligand_in_env": True,
                "ligand_names": [x.name for x in ligand_chain.residues],
            }
        else:
            dict_out["ligands"] = {"ligand_in_env": False, "ligand_names": []}

        # drug
        drug_positions = structure.meta["res_num"]
        # only one kind of drug per structure, or none

        # search for drug residue in env
        dict_out["drugs"] = {"drug_in_env": False}
        for chain in env.chains:
            if "_" == chain.name:
                cur_pos = [str(r.number) for r in chain.residues]
                for pos in cur_pos:
                    if pos in drug_positions:
                        dict_out["drugs"] = {"drug_in_env": True}

        # interface
        dict_out["interface"] = {
            "interface_homomer": False,
            "interface_heteromer": False,
        }
        # get chain names corresponding to current uniprot ID
        chain_names_homomer = [x.split("=")[1] for x in structure.segment_query]
        # get chain names in env which are not ligands or water
        chain_names_env = [
            x.name for x in env.chains if x.name != "-" and x.name != "_"
        ]
        # you need at least two chains from the homomer list to count for a
        # homomer interface
        if len(set(chain_names_env).intersection(set(chain_names_homomer))) > 1:
            dict_out["interface"]["interface_homomer"] = True
        # you need at least one chain which is not in the chain_names_homomer
        # list to count for a heteromer interface
        if len(set(chain_names_homomer).difference(set(chain_names_env))) > 1:
            dict_out["interface"]["interface_heteromer"] = True

        # variants
        # get the position of resistant variants and neutral variants
        resvar_pos = [
            var.pos
            for var in variants
            if var.meta["variant_label"][0] == "resistant"
        ]
        neutvar_pos = [
            var.pos
            for var in variants
            if var.meta["variant_label"][0] == "neutral"
        ]
        # get residue numbers in env
        resnums = [
            res.number.num
            for res in env.residues
            if res.chain.name in chain_names_homomer
        ]
        dict_out["#resvar_in_env"] = len(
            [x for x in resnums if x in resvar_pos]
        )
        dict_out["#neutvar_in_env"] = len(
            [x for x in resnums if x in neutvar_pos]
        )
        dict_out["#resvarpos_in_env"] = len(
            set(resvar_pos).intersection(resnums)
        )
        dict_out["#neutvarpos_in_env"] = len(
            set(neutvar_pos).intersection(resnums)
        )

        # perform hypergeometric testing
        """
        M: population size, total number of residues in structure
        n: number of annotated variants in structure
        N: number of residues in environment
        X: number of annotated variants in environment
        """
        M = sum(
            [
                len(x.residues)
                for x in structure.assembly.chains
                if x.name in chain_names_homomer
            ]
        )
        N = sum(
            [
                len(x.residues)
                for x in env.chains
                if x.name in chain_names_homomer
            ]
        )
        # do it for all variants
        dict_out["hypergeomtest_resvar"] = hypergeom.sf(
            dict_out["#resvar_in_env"] - 1, M, len(resvar_pos), N
        )
        dict_out["hypergeomtest_resvarpos"] = hypergeom.sf(
            dict_out["#resvarpos_in_env"] - 1, M, len(set(resvar_pos)), N
        )
        dict_out["hypergeomtest_neutvar"] = hypergeom.sf(
            dict_out["#neutvar_in_env"] - 1, M, len(neutvar_pos), N
        )
        dict_out["hypergeomtest_neutvarpos"] = hypergeom.sf(
            dict_out["#neutvarpos_in_env"] - 1, M, len(set(neutvar_pos)), N
        )

        return dict_out


class ClusteringVarStructAnno(VarStructAnno):
    """Implementation of VarStructAnno - Variant annotation based on Variant
    Clustering

    We identify regions of connected positions on the protein structure by
    searching in a given Angstrom radius around each residue carrying variants
    to find other variations After each neighbour is identified, we create
    groups of connected variants, each group is a cluster

    :param search_distance: distance in angstrom around which other variants
                            are searched, default 5 angstrom
    :type search_distance: :class:`int`
    :param common_neighbour: number of common neighbours two residues in
                             contacts need to have in order to count as in
                             contact, default 1 neighbour
    :type common_neighbour: :class:`int`
    :param key: Return value of parent class Key function, default: "clustering"
    :type key: :class:`str`
    """

    def __init__(self, search_distance=5, key="clustering"):

        self.search_distance = search_distance
        self.key = key

    def Key(self):
        """Implementation of parent interface"""
        return self.key

    def Run(self, sequence, variants, structures):
        """Implementation of parent interface
        """
        # Parent expects results on a variant first basis. For practical
        # reasons we do the computation on a structure first basis
        # and perform mapping afterwards

        annotations = [list() for v in variants]
        for s in structures:
            structure_annotations = self._ProcessVariants(variants, s)
            for variant_idx, anno in enumerate(structure_annotations):
                annotations[variant_idx].append(anno)
        return annotations

    def _ProcessVariants(self, variants, structure):
        """Calculates clusters of variants on structures
        We get the 3d coordinates using OST and then use SKlearn hierarchical
        clustering to get the clusters in the data set
        """

        ent = structure.assembly

        relevant_variants_idx = list()
        clustering_encodings = list()
        for v_idx, v in enumerate(variants):
            variant_encodings = list()
            for segment_idx in range(structure.GetNumSegments()):
                segment_aln = structure.aln[segment_idx]
                col_idx = segment_aln.GetSequence(0).GetPos(v.pos - 1)
                col = segment_aln[col_idx]
                if col[1] != "-":
                    res = col.GetResidue(1)
                    rnum = res.GetNumber().GetNum()
                    cname = res.GetChain().GetName()
                    encoding = {
                        "ref": res.one_letter_code,
                        "chain": cname,
                        "pos": str(rnum),
                        "alt": v.alt,
                    }
                    if encoding not in variant_encodings:
                        variant_encodings.append(encoding)
            if len(variant_encodings) > 0:
                relevant_variants_idx.append(v_idx)
                clustering_encodings.append(variant_encodings)

        variant_coordinates = []
        for v in clustering_encodings:
            cur_v = v[0]
            v_ent = ent.Select(
                "cname=" + cur_v["chain"] + " and rnum=" + cur_v["pos"]
            )
            assert len(v_ent.residues) == 1
            v_res = v_ent.residues[0]
            variant_coordinates.append(v_res.GetCenterOfMass())

        variant_coordinates = [list(x) for x in variant_coordinates]
        data = pd.DataFrame(variant_coordinates)
        # transform variant_coordinates to a dataframe
        # clustering estimator
        model = AgglomerativeClustering(
            distance_threshold=self.search_distance, n_clusters=None
        )
        model = model.fit(data)

        annotations = [[{"cluster_id": int(x)}] for x in model.labels_]

        return annotations
