from ost import geom

_TLC_OLC = {
    "CYS": "C",
    "ASP": "D",
    "SER": "S",
    "GLN": "Q",
    "LYS": "K",
    "ILE": "I",
    "PRO": "P",
    "THR": "T",
    "PHE": "F",
    "ASN": "N",
    "GLY": "G",
    "HIS": "H",
    "LEU": "L",
    "ARG": "R",
    "TRP": "W",
    "ALA": "A",
    "VAL": "V",
    "GLU": "E",
    "TYR": "Y",
    "MET": "M",
    "*": "*",
    "TER": "*",
    "DUP": "DUP",
    "?": "?",
}


def _CompareAtoms(a1, a2):
    """Helper for CompareEnt
    """
    if abs(a1.occupancy - a2.occupancy) > 0.01:
        return False
    if abs(a1.b_factor - a2.b_factor) > 0.01:
        return False
    if geom.Distance(a1.GetPos(), a2.GetPos()) > 0.001:
        return False
    if a1.is_hetatom != a2.is_hetatom:
        return False
    if a1.element != a2.element:
        return False
    return True


def _CompareResidues(r1, r2):
    """Helper for CompareEnt
    """
    if r1.GetName() != r2.GetName():
        return False
    if r1.GetNumber() != r2.GetNumber():
        return False
    if str(r1.GetSecStructure()) != str(r2.GetSecStructure()):
        return False
    if r1.one_letter_code != r2.one_letter_code:
        return False
    if r1.chem_type != r2.chem_type:
        return False
    if r1.chem_class != r2.chem_class:
        return False
    anames1 = [a.GetName() for a in r1.atoms]
    anames2 = [a.GetName() for a in r2.atoms]
    if sorted(anames1) != sorted(anames2):
        return False
    anames = anames1
    for aname in anames:
        a1 = r1.FindAtom(aname)
        a2 = r2.FindAtom(aname)
        if not _CompareAtoms(a1, a2):
            return False
    return True


def _CompareChains(ch1, ch2):
    """Helper for CompareEnt
    """
    if len(ch1.residues) != len(ch2.residues):
        return False
    for r1, r2 in zip(ch1.residues, ch2.residues):
        if not _CompareResidues(r1, r2):
            return False
    return True


def _CompareBonds(ent1, ent2):
    """Helper for CompareEnt
    """
    bonds1 = list()
    for b in ent1.bonds:
        bond_partners = [str(b.first), str(b.second)]
        bonds1.append([min(bond_partners), max(bond_partners), b.bond_order])
    bonds2 = list()
    for b in ent2.bonds:
        bond_partners = [str(b.first), str(b.second)]
        bonds2.append([min(bond_partners), max(bond_partners), b.bond_order])
    return sorted(bonds1) == sorted(bonds2)


def CompareEnt(ent1, ent2):
    """Compares two entities for equality.
 
    * Entity Level: same number of chains with equal names
    * Chain Level: same number of residues with equal ordering
    * Residue Level: Name, ResNum, ChemType, ChemClass, SecStructure, OLC,
                     same number of atoms with equal names, but no specific
                     ordering required
    * Atom Level: Occupancy, BFactor, Position, IsHetAtm, Element
    * Bond Level: Same number of bonds that connect the same atoms with same
                  bond order


    :param ent1: First entity to compare
    :type ent1: :class:`ost.mol.EntityHandle`/:class:`ost.mol.EntityView`
    :param ent2: Second entity to compare
    :type ent2: :class:`ost.mol.EntityHandle`/:class:`ost.mol.EntityView`
    :returns: True, if *ent1* and *ent2* match in all criteria, False otherwise
    :rtype: :class:`bool`
    """
    chain_names_one = [ch.GetName() for ch in ent1.chains]
    chain_names_two = [ch.GetName() for ch in ent2.chains]
    if not sorted(chain_names_one) == sorted(chain_names_two):
        return False
    chain_names = chain_names_one
    for chain_name in chain_names:
        ch1 = ent1.FindChain(chain_name)
        ch2 = ent2.FindChain(chain_name)
        if not _CompareChains(ch1, ch2):
            return False
    if not _CompareBonds(ent1, ent2):
        return False
    return True


def CheckRange(sequence, seq_range):
    """Checks whether *seq_range* describes a stretch in *sequence* 

    Raises descriptive :class:`RuntimeError` if not.

    :param sequence: Canonical sequence the variants refer to 
    :type sequence: :class:`ost.seq.SequenceHandle` /
                    :class:`ost.seq.ConstSequenceHandle`
    :param range: Range you want to check, something in form [from, to]
                  with 1-based indexing
    :type range: :class:`tuple`/:class:`list` of :class:`int`
    """
    if seq_range is not None:
        if len(seq_range) != 2:
            raise RuntimeError("Expect length of seq_range to be 2")
        if seq_range[0] > seq_range[1]:
            raise RuntimeError(
                "Expect first element in seq_range to be "
                "smaller or equal than second"
            )
        if seq_range[0] < 1:
            raise RuntimeError("Expect first element in seq_range to be >= 1")
        if seq_range[1] > len(sequence):
            raise RuntimeError(
                "Expect second element in seq_range to be "
                "smaller or equal sequence length"
            )


def TLCToOLC(tlc):
    if tlc.upper() not in _TLC_OLC:
        raise RuntimeError(f"Cannot process {tlc} three letter code")
    return _TLC_OLC[tlc.upper()]
