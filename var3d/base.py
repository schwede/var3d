from enum import Enum
import base64
import gzip
import json
import hashlib
import re

from ost import seq
from ost import mol
from ost import io

from var3d import common


class VariantType(str, Enum):
    """Possible types of variants"""

    SUBSTITUTION = "SUBSTITUTION"  #: Amino acid substitution
    INSERTION = "INSERTION"  #: Insertion of 1 or more amino acids
    DELETION = "DELETION"  #: Deletion of 1 or more amino acids
    INDEL = "INDEL"  #: INSERTION and DELETION combined
    STOP_GAINED = (
        "STOP_GAINED"
    )  #: Special case of substitution, leading to stop codon
    STOP_LOST = "STOP_LOST"  #: Special case of substitution, stop codon gone
    START_LOST = (
        "START_LOST"
    )  #: Special case of substitution killing start codon
    FRAMESHIFT = "FRAMESHIFT"  #: Any event that leads to a frameshift
    SYNONYMOUS = (
        "SYNONYMOUS"
    )  #: Variant on nucleotide level with no effect on amino acid identity


class Variant:
    """A variant definition

    Adds constructor args as class attributes after checking, so you can access:

    * ref 
    * alt
    * pos
    * variant_type
    * meta

    *ref*, *alt*, *pos* and *variant_type* define the actual variant, while meta
    allows to decorate it with arbitrary annotations using :func:`AddMetaData`. 
    HASHES AND EQUALITY OPERATIONS DO NOT DEPEND ON META!!!

    :param ref: Canonical sequence before the variant is applied. Depends on
                *variant_type*. In an INSERTION *ref* must be a string of 
                length 1 and corresponds to the one letter code before the 
                insertion event. 
                In a DELETION/INDEL on the other hand *ref* can be any 
                :class:`str` of length >= 1 and corresponds to the 
                characters in the canonical sequence that are removed. 
                In case of SUBSTITUTION, STOP_GAINED, 
                STOP_LOST, START_LOST, FRAMESHIFT, SYNONYMOUS the 
                :class:`str` must have length 1. Constructor checks for correct
                type.
    :type ref: :class:`str`
    :param alt: Sequence after the variant is applied. Depends on
                *variant_type*. In a DELETION, *alt* must be "". In an 
                INSERTION, *alt* contains the character in the canonical 
                sequence before the insertion followed by the inserted
                characters. 
                In an INDEL on the other hand, *alt* corresponds to any
                :class:`str` with length >= 1 corresponding to the
                characters that are added. In case of SUBSTITUTION, STOP_GAINED, 
                STOP_LOST, START_LOST, FRAMESHIFT, SYNONYMOUS the :class:`str`
                must have length 1. Constructor checks for correct type.
    :type alt: :class:`str`
    :param pos: Position of variant in canonical sequence using 1 based 
                indexing, i.e. first character is 1.
                If *variant_type* is INSERTION, *pos* describes the position 
                before the newly added amino acid.
                If *variant_type* is DELETION, *pos* describes the first 
                position that is deleted.
                If *variant_type* is INDEL, *pos* describes the first 
                altered position. In all other cases *pos* describes the 
                location where stuff happens. pos must be >= 1.
                Constructor checks for correct type.
    :type pos: :class:`int`
    :param variant_type: Defines type of variant
    :type variant_type: :class:`VariantType`
    """

    def __init__(self, ref, alt, pos, variant_type):
        # check types
        if not isinstance(ref, str):
            raise TypeError(
                "ref param in Variant constructor is expected to "
                "be of type str"
            )

        if not isinstance(alt, str):
            raise TypeError(
                "alt param in Variant constructor is expected to "
                "be of type str"
            )

        if not isinstance(pos, int):
            raise TypeError(
                "pos param in Variant constructor is expected to "
                "be of type int"
            )

        if not isinstance(variant_type, VariantType):
            raise TypeError(
                "variant_type param in Variant constructor is "
                "expected to be of type sm.var3d.VariantType"
            )

        # check whether alt/ref parameters have expected length given
        # variant_type
        if variant_type == VariantType.INSERTION:
            if len(ref) != 1:
                raise ValueError(
                    "ref is expected to be of length 1 in case of " "INSERTION"
                )
            if len(alt) <= 1:
                raise ValueError(
                    "alt is expected to be larger than 1 in case "
                    "of INSERTION"
                )
            if alt[0] != ref:
                raise ValueError("First character in alt must match ref")
        elif variant_type == VariantType.DELETION:
            if ref == "":
                raise ValueError(
                    "ref is expected to be non-empty in case of " "DELETION"
                )
            if alt != "":
                raise ValueError(
                    "alt is expected to be empty in case of " "DELETION"
                )
        elif variant_type == VariantType.INDEL:
            if ref == "":
                raise ValueError(
                    "ref is expected to be non-empty in case of " "INDEL"
                )
            if alt == "":
                raise ValueError(
                    "alt is expected to be non-empty in case of " "INDEL"
                )
        else:
            if len(ref) != 1:
                raise ValueError(
                    f"Expect ref to be of length 1 in case of "
                    f"{variant_type}"
                )
            if len(alt) != 1:
                raise ValueError(
                    f"Expect alt to be of length 1 in case of "
                    f"{variant_type}"
                )
            if variant_type == VariantType.SYNONYMOUS:
                if ref != alt:
                    raise ValueError(
                        "ref and alt must be equal in case of " "SYNONYMOUS"
                    )

        if pos < 1:
            raise ValueError(f"Expect pos to be >= 1, got {pos}")

        self._ref = ref
        self._alt = alt
        self._pos = pos
        self._variant_type = variant_type
        self._meta = dict()

        # lazy evaluation of hash
        self._hash = None

    @property
    def ref(self):
        return self._ref

    @property
    def alt(self):
        return self._alt

    @property
    def pos(self):
        return self._pos

    @property
    def variant_type(self):
        return self._variant_type

    @property
    def meta(self):
        return self._meta

    def AddMetaData(self, key, data):
        """Adds arbitrary annotation data to variant

        Replaces data if *key* is already present

        :param key: Key to access added data
        :type key: :class:`str`
        :param data: Arbitrary annotation data that must be JSON serializable
        :type data: :class:`object`
        :raises: :class:`RuntimeError` if *data* is not JSON serializable
        """
        try:
            _ = json.dumps(data)
        except:
            raise RuntimeError("provided data is not JSON serializable")

        self._meta[key] = data

    def MatchesSEQRES(self, seqres):
        """Checks for match with *seqres*, raises descriptive ValueError if not

        :param seqres: Canonical sequence
        :type seqres: :class:`ost.seq.SequenceHandle` /
                      :class:`ost.seq.ConstSequenceHandle`
        :raises: :class:`ValueError` if *seqres* doesn't match
        """
        # special case of STOP_LOST: that guy MUST be at idx len(seqres)+1,
        # i.e. max_resnum+1
        if self.variant_type == VariantType.STOP_LOST:
            if self.pos != len(seqres) + 1:
                raise ValueError(
                    "STOP_LOST variants are only allowed with pos "
                    "max_resnum+1"
                )
            else:
                return

        # check whether we have a valid position. We already have a
        # guarantee from the constructor that pos is >= 1.
        if self.pos > len(seqres):
            raise ValueError(
                f"Invalid pos ({self.pos}) for seqres of length "
                f"{len(seqres)}"
            )
        start_idx = self.pos - 1
        end_idx = start_idx + len(self.ref)
        # check whether full ref fits in SEQRES
        if end_idx > len(seqres):
            raise ValueError(
                f"Variant with ref of length {len(self.ref)} at "
                f"pos {self.pos} does not fit in seqres of length "
                f"{len(seqres)}."
            )
        seqres_ref = str(seqres)[start_idx:end_idx]
        if self.ref != seqres_ref:
            raise ValueError(
                f"Expected ref of {self.ref} at pos "
                f"{self.pos} in seqres, got {seqres_ref}"
            )

    def ToJSON(self, include_meta=True):
        """Creates and returns a JSON serializable dictionary

        :returns: JSON serializable dictionary
        :rtype: :class:`dict`
        """
        return_dict = {
            "ref": self.ref,
            "alt": self.alt,
            "pos": self.pos,
            "variant_type": self.variant_type,
            "meta": dict(),
        }
        if include_meta:
            for k, v in self.meta.items():
                return_dict["meta"][k] = v

        return return_dict

    @staticmethod
    def FromJSON(json_data):
        """Creates :class:`Variant` that has been JSON serialized

        :param json_data: Data from which to initialize variant, created with
                          :func:`ToJSON`
        :type json_data: :class:`dict`
        :returns: The created :class:`Variant`
        :rtype: :class:`Variant`
        """
        expected_keys = ["ref", "alt", "pos", "variant_type", "meta"]
        for k in expected_keys:
            if k not in json_data:
                raise ValueError(f'Expected "{k}" key in json_data')
        v = Variant(
            json_data["ref"],
            json_data["alt"],
            json_data["pos"],
            VariantType(json_data["variant_type"]),
        )
        for key, value in json_data["meta"].items():
            v.AddMetaData(key, value)
        return v

    @staticmethod
    def FromHR(var_string):
        """Create variant from Human Readable (HR) string

        Example strings defining variants:

        * **A123B**    (Substitution)
        * **A123A**    (Synonymous)
        * **A123BCD**  (Indel)
        * **AX123BCD** (Indel)
        * **A123AB**   (Insertion)
        * **A123**     (Deletion)
        * **A123-**    (Frameshift)
        * **A123\***   (Stop Gained)
        * **A123?**    (Start lost)

        :param var_string: Defines a variant, must follow the pattern described
                           above
        :type var_string: :class:`str`
        :raises: :class:`RuntimeError` If *var_string* does not match any of the
                 described patterns.
        """
        # one or more capital characters
        pre_rnum_pattern = "([A-Z]+)"
        # one or more digits
        rnum_pattern = "([0-9]+)"
        # zero or more characters or one of -, *, ? (- must be escaped in regex)
        post_rnum_pattern = "([A-Z*\-?]*)"
        pattern = pre_rnum_pattern + rnum_pattern + post_rnum_pattern

        # as opposed to match, fullmatch requires the FULL string to match the
        # regex pattern (available since Python 3.4)
        m = re.fullmatch(pattern, var_string.strip())

        if not m:
            raise RuntimeError(var_string + " does not match expected patterns")

        m_groups = m.groups()
        assert len(m_groups) == 3

        ref = m_groups[0]
        pos = int(m_groups[1])
        alt = m_groups[2]

        variant_type = None
        if alt == "*":
            # its a stop gained, ref must have length 1 (checked in init)
            variant_type = VariantType.STOP_GAINED
        elif alt == "-":
            # its a frameshift, ref must have length 1 (checked in init)
            variant_type = VariantType.FRAMESHIFT
        elif alt == "?":
            # its a start lost, ref must have length 1 (checked in init)
            variant_type = VariantType.START_LOST
        elif len(alt) == 0:
            variant_type = VariantType.DELETION
        elif len(alt) == 1 and len(ref) == 1:
            if alt == ref:
                variant_type = VariantType.SYNONYMOUS
            else:
                variant_type = VariantType.SUBSTITUTION
        elif len(ref) == 1 and len(alt) > 1 and ref[0] == alt[0]:
            variant_type = VariantType.INSERTION
        else:
            variant_type = VariantType.INDEL

        return Variant(ref, alt, pos, variant_type)

    @staticmethod
    def FromHGVS(var_string, seqres):
        """Parses variants in the HGVS sequence variant nomenclature

        Here we only parse sequence variants on the protein level, *var_string*
        must therefore start with p., followed by the variant definition.
        E.g. p.Trp24Cys to define a substitution of Trp at position 24 with Cys.
        Variant definitions without experimental evidence, i.e. predicted
        consequences with no RNA or protein sequence analysed, are given in
        brackets: p.(Trp24Cys). This function simply strips these brackets away
        and they have no consequence. Sometimes, HGVS variant definitions dont
        contain all information to construct a :class:`Variant` object. An
        example would be p.Lys23_Val25del which defines a deletion of 3
        residues. The central residue can only be derived using the underlying
        *seqres* which thus has to be provided too. Sequence information is
        derived from the variant definition whenever possible (e.g. for Lys 23
        and Val 25). However, the stuff in between is blindly copied from
        *seqres* based on residue numbers without checking. The user is thus
        responsible for consistency.
        Patterns exemplified in the following list are matched to *var_string*.
        This should be sufficient to parse the most common HGVS variant
        definitions. An error is raised in case of more exotic definitions.

        * p.Trp24Cys - Defines a substitution of Trp at position 24 with Cys. 
          If the two amino acids are equal, the resulting variant will be of
          type SYNONYMOUS. If the second amino acid is Ter, the resulting
          variant will be of type STOP_GAINED.
        * p.Trp24* - Defines variant of type STOP_GAINED
        * p.Trp24= Defines variant of type SYNONYMOUS
        * p.0 or p.0? or p.Val1? - They all define variants of type START_LOST
        * p.Val7del - Defines single amino acid deletion
        * p.Lys23_Val25del - Defines the deletion of the full stretch from Lys
          23 to Val 25.
        * p.Ala3dup - Duplication of single amino acid. Gets represented by
          variant of type SUBSTITUTION
        * p.Ala3_Ser5dup - Duplication of a full stretch of amino acids. Gets
          represented by variant of type SUBSTITUTION
        * p.Lys2_Gly3insGlnSerLys - Defines a variant of type INSERTION between
          the two flanking residues Lys 2 and Gly 3.
        * p.Cys28delinsTrpVal - Defines a variant of type INDEL where one amino
          acid (Cys 28) gets replaced by TrpVal.
        * p.Glu125_Ala132delinsGlyLeuHisArgPheIleValLeu - Defines a variant of
          type INDEL where a full stretch of amino acids (Glu 125 - Ala 132)
          gets replaced by GlyLeuHisArgPheIleValLeu
        * p.Arg97fs - Defines a variant of type FRAMESHIFT
        * p.Arg97ProfsTer23 - Same as before just different format. Frameshifts
          will eventually result in a stop codon which is here specifically
          defined. However, that information doesn't get parsed.

        :param var_string: variant definition following the HGVS sequence
                           variant nomenclature
        :type var_string: :class:`str`
        :param seqres: SEQRES sequence the variant refers to
        :type seqres: :class:`ost.seq.SequenceHandle` /
                      :class:`ost.seq.ConstSequenceHandle`
        :returns: The parsed variant
        :rtype: :class:`Variant`
        :raises: :class:`RuntimeError` If *var_string* does not match any of the
                 described patterns.
        """

        var_string_processed = var_string.strip()

        # only accept variants at protein level => p. as prefix
        if not var_string_processed.startswith("p."):
            raise RuntimeError(
                f"var_string must be at protein level, i.e. "
                f'starting with "p.", got '
                f"{var_string_processed}"
            )

        # remove prefix
        var_string_processed = var_string_processed[2:]

        # predicted consequences, i.e. without experimental evidence (no RNA or
        # protein sequence analysed), should be given in parentheses, e.g.
        # p.(Arg727Ser). Here we just don't care and remove potential brackets
        var_string_processed = var_string_processed.lstrip("(").rstrip(")")

        variant = Variant._VarFromHGVS(var_string_processed, seqres)

        if variant is None:
            raise RuntimeError(f"Failed to parse HGVS variant: {var_string}")

        return variant

    @staticmethod
    def _VarFromHGVS(v, seqres):
        """Parses preprocessed HGVS string from :func:`FromHGVS`

        Checks the patterns defined in the :func:`FromHGVS` docstring and
        returns variant upon first match. Nothing is returned if nothing matches
        """

        # matches stuff like Trp24Cys => substitution
        match = re.fullmatch("([A-Z][a-z][a-z])(\d+)([A-Z][a-z][a-z])", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = common.TLCToOLC(groups[2])
            pos = int(groups[1])
            variant_type = VariantType.SUBSTITUTION
            if alt == "*":
                if ref == "*":
                    raise RuntimeError(
                        f"Do not support Synonymous variant "
                        f"of stop codon ({v})"
                    )
                variant_type = VariantType.STOP_GAINED
            if ref == alt:
                variant_type = VariantType.SYNONYMOUS
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like Trp24* => STOP
        match = re.fullmatch("([A-Z][a-z][a-z])(\d+)\*", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = "*"
            pos = int(groups[1])
            variant_type = VariantType.STOP_GAINED
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like Trp24= => synonymous
        match = re.fullmatch("([A-Z][a-z][a-z])(\d+)\=", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = ref
            pos = int(groups[1])
            variant_type = VariantType.SYNONYMOUS
            return Variant(ref, alt, pos, variant_type)

        # the following strings represent START_LOST
        if v == "0" or v == "0?":
            return Variant("M", "?", 1, VariantType.START_LOST)

        # matches stuff like "Met1?"
        match = re.fullmatch("([A-Z][a-z][a-z])1\?", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = "?"
            pos = 1
            variant_type = VariantType.START_LOST
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Val7del" => single amino acid deletion
        match = re.fullmatch("([A-Z][a-z][a-z])(\d+)del", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = ""
            pos = int(groups[1])
            variant_type = VariantType.DELETION
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Lys23_Val25del" => deletion of several amino acids
        match = re.fullmatch(
            "([A-Z][a-z][a-z])(\d+)_([A-Z][a-z][a-z])(\d+)del", v
        )
        if match:
            groups = match.groups()
            # variant string does not contain all information required to
            # construct ref sequence, need fallback to *seqres*
            start_pos = int(groups[1])
            end_pos = int(groups[3])
            if start_pos >= end_pos:
                raise RuntimeError(f'Deletion start in "{v}" must be < end')
            if end_pos >= len(seqres):
                raise RuntimeError(f'Invalid pos in "{v}" given seqres')
            ref = common.TLCToOLC(groups[0])
            ref += seqres[start_pos : end_pos - 1]
            ref += common.TLCToOLC(groups[2])
            alt = ""
            pos = int(groups[1])
            variant_type = VariantType.DELETION
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Ala3dup" => single amino acid duplication
        match = re.fullmatch("([A-Z][a-z][a-z])(\d+)dup", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = ref + ref
            pos = int(groups[1])
            variant_type = VariantType.INSERTION
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Ala3_Ser5dup" => multiple amino acid duplication
        match = re.fullmatch(
            "([A-Z][a-z][a-z])(\d+)_([A-Z][a-z][a-z])(\d+)dup", v
        )
        if match:
            groups = match.groups()
            # variant string does not contain all information required to
            # construct alt sequence, need fallback to *seqres*
            start_pos = int(groups[1])
            end_pos = int(groups[3])
            if start_pos >= end_pos:
                raise RuntimeError(f'Deletion start in "{v}" must be < end')
            if end_pos >= len(seqres):
                raise RuntimeError(f'Invalid pos in "{v}" given seqres')
            ref = common.TLCToOLC(groups[2])
            alt = common.TLCToOLC(groups[2])
            alt += common.TLCToOLC(groups[0])
            alt += seqres[start_pos : end_pos - 1]
            alt += common.TLCToOLC(groups[2])
            pos = end_pos
            variant_type = VariantType.INSERTION
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Lys2_Gly3insGlnSerLys" => Insertion of GlnSerLys
        # between Lys2 and Gly3
        match = re.fullmatch(
            "([A-Z][a-z][a-z])(\d+)_([A-Z][a-z][a-z])(\d+)ins(([A-Z][a-z][a-z])+)",
            v,
        )
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt_tlcs = [
                groups[4][i : i + 3] for i in range(0, len(groups[4]), 3)
            ]
            alt = ref + "".join([common.TLCToOLC(tlc) for tlc in alt_tlcs])
            if "*" in alt:
                raise RuntimeError(
                    f"Do not support insertion of stop codons " f"({v})"
                )
            left_flank_pos = int(groups[1])
            right_flank_pos = int(groups[3])
            if left_flank_pos + 1 != right_flank_pos:
                raise RuntimeError(
                    f"Number of amino acid left to insertion "
                    f"({left_flank_pos}) and right of insertion "
                    f"({right_flank_pos}) must be consecutive."
                )
            pos = left_flank_pos
            variant_type = VariantType.INSERTION
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Cys28delinsTrpVal" => Indel with single amino acid
        # being deleted
        match = re.fullmatch(
            "([A-Z][a-z][a-z])(\d+)delins(([A-Z][a-z][a-z])+)", v
        )
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt_tlcs = [
                groups[2][i : i + 3] for i in range(0, len(groups[2]), 3)
            ]
            alt = "".join([common.TLCToOLC(tlc) for tlc in alt_tlcs])
            if "*" in alt:
                raise RuntimeError(
                    f"Do not support insertion of stop codons " f"({v})"
                )
            pos = int(groups[1])
            variant_type = VariantType.INDEL
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Glu125_Ala132delinsGlyLeuHisArgPheIleValLeu"
        # => Indel with multiple amino acids being deleted
        match = re.fullmatch(
            "([A-Z][a-z][a-z])(\d+)_([A-Z][a-z][a-z])(\d+)delins(([A-Z][a-z][a-z])+)",
            v,
        )
        if match:
            groups = match.groups()
            start_pos = int(groups[1])
            end_pos = int(groups[3])
            ref = common.TLCToOLC(groups[0])
            ref += seqres[start_pos : end_pos - 1]
            ref += common.TLCToOLC(groups[2])
            alt_tlcs = [
                groups[4][i : i + 3] for i in range(0, len(groups[4]), 3)
            ]
            alt = "".join([common.TLCToOLC(tlc) for tlc in alt_tlcs])
            if "*" in alt:
                raise RuntimeError(
                    f"Do not support insertion of stop codons " f"({v})"
                )
            pos = start_pos
            variant_type = VariantType.INDEL
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Arg97fs" => mutation that induces frameshift
        # starting at position 97
        match = re.fullmatch("([A-Z][a-z][a-z])(\d+)fs", v)
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = "-"
            pos = int(groups[1])
            variant_type = VariantType.FRAMESHIFT
            return Variant(ref, alt, pos, variant_type)

        # matches stuff like "Arg97ProfsTer23" => same as above just different
        # format
        match = re.fullmatch(
            "([A-Z][a-z][a-z])(\d+)([A-Z][a-z][a-z])fsTer(\d+)", v
        )
        if match:
            groups = match.groups()
            ref = common.TLCToOLC(groups[0])
            alt = "-"
            pos = int(groups[1])
            variant_type = VariantType.FRAMESHIFT
            return Variant(ref, alt, pos, variant_type)

    def ToHR(self):
        """Returns Human Readable (HR) string from variant

        see :func:`FromHR` for examples

        :returns: Human readable string in format <ref><pos><alt>
        """
        return f"{self.ref}{self.pos}{self.alt}"

    def GetHash(self):
        """Returns unique hash that depends on all attributes EXCEPT META

        :returns: Unique variant specific hash
        :rtype: :class:`str`
        """
        if self._hash is None:
            json_dict = self.ToJSON(include_meta=False)
            json_str = json.dumps(json_dict, sort_keys=True)
            self._hash = hashlib.md5(json_str.encode()).hexdigest()
        return self._hash

    def SetHash(self, h):
        """Manually set hash that will be returned by :func:`GetHash`
        
        !!!!!!!!!!!!!!ONLY DO THAT IF YOU KNOW WHAT YOURE DOING!!!!!!!!!!!!!!!
        !!!!!!!!!!!!!!!!!!!!!!!!ACTUALLY, JUST DONT!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Or better watch a video of Bruce Lee beating up Chuck Norris:
        https://www.youtube.com/watch?v=HFm2L03AetU
        """
        self._hash = h

    def GetRange(self):
        """Returns range in reference sequence which is covered by *variant*

        That's the range starting from self.pos with length defined by self.ref

        :returns: Range covered by that variant
        :rtype: :class:`list` of length 2
        """
        v_start = self.pos
        v_end = v_start + len(self.ref) - 1
        return [v_start, v_end]

    def InRange(self, seq_range):
        """Returns True if *seq_range* is covered by *variant*, False otherwise.

        Also returns True if *seq_range* is None.

        :param seq_range: Range to check
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        """
        if seq_range is None:
            return True
        v_range = self.GetRange()
        if v_range[1] < seq_range[0] or v_range[0] > seq_range[1]:
            return False
        return True

    def __eq__(self, other):
        """Returns equality based on hashes
        
        Depends on all attributes EXCEPT META
        """
        if isinstance(other, Variant):
            return self.GetHash() == other.GetHash()
        return False

    def __ne__(self, other):
        """Returns inequality based on hashes
        
        Depends on all attributes EXCEPT META
        """

        return not self.__eq__(other)


class Structure:
    """Helper class to map structural data to a reference sequence

    The object contains a protein complex with bells and whistles
    (potentially oligomeric, contains ligands etc.). One or several peptide
    segments map to the reference sequence.
    In case of a theoretical model, this mapping may be perfect. It's a model
    of that sequence in the end. In case of experimental structures, small
    changes with respect to the target sequence can be expected. Enforcing a
    perfect match is thus optional.

    The following class attributes can be accessed after construction. They
    define the actual structure, while the additional *meta* attribute allows
    to decorate it with arbitrary annotations using :func:`AddMetaData`.
    HASHES AND EQUALITY OPERATIONS DO NOT DEPEND ON META!!!

    * sequence - Passed as *sequence* argument in constructor
    * structure_identifier - Passed as *structure_identifier* argument in
      constructor
    * assembly - Passed as *assembly* argument in constructor
    * segment_query - Passed as *segment_query* argument in constructor.
      Guaranted to be of type :class:`list`, if you passed a single string at
      construction, thats the only element in that list.
    * aln - Passed as *aln* argument in constructor. Guaranteed to be of
      type :class:`list`, if you passed a single 
      :class:`ost.seq.AlignmentHandle` at construction, thats the only element
      in that list. For convenience, each alignment has an 
      :class:`ost.mol.EntityView` attached that represents the corresponding
      segments, i.e. aln[idx].AttachView(1, assembly.Select(segment_query[idx]))
      The sequence name of the first sequence in every alignment is equal to the
      one in *sequence*, the sequence name of the second sequence is equal to
      the chain name the corresponding segment corresponds to.
    * strict - Passed as *strict* argument in constructor
    * segments - :class:`list` of :class:`ost.mol.EntityView` objects that refer
      to *assembly*. Constructed using *segment_query* and guaranteed to refer
      to exactly one chain in assembly.
    * labels - :class:`dict` with :class:`str` as keys and 
                  :class:`str`/:class:`list` of :class:`str` as values

    :param sequence: Reference sequence
    :type sequence: :class:`ost.seq.SequenceHandle` /
                    :class:`ost.seq.ConstSequenceHandle`
    :param structure_identifier: Arbitrary string to identify structure later on
    :type structure_identifier: :class:`str`
    :param assembly: A protein structure
    :type assembly: :class:`ost.mol.EntityHandle`
    :param segment_query: Defines segment(s) in *assembly* that map to
                          *sequence* in the OpenStructure query language.
                          All residues in one segment must be from the same
                          chain. To define a single segment containing a full
                          chain, let's say chain A, *segment_query* would be
                          "cname=A" ("peptide=true and cname=A" if chain A
                          contains non peptide components). To additionally
                          restrict to a range of residues, you can do something
                          like "cname=A and rnum=10:50" (both, residue 10 and 
                          50, are included in this query). More info can be 
                          found in the OpenStructure documentation
                          (https://openstructure.org/docs/dev/mol/base/query/).
                          Several segments mapping to *sequence* are defined
                          as lists of such queries.
    :type segment_query: :class:`str`/:class:`list` of :class:`str`
    :param aln: Maps segment(s) to sequence. If given, the number of alignments
                must match the number of elements in *segment_query*. 
                For each alignment, the first sequence (without gaps) must
                exactly match the reference sequence and the second sequence
                (without gaps) must exactly match the segment residues.
                If not given, the alignment(s) is/are derived using the 
                AlignToSEQRES function in OpenStructure. A Needleman-Wunsch with
                strict substitution matrix (BLOSUM100) is used as fallback.
    :type aln: :class:`ost.seq.AlignmentHandle`/:class:`list` of
               :class:`AlignmentHandle`
    :param strict: If enabled, a perfect match between alignment and segment(s)
                   is enforced, i.e. a RuntimeError is raised if 1) The first
                   sequence in any aln contains a gap 2) The second sequence 
                   in any aln is not a perfect subsequence of the first 
                   (no 100% sequence identity)
    :type strict: :class:`bool`
    :raises: :class:`TypeError` if any constructor argument is not of expected
             type. :class:`RuntimeError` if any aln does not match
             *sequence* and the sequence extracted from *segment* or if you
             enabled the strict flag and the alignment check failed.
    """

    def __init__(
        self,
        sequence,
        structure_identifier,
        assembly,
        segment_query,
        aln=None,
        strict=False,
    ):
        # Do type checks... totally unpythonic I know
        if not (
            isinstance(sequence, seq.SequenceHandle)
            or isinstance(sequence, seq.ConstSequenceHandle)
        ):
            self._RaiseType(
                "sequence", "ost.seq.SequenceHandle/ost.seq.ConstSequenceHandle"
            )

        if not isinstance(structure_identifier, str):
            self._RaiseType("structure", "str")

        if not (isinstance(assembly, mol.EntityHandle)):
            self._RaiseType("assembly", "ost.mol.EntityHandle")

        if isinstance(segment_query, str):
            segment_query = [segment_query]

        if not isinstance(segment_query, list):
            self._RaiseType("segment_query", "str or a list thereof")

        for q in segment_query:
            if not isinstance(q, str):
                self._RaiseType("segment_query", "str or a list thereof")

        if isinstance(aln, seq.AlignmentHandle):
            aln = [aln]

        if aln is not None:
            if not isinstance(aln, list):
                self._RaiseType(
                    "aln", "ost.seq.AlignmentHandle or a list thereof"
                )
            for a in aln:
                if not isinstance(a, seq.AlignmentHandle):
                    self._RaiseType(
                        "aln", "ost.seq.AlignmentHandle or a list thereof"
                    )
            if len(aln) != len(segment_query):
                raise ValueError(
                    "If you provide aln, it must have the same "
                    "number of elements as segment_query"
                )

        if not isinstance(strict, bool):
            self._RaiseType("strict", "bool")

        self._sequence = sequence
        self._structure_identifier = structure_identifier
        self._assembly = assembly
        self._segment_query = segment_query
        self._aln = aln
        self._strict = strict
        self._segments = list()
        self._meta = dict()

        for q in segment_query:
            segment = self.assembly.Select(q)
            if len(segment.chains) != 1:
                raise RuntimeError(
                    "Each segment query must select at least "
                    "one residue and all residues must be from "
                    "the same chain"
                )
            self._segments.append(segment)

        if self._aln is None:
            self._aln = self._SetupAln()
        self._CheckAlnConsistency()
        self._aln = self._RenameAlnSequences()

        if strict:
            self._StrictAlignment()

        # attach segment views to alignments
        for segment_idx in range(self.GetNumSegments()):
            self._aln[segment_idx].AttachView(1, self.segments[segment_idx])

        # hash and omf_str are lazily evaluated
        self._hash = None
        self._omf_str = None

    @property
    def sequence(self):
        return self._sequence

    @property
    def structure_identifier(self):
        return self._structure_identifier

    @property
    def assembly(self):
        return self._assembly

    @property
    def segment_query(self):
        return self._segment_query

    @property
    def aln(self):
        return self._aln

    @property
    def strict(self):
        return self._strict

    @property
    def segments(self):
        return self._segments

    @property
    def hash(self):
        return self.GetHash()

    @property
    def omf_str(self):
        if self._omf_str is None:
            self._omf_str = self.EncodeAssembly()
        return self._omf_str

    @property
    def meta(self):
        return self._meta

    def AddMetaData(self, key, data):
        """Adds arbitrary annotation data to structure

        Replaces data if *key* is already present

        :param key: Key to access added data
        :type key: :class:`str`
        :param data: Arbitrary annotation data that must be JSON serializable
        :type data: :class:`object`
        :raises: :class:`RuntimeError` if *data* is not JSON serializable
        """
        try:
            _ = json.dumps(data)
        except:
            raise RuntimeError("provided data is not JSON serializable")

        self._meta[key] = data

    def GetNumSegments(self):
        """Returns number of segments that refer to target sequence

        :returns: The number of segments
        :rtype: :class:`int`
        """
        return len(self.segment_query)

    def MapToReference(self, values, segment_idx):
        """Helper function to map per-residue values to *sequence* 

        :param values: The values you want to map, length must match the number
                       of residues in segment with index *segment_idx*.
        :type values: :class:`list`
        :param segment_idx: Specifies segment
        :type segment_idx: :class:`int`
        :returns: A :class:`list` of length len(*sequence*) constructed using
                  the mapping info in *aln*. Positions that are not covered by 
                  residues in specified segment are set to None.
        :rtype: :class:`list`
        :raises: :class:`RuntimeError` if length of *values* does not match
                 number of residues in specified segment.
        """
        if segment_idx < 0 or segment_idx >= self.GetNumSegments():
            raise RuntimeError("Invalid segment idx")
        segment = self.segments[segment_idx]
        if len(values) != len(segment.residues):
            raise RuntimeError(
                "Length of input values must match number of "
                "residues in specified segment"
            )
        return_list = [None] * len(self.sequence)
        current_ref_idx = -1
        current_values_idx = -1
        for col in self.aln[segment_idx]:
            if col[0] != "-":
                current_ref_idx += 1
            if col[1] != "-":
                current_values_idx += 1
            if col[0] != "-" and col[1] != "-":
                return_list[current_ref_idx] = values[current_values_idx]
        return return_list

    def ToJSON(self, include_meta=True):
        """Creates and returns a JSON serializable dictionary

        :returns: JSON serializable dictionary
        :rtype: :class:`dict`
        """
        json_data = dict()
        json_data["sequence"] = {
            "name": self.sequence.GetName(),
            "sequence": str(self.sequence),
        }
        json_data["structure_identifier"] = self.structure_identifier
        json_data["assembly"] = self.omf_str
        json_data["segment_query"] = self.segment_query
        json_data["aln"] = list()
        for a in self.aln:
            seqres = a.GetSequence(0)
            atomseq = a.GetSequence(1)
            a_dict = dict()
            a_dict["seqres"] = {
                "name": seqres.GetName(),
                "sequence": str(seqres),
            }
            a_dict["atomseq"] = {
                "name": atomseq.GetName(),
                "sequence": str(atomseq),
            }
            json_data["aln"].append(a_dict)
        json_data["strict"] = self.strict
        json_data["meta"] = dict()

        if include_meta:
            for k, v in self.meta.items():
                json_data["meta"][k] = v

        return json_data

    @staticmethod
    def FromJSON(json_data):
        """Creates :class:`Structure` that has been JSON serialized

        :param json_data: Data from which to initialize structure, created with
                          :func:`ToJSON`
        :type json_data: :class:`dict`
        :returns: The created :class:`Structure`
        :rtype: :class:`Structure`
        """
        expected_keys = [
            "sequence",
            "structure_identifier",
            "assembly",
            "segment_query",
            "aln",
            "strict",
            "meta",
        ]
        for k in expected_keys:
            if k not in json_data:
                raise ValueError(f'Expected "{k}" key in json_data')
        sequence = seq.CreateSequence(
            json_data["sequence"]["name"], json_data["sequence"]["sequence"]
        )

        assembly = Structure.DecodeAssembly(json_data["assembly"])
        aln = list()
        for a_dict in json_data["aln"]:
            seqres = seq.CreateSequence(
                a_dict["seqres"]["name"], a_dict["seqres"]["sequence"]
            )
            atomseq = seq.CreateSequence(
                a_dict["atomseq"]["name"], a_dict["atomseq"]["sequence"]
            )
            a = seq.CreateAlignment()
            a.AddSequence(seqres)
            a.AddSequence(atomseq)
            aln.append(a)
        s = Structure(
            sequence,
            json_data["structure_identifier"],
            assembly,
            json_data["segment_query"],
            aln=aln,
            strict=json_data["strict"],
        )
        # call it hacky or not... costs some memory but saves computation time
        # when calculating the hash on that structure. Furthermore we guarantee
        # that the hash stays the same as oposed to potential rounding errors
        # when recreating the omf string.
        s._omf_str = json_data["assembly"]

        # meta data must be attached separately
        for k, v in json_data["meta"].items():
            s.AddMetaData(k, v)

        return s

    def EncodeAssembly(self):
        """Encodes assembly in compressed string format

        :returns: Compressed assembly string
        :rtype: :class:`str`
        """
        omf = io.OMF.FromEntity(self.assembly)
        omf_gz = gzip.compress(omf.ToBytes(), mtime=0)
        # bytes object is not JSON serializable => base64 encode and then
        # decode to string - 33% size increase :(
        return base64.b64encode(omf_gz).decode("utf-8")

    @staticmethod
    def DecodeAssembly(assembly_str):
        """Takes string encoded with :func:`EncodeAssembly` and returns assembly

        :param assembly_str: Compressed assembly string encoded with 
                             :func:`EncodeAssembly`
        :type assembly_str: :class:`str`
        :returns: The decoded assembly
        :rtype: :class:`ost.mol.EntityHandle`
        """
        omf_gz = base64.b64decode(assembly_str.encode("utf-8"))
        omf = io.OMF.FromBytes(gzip.decompress(omf_gz))
        return omf.GetAU()

    def GetHash(self):
        """Returns unique hash that depends on all attributes EXCEPT META

        First call of this function is computationally expensive! 
        (once calculated, the hash is cached until object destruction).

        :returns: Unique structure specific hash
        :rtype: :class:`str`
        """
        if self._hash is None:
            json_dict = self.ToJSON(include_meta=False)
            json_str = json.dumps(json_dict, sort_keys=True)
            self._hash = hashlib.md5(json_str.encode()).hexdigest()
        return self._hash

    def SetHash(self, h):
        """Manually set hash that will be returned by :func:`GetHash`
        
        !!!!!!!!!!!!!!ONLY DO THAT IF YOU KNOW WHAT YOURE DOING!!!!!!!!!!!!!!!
        !!!!!!!!!!!!!!!!!!!!!!!!ACTUALLY, JUST DONT!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Or better watch a video of Bruce Lee beating up Chuck Norris:
        https://www.youtube.com/watch?v=HFm2L03AetU
        """
        self._hash = h

    def GetAssemblyHash(self):
        """Returns unique hash that only depends on assembly

        This function is computationally expensive!

        :returns: Unique hash that only depends on assembly
        :rtype: :class:`str`
        """
        return hashlib.md5(self.omf_str.encode()).hexdigest()

    def InRange(self, seq_range, segment_idx=None):
        """Returns True if *seq_range* is covered by at least one residue
        
        Also returns True if *seq_range* is None

        :param seq_range: Sequence range to check
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :param segment_idx: If None, residue from all segments are considered,
                            only from specified segment otherwise.
        :type segment_idx: :class:`int`
        :returns: If structure covers *seq_range*
        :rtype: :class:`bool`
        :raises: :class:`RuntimeError` if *segment_idx* is invalid
        """
        if seq_range is None:
            return True

        if segment_idx is None:
            aln = self.aln
        else:
            if segment_idx < 0 or segment_idx >= self.GetNumSegments():
                raise RuntimeError("Invalid segment idx")
            aln = [self.aln[segment_idx]]

        for a in aln:
            rnum = 0
            for col in a:
                if col[0] != "-":
                    rnum += 1
                    if col[1] != "-":
                        if seq_range[0] <= rnum <= seq_range[1]:
                            return True
        return False

    def GetResidue(self, segment_idx, pos):
        """Returns residue from specified segment at pos

        Returns None if pos is not covered by segment

        :param segment_idx: To which segment the residue belongs
        :type segment_idx: :class:`int`
        :param pos: Position in reference sequence (1-based indexing)
        :type pos: :class:`int`
        :returns: The residue at specified pos, None if its not covered by
                  specified segment
        :rtype: :class:`ost.mol.ResidueView`
        :raises: :class:`RuntimeError` if *segment_idx* or *pos* are invalid
        """
        if segment_idx < 0 or segment_idx >= self.GetNumSegments():
            raise RuntimeError("Invalid segment idx")
        if pos < 1 or pos > len(self.sequence):
            raise RuntimeError("Invalid pos")

        col_idx = self.aln[segment_idx].GetPos(0, pos - 1)
        r = self.aln[segment_idx][col_idx].GetResidue(1)
        if r.IsValid():
            return r
        return None

    def GetCAPos(self, segment_idx, pos):
        """Returns CA position from specified segment at pos

        Returns None if pos is not covered by segment or when corresponding
        residue has no atom named 'CA'

        :param segment_idx: To which segment the residue belongs
        :type segment_idx: :class:`int`
        :param pos: Position in reference sequence (1-based indexing)
        :type pos: :class:`int`
        :returns: The CA position at specified pos, None if its not covered by
                  specified segment or when residue has no atom named 'CA'
        :rtype: :class:`ost.geom.Vec3`
        :raises: :class:`RuntimeError` if *segment_idx* or *pos* are invalid
        """
        r = self.GetResidue(segment_idx, pos)
        if r is not None:
            ca = r.FindAtom("CA")
            if ca.IsValid():
                return ca.GetPos()
        return None

    def GetRangeCovered(self, segment_idx):
        """Returns range that is structurally covered by specified segment

        :param segment_idx: From which segment you want the range from
        :type segment_idx: :class:`int`
        :returns: The range in form (from, to) referring to the reference
                  sequence
        :rtype: :class:`tuple` with two :class:`int`
        """
        if segment_idx < 0 or segment_idx >= self.GetNumSegments():
            raise RuntimeError("Invalid segment idx")
        atomseq = str(self.aln[segment_idx].GetSequence(1))
        missing_left = len(atomseq) - len(atomseq.lstrip("-"))
        missing_right = len(atomseq) - len(atomseq.rstrip("-"))
        return (1 + missing_left, len(self.sequence) - missing_right)

    def _RaiseType(self, param, exp):
        raise TypeError(
            f"{param} param in Structure Constructor is "
            f"expected to be of type {exp}"
        )

    def _CreateAlnNW(self, sequence, segment):
        """Aligns *segment* to *sequence* using Needleman-Wunsch

        Uses very strict substitution matrix (BLOSUM100).
        """
        segment_seq = "".join([r.one_letter_code for r in segment.residues])
        aln = seq.alg.SemiGlobalAlign(
            seq.CreateSequence("seqres", str(sequence)),
            seq.CreateSequence("atom_seq", segment_seq),
            seq.alg.BLOSUM100,
        )[0]
        if (
            str(sequence) != aln.GetSequence(0).GetGaplessString()
            or segment_seq != aln.GetSequence(1).GetGaplessString()
        ):
            raise RuntimeError("Shouldnt happen. Gabriel fixes this for a beer")
        return aln

    def _SetupAln(self):
        """Creates alignments of all segments and returns them

        Tries OpenStructure AlignToSEQRES first.
        If that fails, alignment happens with Needleman-Wunsch using a very
        strict substitution matrix (when connectivity problems are present
        or we're dealing with an experimental structure that doesn't entirely
        match SEQRES).
        """
        aln_list = list()
        for query in self.segment_query:
            segment = self.assembly.Select(query)
            try:
                aln_list.append(
                    seq.alg.AlignToSEQRES(
                        segment,
                        self.sequence,
                        try_resnum_first=True,
                        validate=False,
                    )
                )
            except Exception as e:
                aln_list.append(self._CreateAlnNW(self.sequence, segment))

        return aln_list

    def _CheckAlnConsistency(self):
        """Checks whether aln is consistent with sequence and assembly

        Raises descriptive RuntimeError if not.
        """
        for idx, a in enumerate(self.aln):
            if a.GetCount() != 2:
                raise RuntimeError(
                    "Expect exactly two sequences in each " "aln"
                )
            if str(self.sequence) != str(a.GetSequence(0).GetGaplessString()):
                raise RuntimeError(
                    "First sequence in each aln is expected "
                    "to match provided reference sequence"
                )
            segment = self.segments[idx]
            atomseq = "".join([r.one_letter_code for r in segment.residues])
            if atomseq != str(a.GetSequence(1).GetGaplessString()):
                raise RuntimeError(
                    "Second sequence in each aln is expected to "
                    "match ATOMSEQ in corresponding assembly "
                    "segment."
                )

    def _RenameAlnSequences(self):
        """Renames sequences in self._aln such that the first sequence always
        has the same name as self.sequence and the second sequence always has
        the same name as the chain in self.assembly it refers to.
        """
        new_alns = list()  # cannot change things in current aln sequences as
        # they're of type ConstSequenceHandle
        for aln, segment in zip(self._aln, self._segments):
            new_aln = seq.CreateAlignment()
            seqres = seq.CreateSequence(
                self.sequence.GetName(), str(aln.GetSequence(0))
            )
            seqres.SetOffset(aln.GetSequence(0).GetOffset())
            atomseq = seq.CreateSequence(
                segment.chains[0].GetName(), str(aln.GetSequence(1))
            )
            atomseq.SetOffset(aln.GetSequence(1).GetOffset())
            new_aln.AddSequence(seqres)
            new_aln.AddSequence(atomseq)
            new_alns.append(new_aln)
        return new_alns

    def _StrictAlignment(self):
        """Checks for perfect alignment

        Checks for gapless SEQRES (first sequence) and 100% seqid.
        Raises descriptive RuntimeError if this is not the case.
        """
        for aln in self.aln:
            s1 = str(aln.GetSequence(0))
            s2 = str(aln.GetSequence(1))
            if s1.find("-") != -1:
                raise RuntimeError(
                    "Failed to align segment to SEQRES - SEQRES "
                    "must not contain gaps"
                )
            if sum(
                [
                    1
                    for a, b in zip(s1, s2)
                    if (a != "-" and b != "-") and a != b
                ]
            ):
                raise RuntimeError(
                    "Failed to align segment to SEQRES - "
                    "SEQRES - ATOMSEQ does not match."
                )

    def __eq__(self, other):
        """Returns equality based on hashes
        
        Depends on all attributes EXCEPT META
        """
        if isinstance(other, Structure):
            return self.GetHash() == other.GetHash()
        return False

    def __ne__(self, other):
        """Returns inequality based on hashes
        
        Depends on all attributes EXCEPT META
        """

        return not self.__eq__(other)


class VarImporter:
    """General interface for variant import

    Subclasses import variants from arbitrary sources and must implement the
    :func:`Run` method
    """

    def __init__(self):
        pass

    def Import(self, sequence, validate=True, seq_range=None):
        """Imports variants for *sequence*.

        Calls :func:`Run` which must be implemented by child class.

        :param sequence: Canonical sequence the variants refer to 
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param validate: Sanity check - calls 
                         :func:`Variant.MatchesSEQRES` with *sequence* as 
                         seqres argument for all imported variants.
        :type validate: :class:`bool`
        :param seq_range: Optionally provide range in *sequence* you're
                          interested in, will be passed to :meth:`Run`.
                          No matter what subclass returns, only variants
                          covering that range are kept.
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: The imported variants referring to *sequence*
        :rtype: :class:`list` of :class:`Variant`
        :raises: :class:`RuntimeError` If *validate* is True and inconsistencies
                 are detected.
        """
        if seq_range is not None:
            common.CheckRange(sequence, seq_range)

        variants = self.Run(sequence, seq_range=seq_range)

        if seq_range is not None:
            variants = [v for v in variants if v.InRange(seq_range)]

        if not isinstance(variants, list):
            raise RuntimeError(
                "VariantImporter subclass must return list, "
                "got " + str(type(variants))
            )

        for v in variants:
            if not isinstance(v, Variant):
                raise RuntimeError(
                    "VariantImporter subclass must return list "
                    "of Variant object. Got list containing " + str(type(v))
                )

        if validate:
            for v in variants:
                v.MatchesSEQRES(sequence)

        return variants

    def Run(self, sequence, seq_range=None):
        """Must be implemented by child class

        :param sequence: *sequence* param from :func:`Import`
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: *seq_range* param from :func:`Import`
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: Variants as described in :func:`Import`
        """
        raise NotImplementedError("Incomplete child class")


class StructImporter:
    """General interface for structure import

    Subclasses import structures from arbitrary sources and must implement the
    :func:`Run` method
    """

    def __init__(self):
        pass

    def Import(self, sequence, seq_range=None):
        """Imports structures for *sequence*.

        Calls :func:`Run` which must be implemented by child class.

        :param sequence: Canonical sequence the structures refer to
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: Optionally provide range in *sequence* you're
                          interested in, will be passed to :meth:`Run`. No
                          matter what subclass returns, only structures that
                          cover this range with at least one residue are kept.
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int` 
        :returns: The imported structures referring to *sequence*
        :rtype: :class:`list` of :class:`Structure`
        """
        if seq_range is not None:
            common.CheckRange(sequence, seq_range)

        structures = self.Run(sequence, seq_range=seq_range)

        if not isinstance(structures, list):
            raise RuntimeError(
                "StructImporter subclass must return list, "
                "got " + str(type(structures))
            )

        for s in structures:
            if not isinstance(s, Structure):
                raise RuntimeError(
                    "StructImporter subclass must return list "
                    "of Structure object. Got list containing " + str(type(s))
                )

        for s in structures:
            if str(s.sequence) != str(sequence):
                raise RuntimeError(
                    "Sequence mismatch between reference "
                    "sequence in StructImporter and one of the "
                    "returned structures"
                )

        if seq_range is not None:
            structures = [s for s in structures if s.InRange(seq_range)]

        return structures

    def Run(self, sequence, seq_range=None):
        """Must be implemented by child class

        :param sequence: *sequence* param from :func:`Import`
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: *seq_range* param from :func:`Import`
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: Structures as described in :func:`Import`
        """
        raise NotImplementedError("Incomplete child class")


class SeqAnno:
    """General interface for sequence annotation
    
    Subclasses assign per-character annotations and must implement the
    :func:`Key` and :func:`Run` methods
    """

    def __init__(self):
        pass

    def Annotate(self, sequence, seq_range=None):
        """Annotation for every character in *sequence*

        Calls :func:`Run` which must be implemented by child class.

        :param sequence: Sequence to annotate
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: Optionally provide range in *sequence* you're
                          interested in, will be passed to :meth:`Run`. No
                          matter what subclass returns, all annotations outside
                          this range will be set to None.
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: List with len(*sequence*) annotation elements
        :rtype: :class:`list`
        """
        if not (
            isinstance(sequence, seq.SequenceHandle)
            or isinstance(sequence, seq.ConstSequenceHandle)
        ):
            raise RuntimeError(
                "Expect sequence to be of type "
                "ost.seq.SequenceHandle or "
                "ost.seq.ConstSequenceHandle got " + str(type(sequence))
            )

        if seq_range is not None:
            common.CheckRange(sequence, seq_range)

        annotation = self.Run(sequence, seq_range=seq_range)

        if not isinstance(annotation, list):
            raise RuntimeError(
                "SeqAnno subclass must return list, "
                "got " + str(type(annotation))
            )

        if len(annotation) != len(sequence):
            raise RuntimeError(
                f"SeqAnno subclass must return list with same "
                f"length as input sequence (len(sequence): "
                f"{len(sequence)}, len(annotation_list): "
                f"{len(annotation)})"
            )

        if seq_range is not None:
            for idx in range(seq_range[0] - 1):
                annotation[idx] = None
            for idx in range(seq_range[1], len(annotation)):
                annotation[idx] = None

        return annotation

    def Key(self):
        """Must be implemented by child class

        :returns: Identifier of annotation
        :rtype: :class:`str`
        """
        raise NotImplementedError("Incomplete child class")

    def Run(self, sequence, seq_range=None):
        """Must be implemented by child class

        :param sequence: *sequence* param from :func:`Annotate`
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: *seq_range* param from :func:`Annotate`
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: Annotation as described in :func:`Annotate`
        """
        raise NotImplementedError("Incomplete child class")

    def SubSeq(self, sequence, seq_range):
        """Extracts sub-sequence given seq_range

        Helper function to be used by child classes.
        
        :param sequence: Sequence from which to extract subsequence
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: Range in form (from, to) defining the subsequence.
                          from and to are 1-based indices and both are
                          included in the returned subsequence.
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: Sub-sequence as defined by *seq_range*
        :rtype: :class:`ost.seq.SequenceHandle`
        """
        if seq_range is None:
            return sequence
        else:
            return seq.CreateSequence(
                sequence.GetName(), sequence[seq_range[0] - 1 : seq_range[1]]
            )

    def MapAnno(self, sequence, anno, seq_range):
        """Maps annotation relative to subsequence back to overall sequence

        Helper function to be used by child classes. Useful for mapping
        annotations specific to subsequences created with :func:`SubSeq`.
        Every item outside that range is set to None.

        :param sequence: The original sequence
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :param seq_range: The range you used to extract subsequence
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :param anno: Annotation for *seq_range*
        :type anno: :class:`list` 
        :returns: List of length len(*sequence*) with *anno* mapped to
                  location
        :rtype: :class:`list`
        """
        if seq_range is None:
            assert len(sequence) == len(anno)
            return anno
        else:
            assert seq_range[1] - seq_range[0] + 1 == len(anno)
            return_anno = [None] * len(sequence)
            start_idx = seq_range[0] - 1
            for item_idx, item in enumerate(anno):
                return_anno[start_idx + item_idx] = item
        return return_anno


class StructAnno:
    """General interface for structure annotation
    
    Subclasses assign per-structure annotations. They can either be arbitrary
    annotations referring to a full :class:`Structure`, or per-residue
    annotations. Subclasses must implement the :func:`PerResidue` function.
    The only consequence if this returns True is a consistency check in
    :func:`Annotate` to ensure per-residue data in the described format. 
    Additionally, subclasses must implement the :func:`Key` and :func:`Run`
    methods. 
    """

    def __init__(self):
        pass

    def Annotate(self, sequence, structures, seq_range=None):
        """Annotations for each structure in *structures*

        Calls :func:`Run` which must be implemented by child class.

        The return list contains len(*structures*) annotations.
        If :func:`PerResidue` returns True, the following formatting for
        a structure annotation is enforced:
        A list with s.GetNumSegments() elements, each of which being a list of
        length len(*sequence*).
        The position of a residue in these lists is defined by the respective
        structure alignments. Positions that are not covered by structural
        information are expected to be None.

        :param sequence: Reference sequence all elements in *structures*
                         refer to
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :param structures: Structures to annotate
        :type structures: :class:`list` of :class:`Structure`
        :param seq_range: Optionally provide range in *sequence* you're
                          interested in, will be passed to :meth:`Run`. 
                          If :func:`PerResidue` returns True, No matter what
                          subclass returns, all annotations outside this range
                          will be set to None.
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: List containing len(*structures*) per-structure
                  annotations as described above
        :rtype: :class:`list`
        """
        if seq_range is not None:
            common.CheckRange(sequence, seq_range)

        if not (
            isinstance(sequence, seq.SequenceHandle)
            or isinstance(sequence, seq.ConstSequenceHandle)
        ):
            raise RuntimeError(
                "Expect sequence to be of type "
                "ost.seq.SequenceHandle or "
                "ost.seq.ConstSequenceHandle got " + str(type(sequence))
            )

        if not (isinstance(structures, list)):
            raise RuntimeError(
                "Expect variants to be of type list got "
                + str(type(structures))
            )

        for structure in structures:
            if not (isinstance(structure, Structure)):
                raise RuntimeError(
                    "Expect structure to be of type "
                    "var3d.base.Structure got " + str(type(structure))
                )
            if str(sequence) != str(structure.sequence):
                raise RuntimeError(
                    "Expect sequence of all structures to match "
                    "given reference sequence"
                )

        annotation = self.Run(sequence, structures, seq_range=seq_range)

        if not isinstance(annotation, list):
            raise RuntimeError(
                "StructAnno subclass must return list, "
                "got " + str(type(annotation))
            )

        if len(annotation) != len(structures):
            raise RuntimeError(
                "StructAnno subclass must return exactly one "
                "return list per input structure"
            )

        if self.PerResidue():
            for s, anno in zip(structures, annotation):
                if not isinstance(anno, list) or s.GetNumSegments() != len(
                    anno
                ):
                    raise RuntimeError(
                        "StructAnno subclass must return "
                        "per-structure annotations of type list "
                        "with same number of elements as segments "
                        "in the respective structure"
                    )
                for segment_anno in anno:
                    if len(segment_anno) != len(sequence):
                        raise RuntimeError(
                            "Per-segment annotations returned by "
                            "StructAnno subclass must have same "
                            "length as reference sequence"
                        )

            if seq_range is not None:
                for structure_idx, structure in enumerate(structures):
                    for segment_idx in range(structure.GetNumSegments()):
                        for idx in range(seq_range[0] - 1):
                            annotation[structure_idx][segment_idx][idx] = None
                        for idx in range(seq_range[1], len(sequence)):
                            annotation[structure_idx][segment_idx][idx] = None

        return annotation

    def PerResidue(self):
        """Must be implemented by child class

        :returns: Whether the annotations are on a per-residue basis
        :rtype: :class:`bool`
        """
        raise NotImplementedError("Incomplete child class")

    def Key(self):
        """Must be implemented by child class

        :returns: Identifier of annotation
        :rtype: :class:`str`
        """
        raise NotImplementedError("Incomplete child class")

    def Run(self, sequence, structures, seq_range=None):
        """Must be implemented by child class

        :param sequence: *sequence* param of :func:`Annotate`
        :type sequence: :class:`ost.seq.SequenceHandle` / 
                        :class:`ost.seq.ConstSequenceHandle`
        :param structures: *structures* param of :func:`Annotate`
        :type structures: :class:`list` of :class:`Structure`
        :param seq_range: *seq_range* paaram of :func:`Annotate`
        :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
        :returns: Annotation as described in :func:`Annotate`
        """
        raise NotImplementedError("Incomplete child class")


class VarSeqAnno:
    """General interface for sequence based variant annotation
    
    Subclasses assign per-variant annotations and must implement the
    :func:`Key` and :func:`Run` methods    
    """

    def __init__(self):
        pass

    def Annotate(self, sequence, variants):
        """Annotation for every variant in *variants* 

        Calls :func:`Run` which must be implemented by child class.

        :param sequence: Canonical sequence the variants refer to
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param variants: Variants to annotate
        :type variants: :class:`list` of :class:`Variant`
        :returns: :class:`list` with len(*variants*) elements
        """
        if not (
            isinstance(sequence, seq.SequenceHandle)
            or isinstance(sequence, seq.ConstSequenceHandle)
        ):
            raise RuntimeError(
                "Expect sequence to be of type "
                "ost.seq.SequenceHandle or "
                "ost.seq.ConstSequenceHandle got " + str(type(sequence))
            )

        if not (isinstance(variants, list)):
            raise RuntimeError(
                "Expect variants to be of type list got " + str(type(variants))
            )

        for v in variants:
            if not isinstance(v, Variant):
                raise RuntimeError(
                    "VarSeqAnno subclass expects variants to be "
                    "list of Variant objects. Got list "
                    "containing" + str(type(v))
                )

        for v in variants:
            v.MatchesSEQRES(sequence)  # raises in case of mismatch...

        annotations = self.Run(sequence, variants)

        if not isinstance(annotations, list):
            raise RuntimeError(
                "VarSeqAnno subclass must return list, "
                "got " + str(type(annotations))
            )

        if len(annotations) != len(variants):
            raise RuntimeError(
                f"VarSeqAnno subclass must return list with "
                f"same length as input variants (len(variants): "
                f"{len(variants)}, len(annotations): "
                f"{len(annotations)})"
            )

        return annotations

    def Key(self):
        """Must be implemented by child class

        :returns: Identifier of annotation
        :rtype: :class:`str`
        """
        raise NotImplementedError("Key must be implemented")

    def Run(self, sequence, variants):
        """Must be implemented by child class

        :param sequence: *sequence* param of :func:`Annotate`
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param variants: *variants* param of :func:`Annotate`
        :type variants: :class:`list` of :class:`Variant`
        :returns: Annotation as described in :func:`Annotate`
        """
        raise NotImplementedError("Incomplete child class")


class VarStructAnno:
    """General interface for structure based variant annotation
    
    Subclasses assign per-variant annotations and must implement the
    :func:`Key` and :func:`Run` methods    
    """

    def __init__(self):
        pass

    def Annotate(self, sequence, variants, structures):
        """Annotation for every combination of *variants*/*structures*

        Calls :func:`Run` which must be implemented by child class.

        Annotation describes the effect if a certain variant is applied to ALL
        segments in a structure. They refer to the same reference sequence
        after all. However, be aware: there is no guarantee that a certain
        residue is not covered by several segments.

        The return :class:`list` has len(*variants*) elements, each element is a
        :class:`list` with len(*structures*) elements. These variant/structure
        pairs can be None if the variant is not covered by that structure, or if
        assigned by subclass implementation. As an example: FoldX can only
        annotate variants of type :class:`VariantType.SUBSTITUTION`.
         
        :param sequence: Canonical sequence the variants refer to
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param variants: Variants to annotate
        :type variants: :class:`list` of :class:`Variant`
        :param structures: Structural info
        :type structures: :class:`list` of :class:`Structure`
        :returns: List containing len(*variants*) elements as described above
        :rtype: :class:`list`
        """
        if not (
            isinstance(sequence, seq.SequenceHandle)
            or isinstance(sequence, seq.ConstSequenceHandle)
        ):
            raise RuntimeError(
                "Expect sequence to be of type "
                "ost.seq.SequenceHandle or "
                "ost.seq.ConstSequenceHandle got " + str(type(sequence))
            )

        if not (isinstance(variants, list)):
            raise RuntimeError(
                "Expect variants to be of type list got " + str(type(variants))
            )

        for v in variants:
            if not isinstance(v, Variant):
                raise RuntimeError(
                    "VarStructAnno subclass expects variants to "
                    "be list of Variant object. Got list "
                    "containing" + str(type(v))
                )

        if not (isinstance(structures, list)):
            raise RuntimeError(
                "Expect structures to be of type list got "
                + str(type(structures))
            )

        for s in structures:
            if not isinstance(s, Structure):
                raise RuntimeError(
                    "VarStructAnno subclass expects structures "
                    "to be list of Structure objects. Got list "
                    "containing" + str(type(s))
                )

        for v in variants:
            v.MatchesSEQRES(sequence)  # raises in case of mismatch...

        for s in structures:
            if not str(sequence) == str(s.sequence):
                raise RuntimeError(
                    "Reference sequence of all structures must "
                    "match sequence"
                )

        annotations = self.Run(sequence, variants, structures)

        if not isinstance(annotations, list):
            raise RuntimeError(
                "VarStructAnno subclass must return list, "
                "got " + str(type(annotations))
            )

        if len(annotations) != len(variants):
            raise RuntimeError(
                f"VarStructAnno subclass must return list with "
                f"same length as input variants (len(variants): "
                f"{len(variants)}, len(annotations): "
                f"{len(annotations)})"
            )

        for v_anno in annotations:
            if not isinstance(v_anno, list):
                raise RuntimeError(
                    "VarStructAnno subclass must return list "
                    "for each variant got " + str(type(v_anno))
                )

            if len(v_anno) != len(structures):
                raise RuntimeError(
                    f"VarStructAnno subclass must return list "
                    f"for each variant that has the same length "
                    f" as structures  (len(variant_anno): "
                    f"{len(v_anno)}, len(structures): "
                    f"{len(structures)})"
                )

        return annotations

    def Key(self):
        """Must be implemented by child class

        :returns: Identifier of annotation
        :rtype: :class:`str`
        """
        raise NotImplementedError("Key must be implemented")

    def Run(self, sequence, variants, structures):
        """Must be implemented by child class

        :param sequence: *sequence* param of :func:`Annotate`
        :type sequence: :class:`ost.seq.SequenceHandle` /
                        :class:`ost.seq.ConstSequenceHandle`
        :param variants: *variants* param of :func:`Annotate`
        :type variants: :class:`list` of :class:`Variant`
        :param structures: *structures* param of :func:`Annotate`
        :type structures: :class:`list` of :class:`Structure`
        :returns: Annotation as described in :func:`Annotate`
        """
        raise NotImplementedError("Incomplete child class")
