import traceback
import json

import ost
from ost import seq

from var3d.base import *
from var3d import common


class _Logger(ost.LogSink):
    """Only logs errors, intended to catch the bad stuff from annotators
    """

    def __init__(self):
        ost.LogSink.__init__(self)
        self.error_messages = list()

    def LogMessage(self, message, severity):
        if severity == ost.LogLevel.Error:
            self.error_messages.append(message)

    def Clear(self):
        self.error_messages = list()


class DataContainer:
    """Stores variants/structures for specified sequence

    Objects can be retreived using indices or based on unique hashes

    :param sequence: Reference sequence all variants/structures refer to
    :type sequence: :class:`ost.seq.SequenceHandle` / 
                    :class:`ost.seq.ConstSequenceHandle`
    :param seq_identifier: Arbitrary identifier
    :type seq_identifier: :class:`str`
    :param seq_range: Range in *sequence* you're interested in, all
                      variants/structures must cover that range
    :type seq_range: :class:`tuple`/:class:`list` of :class:`int`
    """

    def __init__(self, sequence, seq_identifier=None, seq_range=None):

        # fine if seq_range is None
        common.CheckRange(sequence, seq_range)

        self.sequence = sequence
        self.seq_identifier = seq_identifier
        self.seq_range = seq_range

        # storage for variants and structures
        self.variants = list()
        self.structures = list()

        # when loaded from json, raw data are stored in here and only lazily
        # constructed.
        self.json_variants = list()
        self.json_structures = list()

        # the hashes of the variants and structures
        self.variant_hashes = list()
        self.structure_hashes = list()

        # dicts for fast mapping from hash to index
        self.variant_hash_mapper = dict()
        self.structure_hash_mapper = dict()

        # structure hashes that only depend on assembly - won't be saved/loaded
        # with JSON. Just available as auxiliary data which is lazily computed
        self.assembly_hashes = list()

    def GetSeqHash(self):
        """Unique hash identifer that depends only on sequence parameters

        Useful to check whether two :class:`DataContainer` refer to the same
        underlying data, i.e. self.sequence, self.seq_identifier, self.seq_range

        :returns: Unique hash identifier that only depends on underlying
                  sequence data
        :rtype: :class:`str`
        """
        data = "_".join(
            [
                self.sequence.GetName(),
                str(self.sequence),
                str(self.seq_identifier),
                str(self.seq_range),
            ]
        )
        return hashlib.md5(data.encode()).hexdigest()

    def GetNumStructures(self):
        """
        :returns: Number of structures stored in DataContainer
        :rtype: :class:`int`
        """
        return len(self.structures)

    def GetNumVariants(self):
        """
        :returns: Number of variants stored in DataContainer
        :rtype: :class:`int`
        """
        return len(self.variants)

    def GetStructure(self, idx):
        """
        :param idx: Index of stored structure
        :type idx: :class:`int`
        :returns: Structure at given idx
        :rtype: :class:`var3d.base.Structure`
        """
        if self.structures[idx] is None:
            self._StructureFromJSON(idx)  # only construct if needed
        return self.structures[idx]

    def GetStructureHash(self, idx):
        """
        :param idx: Index of stored structure
        :type idx: :class:`int`
        :returns: Structure hash at given idx
        :rtype: :class:`str`
        """
        return self.structure_hashes[idx]

    def GetAssemblyHash(self, idx):
        """
        :param idx: Index of stored structure
        :type idx: :class:`int`
        :returns: Hash of structure object at given idx which only
                  depends on its assembly (raw coordinates)
        :rtype: :class:`str`
        """
        if self.assembly_hashes[idx] is None:
            self.assembly_hashes[idx] = self.GetStructure(idx).GetAssemblyHash()
        return self.assembly_hashes[idx]

    def GetStructureIdxFromHash(self, h):
        """
        :param h: Hash of stored structure
        :type h: :class:`str`
        :returns: Idx for given structure hash 
        :rtype: :class:`int`
        :raises: :class:`RuntimeError` if structure hash doesn't exist
        """
        if h not in self.structure_hash_mapper:
            raise RuntimeError(f"No structure with hash {h} in data container")
        return self.structure_hash_mapper[h]

    def GetStructureFromHash(self, h):
        """
        :param h: Hash of stored structure
        :type h: :class:`str`
        :returns: Structure with given hash
        :rtype: :class:`var3d.base.Structure`
        :raises: :class:`RuntimeError` if no such :class:`Structure` exists
        """
        return self.GetStructure(self.GetStructureIdxFromHash(h))

    def GetVariant(self, idx):
        """
        :param idx: Index of stored variant
        :type idx: :class:`int`
        :returns: Variant at given idx
        :rtype: :class:`var3d.base.Variant`
        """
        if self.variants[idx] is None:
            self._VariantFromJSON(idx)  # only construct if needed
        return self.variants[idx]

    def GetVariantHash(self, idx):
        """
        :param idx: Index of stored variant
        :type idx: :class:`int`
        :returns: Variant hash at given idx
        :rtype: :class:`str`
        """
        return self.variant_hashes[idx]

    def GetVariantIdxFromHash(self, h):
        """
        :param h: Hash of stored variant
        :type h: :class:`str`
        :returns: Idx for given variant hash 
        :rtype: :class:`int`
        :raises: :class:`RuntimeError` if variant hash doesn't exist
        """
        if h not in self.variant_hash_mapper:
            raise RuntimeError(f"No variant with hash {h} in data container")
        return self.variant_hash_mapper[h]

    def GetVariantFromHash(self, h):
        """
        :param h: Hash of stored variant
        :type h: :class:`str`
        :returns: Variant with given hash
        :rtype: :class:`var3d.base.Variant`
        :raises: :class:`RuntimeError` if no such :class:`Variant` exists
        """
        return self.GetVariant(self.GetVariantIdxFromHash(h))

    def AddStructure(self, structure):
        """Add :class:`var3d.base.Structure` to container

        :param structure: Structure to add
        :type structure: :class:`var3d.base.Structure`
        :returns: Unique hash of *structure*
        :rtype: :class:`str`
        :raises: :class:`RuntimeError` if *structure* does not cover seq_range
                 specified at container construction, the reference sequence
                 in the structure does not match the reference sequence of the
                 container or when a structure with equal hash is already
                 present.
        """
        if not structure.InRange(self.seq_range):
            raise RuntimeError(
                "structure doesn't cover seq_range provided at "
                "DataContainer construction"
            )
        if str(self.sequence) != str(structure.sequence):
            raise RuntimeError(
                "reference sequences from structure and " "container differ."
            )
        h = structure.GetHash()
        if h in self.structure_hash_mapper:
            raise RuntimeError("Structure with equal hash already in container")
        self.structure_hashes.append(h)
        self.assembly_hashes.append(None)  # lazy eval
        self.structure_hash_mapper[h] = len(self.structures)
        self.structures.append(structure)
        self.json_structures.append(None)
        return h

    def AddVariant(self, variant):
        """Add :class:`var3d.base.Variant` to container

        :param variant: Variant to add
        :type variant: :class:`var3d.base.Variant`
        :returns: Unique hash of *variant*
        :rtype: :class:`str`
        :raises: :class:`RuntimeError` if *variant* does not cover seq_range
                 specified at container construction or when a variant with
                 equal hash is already present.
        """
        if not variant.InRange(self.seq_range):
            raise RuntimeError(
                "variant doesn't cover seq_range provided at "
                "DataContainer construction"
            )
        h = variant.GetHash()
        if h in self.variant_hash_mapper:
            raise RuntimeError("Variant with equal hash already in container")
        self.variant_hashes.append(h)
        self.variant_hash_mapper[h] = len(self.variants)
        self.variants.append(variant)
        self.json_variants.append(None)
        return h

    def ToJSON(self):
        """
        :returns: JSON serializable dictionary containing all data
        :rtype: :class:`dict`
        """
        return_dict = dict()

        return_dict["sequence"] = str(self.sequence)
        return_dict["sequence_name"] = self.sequence.GetName()
        return_dict["sequence_identifier"] = self.seq_identifier
        return_dict["seq_range"] = self.seq_range
        return_dict["variants"] = list()
        for v_idx, v in enumerate(self.variants):
            if self.json_variants[v_idx] is not None:
                return_dict["variants"].append(self.json_variants[v_idx])
            else:
                return_dict["variants"].append(v.ToJSON())
        return_dict["structures"] = list()
        for s_idx, s in enumerate(self.structures):
            if self.json_structures[s_idx] is not None:
                return_dict["structures"].append(self.json_structures[s_idx])
            else:
                return_dict["structures"].append(s.ToJSON())

        # only store the hash lists, the dicts can be reconstructed on the fly
        return_dict["variant_hashes"] = self.variant_hashes
        return_dict["structure_hashes"] = self.structure_hashes
        return return_dict

    @staticmethod
    def FromJSON(json_data):
        """Creates :class:`DataContainer` from json serialized data

        :param json_data: Data to load, created with :func:`ToJSON`
        :type json_data: :class:`dict`
        :returns: The created container
        :rtype: :class:`DataContainer`
        """

        expected_keys = [
            "sequence",
            "sequence_name",
            "sequence_identifier",
            "seq_range",
            "variants",
            "structures",
            "variant_hashes",
            "structure_hashes",
        ]
        for k in expected_keys:
            if k not in json_data:
                raise RuntimeError(f'Expected "{k}" key in json_data')
        if len(json_data["variants"]) != len(json_data["variant_hashes"]):
            raise RuntimeError("Inconsistent input sizes in json_data")
        if len(json_data["structures"]) != len(json_data["structure_hashes"]):
            raise RuntimeError("Inconsistent input sizes in json_data")

        sequence = seq.CreateSequence(
            json_data["sequence_name"], json_data["sequence"]
        )
        seq_identifier = json_data["sequence_identifier"]
        seq_range = json_data["seq_range"]
        container = DataContainer(
            sequence, seq_identifier=seq_identifier, seq_range=seq_range
        )
        container.json_variants = json_data["variants"]
        container.variants = [None] * len(container.json_variants)
        container.json_structures = json_data["structures"]
        container.structures = [None] * len(container.json_structures)
        container.variant_hashes = json_data["variant_hashes"]
        container.structure_hashes = json_data["structure_hashes"]
        container.assembly_hashes = [None] * len(container.structure_hashes)
        for h_idx, h in enumerate(container.variant_hashes):
            container.variant_hash_mapper[h] = h_idx
        for h_idx, h in enumerate(container.structure_hashes):
            container.structure_hash_mapper[h] = h_idx
        return container

    def _VariantFromJSON(self, idx):
        # pseudo lazy loading - Data is already there, wait with object creation
        self.variants[idx] = Variant.FromJSON(self.json_variants[idx])
        self.variants[idx].SetHash(self.variant_hashes[idx])

    def _StructureFromJSON(self, idx):
        # pseudo lazy loading - Data is already there, wait with object creation
        self.structures[idx] = Structure.FromJSON(self.json_structures[idx])
        self.structures[idx].SetHash(self.structure_hashes[idx])


class DataImportPipeline:
    """Pipeline to import variants and structures

    :param var_importer: One or several importers of variants
    :type var_importer: :class:`var3d.base.VariantImporter` or :class:`list` of
                        :class:`var3d.base.VariantImporter`
    :param struct_importer: One or several importers of structures
    :type struct_importer: :class:`var3d.base.StructImporter` or :class:`list`
                           of :class:`var3d.base.StructImporter`
    :raises: :class:`ValueError` if *var_importer* or *struct_importer* are not
             of expected type.
    """

    def __init__(self, var_importer=list(), struct_importer=list()):

        if not isinstance(var_importer, list):
            var_importer = [var_importer]
        for importer in var_importer:
            if not isinstance(importer, VarImporter):
                raise ValueError(
                    "All var_importer items must be of type " "VarImporter"
                )
        self.var_importer = var_importer

        if not isinstance(struct_importer, list):
            struct_importer = [struct_importer]
        for importer in struct_importer:
            if not isinstance(importer, StructImporter):
                raise ValueError(
                    "All struct_importer items must be of type "
                    "StructImporter"
                )
        self.struct_importer = struct_importer

    def Import(self, data_container):
        """Imports structures/variants specific to *data_container*

        All variant and structure importers given at initialization are called
        with data specified in *data_container*. Imported objects are directly
        added to *data_container*

        :param data_container: From which reference sequence is extracted to
                               call importers, imported objects are directly
                               added to it.
        :type data_container: :class:`DataContainer`
        :returns: Updated *data_container* for convenience (stuff is added
                  in-place).
        :rtype: :class:`DataContainer`
        """
        # import variants
        variants = list()
        for importer in self.var_importer:
            variants += importer.Import(
                data_container.sequence, seq_range=data_container.seq_range
            )
        for v in variants:
            data_container.AddVariant(v)

        # import structures
        structures = list()
        for importer in self.struct_importer:
            structures += importer.Import(
                data_container.sequence, seq_range=data_container.seq_range
            )
        for s in structures:
            data_container.AddStructure(s)

        return data_container


def GetEmptyAnno():
    """Returns empty annotation dictionary

    Same keys as returned by :func:`AnnotationPipeline.Run` but without any data

    :returns: The dict
    """
    return {
        "seq_anno": {},
        "var_seq_anno": {},
        "struct_anno": {},
        "var_struct_anno": {},
        "err": [],
    }


class AnnotationPipeline:
    """Bring it all together
    
    Generates JSON serializable annotations for sequences and their
    variants/structures

    :param seq_anno: One or several sequence annotations
    :type seq_anno: :class:`var3d.base.SeqAnno` or :class:`list` of 
                    :class:`var3d.base.SeqAnno`
    :param var_seq_anno: One or several variant annotations based on sequence
    :type var_seq_anno: :class:`var3d.base.VarSeqAnno` or :class:`list` of 
                        :class:`var3d.base.VarSeqAnno`
    :param struct_anno: One or several structure annotations
    :type struct_anno: :class:`var3d.base.StructAnno` or :class:`list` of 
                       :class:`var3d.base.StructAnno`
    :param var_struct_anno: One or several variant annotations based on 
                            structure
    :type var_struct_anno: :class:`var3d.base.VarStructAnno` or :class:`list` of
                           :class:`var3d.base.VarStructAnno`
    :raises: :class:`ValueError` if any of the annotation parameters is not of
             expected type. :class:`RuntimeError` if any of the annotation
             parameters contains duplicate keys, e.g. two annotation objects in
             *seq_anno* return "yolo" for :func:`var3d.base.SeqAnno.Key`
    """

    def __init__(
        self,
        seq_anno=list(),
        var_seq_anno=list(),
        struct_anno=list(),
        var_struct_anno=list(),
    ):

        if not isinstance(seq_anno, list):
            seq_anno = [seq_anno]
        for anno in seq_anno:
            if not isinstance(anno, SeqAnno):
                raise ValueError("All seq_anno items must be of type SeqAnno")
        keys = [anno.Key() for anno in seq_anno]
        if len(keys) != len(set(keys)):
            raise RuntimeError("All keys of seq_anno items must be unique")
        self.seq_anno = seq_anno

        if not isinstance(var_seq_anno, list):
            var_seq_anno = [var_seq_anno]
        for anno in var_seq_anno:
            if not isinstance(anno, VarSeqAnno):
                raise ValueError(
                    "All var_seq_anno items must be of type " "VarSeqAnno"
                )
        keys = [anno.Key() for anno in var_seq_anno]
        if len(keys) != len(set(keys)):
            raise RuntimeError("All keys of var_seq_anno items must be unique")
        self.var_seq_anno = var_seq_anno

        if not isinstance(struct_anno, list):
            struct_anno = [struct_anno]
        for anno in struct_anno:
            if not isinstance(anno, StructAnno):
                raise ValueError(
                    "All struct_anno items must be of type " "StructAnno"
                )
        keys = [anno.Key() for anno in struct_anno]
        if len(keys) != len(set(keys)):
            raise RuntimeError("All keys of struct_anno items must be unique")
        self.struct_anno = struct_anno

        if not isinstance(var_struct_anno, list):
            var_struct_anno = [var_struct_anno]
        for anno in var_struct_anno:
            if not isinstance(anno, VarStructAnno):
                raise ValueError(
                    "All var_struct_anno items must be of type " "VarStructAnno"
                )
        keys = [anno.Key() for anno in var_struct_anno]
        if len(keys) != len(set(keys)):
            raise RuntimeError(
                "All keys of var_struct_anno items must be unique"
            )
        self.var_struct_anno = var_struct_anno

    def Run(self, data_container, variant_hashes=None, structure_hashes=None):
        """Performs computations for data in *data_container* using annotation
        objects specified at initialisation.

        The return dictionary contains the following keys:

        * err - :class:`list` of occured errors when calling annotation objects. 
                Every error item is a dict with keys 

                * anno_type - e.g. "seq_anno"
                * anno_key - Key() return value of affected annotation object
                * err_msg - The error message
                * traceback - Full traceback as :class:`str`

                If an error occured, the respective annotation of the affected
                annotation object is set to None. Errors that are logged by the
                annotation object itself, i.e. ost.LogError(err_msg) are also
                added here. traceback is None in this case.

        * seq_anno - :class:`dict` with :func:`var3d.base.SeqAnno.Key` as keys
                     and the respective return value of
                     :func:`var3d.base.SeqAnno.Annotate` as values
        * struct_anno - :class:`dict` with :func:`var3d.base.StructAnno.Key` as
                        keys and the respective structure annotation as values.
                        A structure annotation is yet again a :class:`dict` with
                        structure hashes as key and the structure annotation as
                        value.
        * var_seq_anno - :class:`dict` with :func:`var3d.base.VarSeqAnno.Key` as
                         keys and the respective annotation as values.
                         The annotation is yet again a :class:`dict` with
                         variant hashes as key and the variant annotation as
                         value.
        * var_struct_anno - :class:`dict` with 
                            :func:`var3d.base.VarStructAnno.Key` as keys and the
                            respective annotation as values.
                            The annotation is a :class:`dict` of :class:`dict`,
                            i.e. you get the annotation of variant with hash h_v
                            on structure with hash h_s from annotator with key 
                            yolo as follows: 
                            :code:`data['var_struct_anno']['yolo'][v_h][s_h]`
        
        :param data_container: Contains sequence and associated 
                               structures/variants
        :type data_container: :class:`var3d.pipeline.DataContainer`
        :param variant_hashes: Hashes of variants in *data_container* for which
                               computations should be performed. If not given,
                               all are processed.
        :type variant_hashes: :class:`list` of :class:`str`
        :param structure_hashes: Hashes of structures in *data_container* for
                                 which computations should be performed. If not
                                 given, all are processed.
        :type structure_hashes: :class:`list` of :class:`str`
        :returns: Standardized dict containing the computation results as
                  described above
        :rtype: :class:`dict`
        """
        if variant_hashes is None:
            variant_hashes = data_container.variant_hashes
        if structure_hashes is None:
            structure_hashes = data_container.structure_hashes
        variants = [
            data_container.GetVariantFromHash(h) for h in variant_hashes
        ]
        structures = [
            data_container.GetStructureFromHash(h) for h in structure_hashes
        ]
        return_dict = GetEmptyAnno()

        # setup logger that catches errors from annotators
        logger = _Logger()
        ost.PushLogSink(logger)

        # perform sequence annotations
        for annotator in self.seq_anno:
            try:
                anno = annotator.Annotate(
                    data_container.sequence, seq_range=data_container.seq_range
                )
                return_dict["seq_anno"][annotator.Key()] = anno
            except Exception as e:
                return_dict["seq_anno"][annotator.Key()] = None
                return_dict["err"].append(
                    {
                        "anno_type": "seq_anno",
                        "anno_key": annotator.Key(),
                        "err_msg": str(e),
                        "traceback": traceback.format_exc(),
                    }
                )
            # process potential other errors that have been logged by anno
            return_dict["err"] += self._ProcessLogger(
                logger, "seq_anno", annotator.Key()
            )
            logger.Clear()

        # annotate variants using sequence only tools
        for annotator in self.var_seq_anno:
            try:
                annotations = annotator.Annotate(
                    data_container.sequence, variants
                )
                tmp = dict()
                for h, a in zip(variant_hashes, annotations):
                    tmp[h] = a
                return_dict["var_seq_anno"][annotator.Key()] = tmp
            except Exception as e:
                return_dict["var_seq_anno"][annotator.Key()] = None
                return_dict["err"].append(
                    {
                        "anno_type": "var_seq_anno",
                        "anno_key": annotator.Key(),
                        "err_msg": str(e),
                        "traceback": traceback.format_exc(),
                    }
                )
            # process potential other errors that have been logged by anno
            return_dict["err"] += self._ProcessLogger(
                logger, "var_seq_anno", annotator.Key()
            )
            logger.Clear()

        # perform structure annotations
        for annotator in self.struct_anno:
            try:
                annotations = annotator.Annotate(
                    data_container.sequence,
                    structures,
                    seq_range=data_container.seq_range,
                )
                tmp = dict()
                for h, a in zip(structure_hashes, annotations):
                    tmp[h] = a
                return_dict["struct_anno"][annotator.Key()] = tmp
            except Exception as e:
                return_dict["struct_anno"][annotator.Key()] = None
                return_dict["err"].append(
                    {
                        "anno_type": "struct_anno",
                        "anno_key": annotator.Key(),
                        "err_msg": str(e),
                        "traceback": traceback.format_exc(),
                    }
                )
            # process potential other errors that have been logged by anno
            return_dict["err"] += self._ProcessLogger(
                logger, "struct_anno", annotator.Key()
            )
            logger.Clear()

        # annotate variants using tools considering structures
        for annotator in self.var_struct_anno:
            try:
                annotations = annotator.Annotate(
                    data_container.sequence, variants, structures
                )
                tmp = dict()
                for v_idx, v_hash in enumerate(variant_hashes):
                    if v_hash not in tmp:
                        tmp[v_hash] = dict()
                    for s_idx, s_hash in enumerate(structure_hashes):
                        tmp[v_hash][s_hash] = annotations[v_idx][s_idx]
                return_dict["var_struct_anno"][annotator.Key()] = tmp
            except Exception as e:
                return_dict["var_struct_anno"][annotator.Key()] = None
                return_dict["err"].append(
                    {
                        "anno_type": "var_struct_anno",
                        "anno_key": annotator.Key(),
                        "err_msg": str(e),
                        "traceback": traceback.format_exc(),
                    }
                )
            # process potential other errors that have been logged by anno
            return_dict["err"] += self._ProcessLogger(
                logger, "var_struct_anno", annotator.Key()
            )
            logger.Clear()

        # done logging
        ost.PopLogSink()

        return return_dict

    def _ProcessLogger(self, logger, anno_type, anno_key):
        """Processes logger
        """
        return_list = list()
        for m in logger.error_messages:
            return_list.append(
                {
                    "anno_type": anno_type,
                    "anno_key": anno_key,
                    "err_msg": m,
                    "traceback": None,
                }
            )
        return return_list


def MergeAnnotations(dest, source):
    """Merges two annotations that refer to same DataContainer

    Raises an error in case of conflicting data:

    * seq_anno - More than one annotations with same key
    * var_seq_anno - More than one annotation with same variant hash and same
                     annotation key
    * struct_anno - More than one annotation with same structure hash and same
                    annotation key
    * var_struct_anno - More than one annotation with same structure/variant
                        hash pair and same annotation key

    :param dest: Reference annotation that has been generated with
                 :func:`AnnotationPipeline.Run`. Annotations from *source*
                 will be pulled in and the dictionary is returned.
    :type dest: :class:`dict`
    :param source: Annotation that has been generated with
                   :func:`AnnotationPipeline.Run`. All annotations are added
                   to *dest*.
    :type source: :class:`dict`

    :returns: *dest* dictionary with annotations merged in from *source*
    :rtype: :class:`dict`
    :raises: :class:`RuntimeError` in case of described conflicts
    """
    # do err
    dest["err"] += source["err"]

    # do seq_anno
    # instead of just updating, we check for conflicts first
    for key in source["seq_anno"].keys():
        if key in dest["seq_anno"]:
            raise RuntimeError("Conflicting annotation in seq_anno")
    dest["seq_anno"].update(source["seq_anno"])

    # do var_seq_anno
    for key in source["var_seq_anno"].keys():
        if key in dest["var_seq_anno"]:
            # already annotations with that key present, let's check for
            # conflicts before updating
            for v_h in source["var_seq_anno"][key].keys():
                if v_h in dest["var_seq_anno"][key]:
                    raise RuntimeError("Conflicting annotation in var_seq_anno")
            dest["var_seq_anno"][key].update(source["var_seq_anno"][key])
        else:
            dest["var_seq_anno"][key] = source["var_seq_anno"][key]

    # do struct_anno
    for key in source["struct_anno"].keys():
        if key in dest["struct_anno"]:
            # already annotations with that key present, let's check for
            # conflicts before updating
            for s_h in source["struct_anno"][key].keys():
                if s_h in dest["struct_anno"][key]:
                    raise RuntimeError("Conflicting annotation in struct_anno")
            dest["struct_anno"][key].update(source["struct_anno"][key])
        else:
            dest["struct_anno"][key] = source["struct_anno"][key]

    # do var_struct_anno
    for key in source["var_struct_anno"]:
        if key in dest["var_struct_anno"]:
            for v_h in source["var_struct_anno"][key].keys():
                if v_h in dest["var_struct_anno"][key]:
                    # already variant annotations for that key present, lets
                    # check for conflicts
                    for s_h in source["var_struct_anno"][key][v_h].keys():
                        if s_h in dest["var_struct_anno"][key][v_h]:
                            raise RuntimeError(
                                "Conflicting annotation in " "var_struct_anno"
                            )
                    dest["var_struct_anno"][key][v_h].update(
                        source["var_struct_anno"][key][v_h]
                    )
                else:
                    dest["var_struct_anno"][key][v_h] = source[
                        "var_struct_anno"
                    ][key][v_h]
        else:
            dest["var_struct_anno"][key] = source["var_struct_anno"][key]

    return dest


def CollectAnnotations(anno_files):
    """Collects annotations from several files that refer to same DataContainer

    Raises an error in case of conflicting data:

    * seq_anno - More than one annotations with same key
    * var_seq_anno - More than one annotation with same variant hash and same
                     annotation key
    * struct_anno - More than one annotation with same structure hash and same
                    annotation key
    * var_struct_anno - More than one annotation with same structure/variant
                        hash pair and same annotation key

    :param anno_files: Paths to JSON serialized annotation files generated with
                       :func:`AnnotationPipeline.Run`.
    :type anno_files: :class:`str`
    :returns: Annotation dictionary in same formatting as generated with
              :func:`AnnotationPipeline.Run` containing the concatenated
              annotations.
    :rtype: :class:`dict`
    :raises: :class:`RuntimeError` in case of described conflicts
    """

    with open(anno_files[0], "r") as fh:
        anno_dict = json.load(fh)

    for f in anno_files[1:]:
        with open(f, "r") as fh:
            data = json.load(fh)
        anno_dict = MergeAnnotations(anno_dict, data)

    return anno_dict
