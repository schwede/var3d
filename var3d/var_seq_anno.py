import os
import subprocess
import tempfile
import hashlib

from ost import settings
from ost import io
from ost.seq.alg import aaindex

from var3d.base import *


class ProveanVarSeqAnno(VarSeqAnno):
    """Implementation of VarSeqAnno - Variant annotation based on provean. 

    Choi Y, Sims GE, Murphy S, Miller JR, Chan AP Predicting the Functional 
    Effect of Amino Acid Substitutions and Indels. PLoS ONE 7(10) (2012)

    Returns provean scores for variants of type 
    :class:`var3d.base.VariantType.SUBSTITUTION`, 
    :class:`var3d.base.VariantType.INSERTION`, 
    :class:`var3d.base.VariantType.DELETION` or
    :class:`var3d.base.VariantType.INDEL`. Annotation will be None for all other
    types.

    :param blast_nrdb: Path to BLAST nr database searched by provean. Let's say
                       the db has the prefix *nr*, you'll have many files that 
                       match: <SOME_PATH>/nr* => blast_nrdb is <SOME_PATH>/nr
    :type blast_nrdb: :class:`str`
    :param key: Return value of parent class Key function, default: "provean"
    :type key: :class:`str`
    :param provean_exe: Path to provean executable, defaults to location in 
                        var3d Singularity container
    :type provean_exe: :class:`str`
    :param psiblast_exe: Path to psiblast executable, defaults to location in 
                         var3d Singularity container, provean really really 
                         wants version 2.4.0 here (don't ask)
    :type psiblast_exe: :class:`str`
    :param cdhit_exe: Path to cdhit executable, defaults to location in 
                      var3d Singularity container
    :type cdhit_exe: :class:`str`
    :param blastdbcmd_exe: Path to blastdbcmd executable, defaults to location
                           in var3d Singularity container, provean really really 
                           wants version 2.4.0 here (don't ask)
    :type blastdbcmd_exe: :class:`str`
    :param tmp_dir: Directory to dump input for the provean executable, will
                    also be used by provean as tmpdir. Will be created 
                    (and cleaned up) if not provided.
    :type tmp_dir: :class:`str`
    :param load_from_cache_callback: If given, this callback function with
                                     interface f(sequence, filepath) is called
                                     for every Provean run. The function should
                                     copy the supporting set file 
                                     (see *save_from_cache_callback*) specific
                                     for *sequence* to *filepath*. In case of
                                     success, i.e. the file exists after
                                     calling, *filepath* is passed as
                                     "--supporting_set" to the Provean
                                     executable. This will save time for BLAST
                                     search and clustering.
    :type load_from_cache_callback: function with exactly two arguments
    :param save_to_cache_callback: If given and a potential loading from cache
                                   was not successful, the Provean executable is
                                   called with "--save_supporting_set".
                                   Subsequently this callback with interface
                                   f(sequence, filepath) is called to enable
                                   a user defined caching mechanism of the
                                   *sequence* specific supporting set file at
                                   *filepath*.
    :type save_to_cache_callback: function with exactly two arguments
    :raises: :class:`ValueError` if provided *blast_nrdb* doesn't exist,
             :class:`ValueError` if any provided executable does not
             exist (or is not executable), 
             :class:`RuntimeError` if any executable which is not provided 
             cannot be found in the search path.
    """

    def __init__(
        self,
        blast_nrdb,
        key="provean",
        provean_exe="/provean/bin/provean",
        psiblast_exe="/provean/dependencies/psiblast",
        cdhit_exe="/usr/bin/cd-hit",
        blastdbcmd_exe="/provean/dependencies/blastdbcmd",
        tmp_dir=None,
        load_from_cache_callback=None,
        save_to_cache_callback=None,
    ):

        self.blast_nrdb_dirname = os.path.dirname(blast_nrdb)
        self.blast_nrdb_prefix = os.path.basename(blast_nrdb)
        if not os.path.exists(self.blast_nrdb_dirname):
            raise ValueError(
                f"Directory containing specified BLAST db does "
                f"not exist ({self.blast_nrdb_dirname})"
            )
        blast_files = os.listdir(self.blast_nrdb_dirname)
        n = 0
        for f in blast_files:
            if f.startswith(self.blast_nrdb_prefix) and \
               os.path.isfile(os.path.join(self.blast_nrdb_dirname,f)):
               n += 1
        if n == 0:
            raise ValueError(
                f"No files starting with expected prefix "
                f"({self.blast_nrdb_prefix}) in "
                f"{self.blast_nrdb_dirname}"
            )

        self.key = key

        if tmp_dir:
            if not os.path.exists(tmp_dir):
                os.makedirs(tmp_dir)
            self.tmp_dir = tmp_dir
        else:
            # as soon as self.tmp_dir_context is garabage collected,
            # the directory is deleted on disk
            self.tmp_dir_context = tempfile.TemporaryDirectory()
            self.tmp_dir = self.tmp_dir_context.name

        if provean_exe is None:
            self.provean_exe = settings.Locate("provean")
        else:
            if not os.access(provean_exe, os.X_OK):
                raise ValueError(
                    f"Provided provean_exe ({provean_exe})) "
                    f"does not exist or is not executable"
                )
            self.provean_exe = provean_exe

        if psiblast_exe is None:
            self.psiblast_exe = settings.Locate("psiblast")
        else:
            if not os.access(psiblast_exe, os.X_OK):
                raise ValueError(
                    f"Provided psiblast_exe ({psiblast_exe}) "
                    f"does not exist or is not executable"
                )
            self.psiblast_exe = psiblast_exe

        if blastdbcmd_exe is None:
            self.blastdbcmd_exe = settings.Locate("blastdbcmd")
        else:
            if not os.access(blastdbcmd_exe, os.X_OK):
                raise ValueError(
                    f"Provided blastdbcmd_exe "
                    f"({blastdbcmd_exe}) does not exist or "
                    f"is not executable"
                )
            self.blastdbcmd_exe = blastdbcmd_exe

        if cdhit_exe is None:
            self.cdhit_exe = settings.Locate("cd-hit")
        else:
            if not os.access(cdhit_exe, os.X_OK):
                raise ValueError(
                    f"Provided cdhit_exe ({cdhit_exe}) does "
                    f"not exist or is not executable"
                )
            self.cdhit_exe = cdhit_exe

        self.load_from_cache_callback = load_from_cache_callback
        self.save_to_cache_callback = save_to_cache_callback

    def Key(self):
        """Implementation of parent interface
        """
        return self.key

    def Run(self, sequence, variants):
        """Implementation of parent interface
        """
        # PREPARE
        #########
        seq_file, var_file, var_encodings, supporting_set_file = self._PrepareProveanInput(
            sequence, variants
        )

        cmd = self._GetCommand(seq_file, var_file, supporting_set_file)

        # RUN
        #####
        p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if p.returncode != 0:
            raise RuntimeError("Provean failed")  # THAT COULD BE IMPROVED

        # check whether we have a save to cache callback and whether we
        # created a new supporting set to cache
        if (
            self.save_to_cache_callback is not None
            and "--save_supporting_set" in cmd
            and os.path.exists(supporting_set_file)
        ):
            self.save_to_cache_callback(sequence, supporting_set_file)

        # PARSE AND RETURN
        ##################
        scores = self._ParseProveanResults(p.stdout.decode().splitlines())
        encoding_mapper = {item[1]: item[0] for item in var_encodings}
        return_list = [None] * len(variants)
        for encoding, score in scores.items():
            if encoding in encoding_mapper:
                return_list[encoding_mapper[encoding]] = score
            else:
                raise RuntimeError(
                    f"Provean provides results for {encoding} "
                    f"which was not requested..."
                )
        if len(scores) != len(encoding_mapper):
            for k in encoding_mapper.keys():
                if k not in scores:
                    raise RuntimeError(
                        f"Provean did not provide requested "
                        f"result for variant {k}"
                    )

        return return_list

    def _GetProveanInputEncoding(self, variants):
        """Iterates variants and returns an encoding that can be used as provean 
        input for every :class:`var3d.base.VariantType.SUBSTITUTION`, 
        :class:`VariantType.INSERTION`, :class:`VariantType.DELETION` and
        :class:`VariantType.INDEL`. 

        :param sequence: Canonical target sequence
        :type sequence: :class:`ost.seq.SequenceHandle`/
                        :class:`ost.seq.ConstSequenceHandle`
        :param variants: List of variants to process
        :type variants: :class:`list` of :class:`Variant`
        :returns: :class:`list` of :class:`tuple`. One tuple for each of the 
                  aforementioned variant types. First element: :class:`int` 
                  index in *variants*, second element: :class:`str` describing
                  the variant in Provean input style.
        """
        return_list = list()
        for v_idx, v in enumerate(variants):
            start_pos = None
            ref = None
            alt = None
            if v.variant_type in [
                VariantType.SUBSTITUTION,
                VariantType.INSERTION,
                VariantType.INDEL,
            ]:
                start_pos = v.pos
                ref = v.ref
                alt = v.alt
            elif v.variant_type == VariantType.DELETION:
                start_pos = v.pos
                ref = v.ref
                alt = "."
            if start_pos and ref and alt:
                return_list.append((v_idx, f"{start_pos},{ref},{alt}"))
        return return_list

    def _PrepareProveanInput(self, sequence, variants):
        """Prepares input 
        
        dumps two files required by Provean into self.tmp_dir: 
        (1) sequence in fasta format (2) variant specifications.

        :param sequence: Canonical target sequence
        :type sequence: :class:`ost.seq.SequenceHandle`/
                        :class:`ost.seq.ConstSequenceHandle`
        :param variants: List of variants to process
        :type variants: :class:`list` of :class:`Variant`
        :returns: :class:`tuple` with 3 elements: (1) path to sequence file,
                  (2) path to variant specifications (3) return value of 
                  :func:`GetProveanEncoding`.
        """
        # dump sequence file
        tmp = "_".join([sequence.GetName(), sequence.GetGaplessString()])
        seq_hash = hashlib.md5(tmp.encode()).hexdigest()
        seq_file = os.path.join(self.tmp_dir, seq_hash + ".fasta")
        io.SaveSequence(sequence, seq_file)

        # dump variant_file
        var_encodings = self._GetProveanInputEncoding(variants)
        tmp = "_".join([item[1] for item in var_encodings])
        var_hash = hashlib.md5(tmp.encode()).hexdigest()
        var_file = os.path.join(self.tmp_dir, var_hash + ".var")
        with open(var_file, "w") as fh:
            fh.write("\n".join([item[1] for item in var_encodings]))

        # check whether callback has been registered to potentially
        # use cached supporting_set
        supporting_set_file = os.path.join(
            self.tmp_dir, seq_hash + "_supporting_set.dat"
        )
        if self.load_from_cache_callback is not None:
            self.load_from_cache_callback(sequence, supporting_set_file)

        return (seq_file, var_file, var_encodings, supporting_set_file)

    def _GetCommand(self, seq_file, var_file, supporting_set_file):
        """Concatenates executable command 

        :param seq_file: Sequence file that has been dumped with 
                         :func:`_PrepareProveanInput`
        :param var_file: Variant specification file that has been dumped with
                         :func:`_PrepareProveanInput`
        :returns: :class:`list` of :class:`str` containing the command.
        """
        cmd = [
            self.provean_exe,
            "-d",
            os.path.join(self.blast_nrdb_dirname, self.blast_nrdb_prefix),
            "-q",
            seq_file,
            "-v",
            var_file,
            "--cdhit",
            self.cdhit_exe,
            "--psiblast",
            self.psiblast_exe,
            "--blastdbcmd",
            self.blastdbcmd_exe,
            "--tmp_dir",
            self.tmp_dir,
        ]

        if os.path.exists(supporting_set_file):
            cmd += ["--supporting_set", supporting_set_file]
        elif self.save_to_cache_callback is not None:
            # doesn't exist yet but there is a save to cache callback
            cmd += ["--save_supporting_set", supporting_set_file]

        return cmd

    def _ParseProveanResults(self, provean_out):
        """Parses Provean output

        :param provean_out: Some iterable that spits out the provean output 
                            line by line
        :returns: :class:`dict` with key: variant descriptor as 
                  generated from :func:`_GetProveanInputEncoding` and value:
                  the provean score.
        """
        return_dict = dict()
        hot = False
        for line in provean_out:
            if line.startswith("## PROVEAN scores"):
                hot = True
            if not hot or line.startswith("#"):
                continue
            split_line = line.strip().split()
            if len(split_line) == 2:
                return_dict[split_line[0]] = float(split_line[1])
        return return_dict


class AAIndexVarSeqAnno(VarSeqAnno):
    """Implementation of VarSeqAnno - Variant annotation based on the AAindex DB

    Kawashima, S. and Kanehisa, M.; AAindex: amino acid index database. 
    Nucleic Acids Res. 28, 374 (2000).

    Annotation will be None if the according variant is any other type than 
    :class:`var3d.base.VariantType.SUBSTITUTION`. The AAindex database entries
    come in two flavours. Single amino acid scores 
    (:class:`ost.seq.alg.aaindex.AnnoType.SINGLE`)
    and pairwise scores (:class:`ost.seq.alg.aaindex.AnnoType.PAIR`). For the
    former, the annotation will be a dict in format
    {"ref": score, "alt": score}, for the latter the item will be a float.

    :param aaindex_ac: AAindex database accesion number (e.g.ANDN920101)
    :type aaindex_ac: :class:`str`
    :param key: Return value of parent class Key function
    :type key: :class:`str`
    :param aaindex_files: Passed to ost.seq.alg.aaindex.AAIndex constructor,
                          default database is used if not given (recommended).
    :type aaindex_files: :class:`list` of :class:`str`
    """

    def __init__(self, aaindex_ac, key, aaindex_files=None):
        self.key = key
        self.index = aaindex.AAIndex(aaindex_files=aaindex_files)
        self.index_data = self.index[aaindex_ac]

    def Key(self):
        """Implementation of parent interface.
        """
        return self.key

    def Run(self, sequence, variants):
        """Implementation of parent interface. 
        """
        return_list = list()
        for v in variants:
            if v.variant_type != "SUBSTITUTION":
                return_list.append(None)
            else:
                if self.index_data.anno_type == aaindex.AnnoType.SINGLE:
                    try:
                        ref_score = self.index_data.GetScore(v.ref)
                    except:
                        ref_score = None  # olc not in aaindex
                    try:
                        alt_score = self.index_data.GetScore(v.alt)
                    except:
                        alt_score = None  # olc not in aaindex
                    return_list.append({"ref": ref_score, "alt": alt_score})
                else:
                    try:
                        score = self.index_data.GetPairScore(v.ref, v.alt)
                    except:
                        score = None
                    return_list.append(score)
        return return_list
